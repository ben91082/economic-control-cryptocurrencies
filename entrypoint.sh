# Migrate and collect static files
cd /home/docker/code/django_app
python3.6 manage.py collectstatic --no-input
python3.6 manage.py migrate --no-input
exec "$@"