#######################
Parameters --- Currency
#######################

.. sectnum:: :start: 9

Settings on these parameters determine design features of the alternative currency.


.. _p_curr_basic:

*****************
Basic Information
*****************

*   **Name:** This is for the name of the alternative currency. The default is 'Alternative Coins'.
*   **Abbreviation:** This is for an abbreviation of the name and is the most commonly used way to identify the alternative currency on exchanges and elsewhere. Usually it has three letters (as with USD, GBP, RMB, and BTC for example). The default is 'ALT'.
*   **Symbol:** This is a single character symbol for the alternative currency used as with symbols like $ and £. The default is 'c'.
*   **Decimal Places:** This is the maximum number of decimal places used to state amounts in the alternative currency. For example, USD, GBP, and EUR all use two decimal places. Bitcoin has eight.
*   **Who Pays Fees:** This is who pays the transaction fees for a payment for goods using the alternative currency. The values are Customer and Merchant, so either Customer Pays Fees or Merchant Pays Fees. (See :ref:`tech_trans_fees_purch` for full details.)
*   **Initial Exchange Rate:** The amount of fiat currency that is equivalent to one alternative coin. This exchange rate is not necessarily required but is the last resort when trying to establish a valuation of the the alternative currency (see :ref:`tech_valuation_history`).


.. _p_curr_support:

**************
System Support
**************
There are also settings related to the number of Operators involved, making the currency function:

*   **Operators Needed:** Choose 'Yes' if one or more Operators must be in place for the currency to function. This is the case for cryptocurrencies (which typically need nodes to process transactions and miners to provide proof that the Operators exist) but not for conventional notes and coins (where we ignore the physical act of creating the notes and coins because no further action is needed to support individual transactions). The number of Operators in place initially is set below under Launch Users And Holdings. If operators are needed then the minimum number is assumed to be 1.
*   **Maximum Operators:** The maximum number of Operators for the currency. This is useful if you want to limit the number of Operators for a blockchain in order to control its efficiency. Leave this blank if there is to be no maximum.
*   **Maximum Operators Per Transaction:** Current blockchain systems require all node operators to hold the full blockchain. For efficiency, future systems might split the transactions up, with only some nodes holding each transaction. This setting specifies the maximum number of Operators processing and holding any one transaction. If there are fewer than this many Operators then all the Operators will be involved. If this is left blank then this feature is not used. See :ref:`adr_curr_operators` for details on how this affects decisions.
*   **Maximum Percentage Of Operators Per Transaction:** This setting specifies the maximum proportion of all Operators processing and holding any one transaction. If left blank then this feature is not used. If a Maximum Operators Per Transaction and a Maximum Percentage Of Operators Per Transaction are both set then it is the one that results in the lower number at any time that is effective. See :ref:`adr_curr_operators` for details on how this affects decisions.

There are settings related to payments to Operators for transactions. These are made by the Controller and/or Users. All amounts are specified as the total paid across all Operators (so if there are more Operators they will each get smaller fees on average):

*   **Controller Paid Fixed Transaction Fee (Minted):** The total fixed fee per transaction paid to Operators by the Controller in the alternative currency, newly minted for the purpose. To model some cryptocurrencies it will be necessary to make this a fraction of the per-block fee.
*   **Controller Paid Percentage Transaction Fee (Minted):** This is the percentage of transaction value paid to Operators in the alternative currency by the Controller, newly minted for the purpose.
*   **User Paid Fixed Transaction Fee:** The total fixed fee per transaction paid to Operators in the alternative currency, paid by taking a cut of the transaction.
*   **User Paid Percentage Transaction Fee:** This is the percentage of transaction value paid to Operators in the alternative currency, paid by taking a cut of the transaction.
*   **Just To Winner:** Set to 'Yes' if the entire transaction fee is to be paid to the winner (in a consensus algorithm for example) and 'No' if it is to be split equally over all Operators that process and hold the transaction. (The settings above for Maximum Operators Per Transaction and Maximum Percentage Of Operators Per Transaction, if used, mean that in many cases not all Operators are involved with a transaction.) See :ref:`adr_curr_operators` for details on how this affects decisions.

N.B. As the average reward per day to Operators falls to near the sum of their variable and fixed costs some will decide to quit. Alternatively, if the gap opens up then new Operators should enter, if this is allowed by Maximum Operators (the maximum number of Operators allowed).

These settings are important in various decisions by Customers and Merchants in particular. The settings are also important controls, so are repeated in :ref:`p_contr_stocks` and in :ref:`p_contr_other`.

.. _p_curr_minting:

***************
Minting Methods
***************

This refers to the way new coins in the alterative currency get into circulation, not the physical process of minting.

The options supported by the simulator are:

*   *Gifting:* When a new user goes through some kind of registration process they are issued with a quantity of newly minted coins.
*   *Purchase:* Users can buy newly minted coins at any time at a price that may or may not vary over time.
*   *Rewards:* Operators are paid for their services in the alternative currency (according to Controller Paid Fixed Transaction Fee (Minted) and Controller Paid Percentage Transaction Fee (Minted) above).

These methods may also be used in combination.

The only parameter settings controlling these within this section are:

*   **Gift Coins:** The number of coins given to each user on registering for the first time. This may be zero or any positive number at the appropriate precision for the currency. Blank is taken as zero. The default is zero. If zero then Gift Coins are not given.
*   **Sell Freshly Minted On/Off:** Switches on or off the ability to buy freshly minted alternative currency from the Controller at the Mint Purchase Price.
*   **Mint Purchase Price:** The price in fiat currency to buy one alternative currency coin, freshly minted, from the Controller. Only effective when Sell Freshly Minted On/Off is set to On.

These two can also be set at :ref:`p_contr_stocks` and the payments to Operators are specified above under System Support as Controller Paid (Minted) amounts, as well as being repeated at :ref:`p_contr_stocks`.


.. _p_curr_launch:

*************************
Launch Users And Holdings
*************************

Currencies can launch in a variety of ways:

*   *From Nothing:* One possibility is to start with no Users and no coins minted.
*   *Launch Users:* Another possibility is to do some pre-launch work and so start with an initial population of Users with coins already minted and in their possession.
*   *Token Holders:* Another possibility is to do pre-launch work and have people who own tokens which they will be able to exchange for newly minted coins once the currency launches. Most will do that swap fairly quickly. 
*   *A Mix:* Finally, there can be a mix of both initial coin holders and initial token holders.

The amount of money (of all kinds) that Users have initially is set under Parameters - Users, but for the Initial Users it is still necessary to specify how much of their initial wealth is to be held as the alternative currency and how much of the wealth of token holders is in the form of tokens. Note that Initial Users and token holders are separate groups of Users. That is, an individual User cannot both be an initial User and a Token Holding User.

The settings needed to define the initial situation as as follows:

*   **Initial Users In Roles:** This is a table used to specify the number of Users initially, showing which roles they have adopted. The available roles are Speculator, Customer, Merchant, and Operator and, since they must adopt at least one role, there are 15 combinations of roles they could adopt (:math:`2^4 - 1 = 15`). In the example below, most Initial Users are Speculators only but some are Customers only and slightly more are Customers and Speculators together. There are five Merchants and five Operators. All the values must be non-negative integers and the defaults are all zero. The settings must be consistent with Operators Needed (explained above) so you must have at least one Operator initially if Operators Needed is 'Yes' but if it is 'No' then you must not have any users initially that are taking the role of Operator.

+-----+-----+------+-----+-----+
|Spec.|Cust.|Merch.|Oper.|Users|
+=====+=====+======+=====+=====+
|  Y  |  \- |  \-  |  \- | 100 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  \-  |  \- |  50 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  \-  |  \- |  60 |
+-----+-----+------+-----+-----+
|  \- |  \- |  Y   |  \- |   5 |
+-----+-----+------+-----+-----+
|  Y  |  \- |  Y   |  \- |   0 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  Y   |  \- |   0 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  Y   |  \- |   0 |
+-----+-----+------+-----+-----+
|  \- |  \- |  \-  |  Y  |   5 |
+-----+-----+------+-----+-----+
|  Y  |  \- |  \-  |  Y  |   0 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  \-  |  Y  |   0 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  \-  |  Y  |   0 |
+-----+-----+------+-----+-----+
|  \- |  \- |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|  Y  |  \- |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|Total                   | 220 |
+-----+-----+------+-----+-----+

*   **Mean Fraction Held As Alternative Currency:** All Initial Users are considered the same for wealth purposes initially. The total value of their holdings of fiat currency and alternative currency are determined by settings under :ref:`p_ipu_all`, but the proportion of that cash held as the alternative currency is drawn at random from a beta distribution whose parameters are calculated from its mean and variance. The Mean Fraction Held As Alternative Currency is the mean fraction of the initial cash pot of each User that is held in the alternative currency initially. (See :ref:`tech_minting` for all the ways that alternative currency can be minted.)
*   **Variance Of Fraction Held As Alternative Currency:** This variance is used along with the Mean above to calculate the parameters of the beta distribution used to generate initial fractions of wealth held in the alternative currency for each initial User. The simulator calculates parameters for the beta distribution and then displays feedback in the form of the mean, variance, P10, and P90 levels for the distribution resulting.
*   **Initial Alternative Currency Value:** The fiat currency price of an alternative currency coin to be used just for establishing how much of each initial User's cash pot is in alternative currency.

See :ref:`adr_curr_init_users` for use in decisions.

*   **Initial Token Holders:** The number of people initially holding tokens they can exchange for coins.
*   **Initial Token Holder Roles:** This is a table used to specify the probability of a Token Holding User adopting each combination of roles when first becoming a User (as a result of swapping their tokens for alternative currency). If the alternative currency does not need Operators then the probability of each combination of roles that includes being an Operator must be zero. The total of the probabilities must be 1.

+-----+-----+------+-----+-----+
|Spec.|Cust.|Merch.|Oper.|Prob.|
+=====+=====+======+=====+=====+
|  Y  |  \- |  \-  |  \- |0.8  |
+-----+-----+------+-----+-----+
|  \- |  Y  |  \-  |  \- |0.1  |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  \-  |  \- |0.05 |
+-----+-----+------+-----+-----+
|  \- |  \- |  Y   |  \- |0.02 |
+-----+-----+------+-----+-----+
|  Y  |  \- |  Y   |  \- |   0 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  Y   |  \- |   0 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  Y   |  \- |   0 |
+-----+-----+------+-----+-----+
|  \- |  \- |  \-  |  Y  |0.03 |
+-----+-----+------+-----+-----+
|  Y  |  \- |  \-  |  Y  |   0 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  \-  |  Y  |   0 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  \-  |  Y  |   0 |
+-----+-----+------+-----+-----+
|  \- |  \- |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|  Y  |  \- |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|  \- |  Y  |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|  Y  |  Y  |  Y   |  Y  |   0 |
+-----+-----+------+-----+-----+
|Total                   |   1 |
+-----+-----+------+-----+-----+

*   **Initial Token Price:** This is the price, in the fiat currency, of each token (with each token being worth one unit of the alternative currency).
*   **Mean Fraction Held As Tokens:** All Token Holding Users are considered the same for wealth purposes initially. Their pots of cash are determined by settings under :doc:`parameters users`, but the proportion of that cash held as tokens is drawn at random from a beta distribution whose parameters are calculated from its mean and variance. The Mean Fraction Held As Tokens is the mean fraction of the initial holding of each Token Holding User that is held in tokens, using the Initial Token Price to value the tokens.  (See :ref:`tech_minting` for all the ways that alternative currency can be minted.)
*   **Variance Of Fraction Held As Tokens:** This variance is used along with the Mean above to calculate the parameters of the beta distribution used to generate initial fractions of wealth held in tokens for each Token Holding User.

The parameters of the beta distribution are calculated in the same way as for the distribution of alternative currency above.

*   **Mean Delay Until Using Tokens:** This is the mean number of days before a Token Holding User exchanges all their tokens for alternative currency. The delay is distributed exponentially. See :ref:`tech_delays` for more details.

See :ref:`adr_curr_token_users`, :ref:`adr_token_when_convert`, :ref:`adr_token_roles`, and :ref:`adr_token_extras` for more information on use in decisions.


.. _p_curr_launch_controller:

**********************************
Initial Holdings Of The Controller
**********************************

At the start of a Trial, the Controller has initial stocks of currency:

*   **Initial First Fiat Currency Of Controller:** The amount of fiat currency held by the Controller when the Trial begins. Must not be negative.
*   **Initial Alternative Currency Of Controller:** The amount of alternative currency held by the Controller when the Trial begins. Must not be negative.
