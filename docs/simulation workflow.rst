###################
Simulation Workflow
###################

.. sectnum:: :start: 2

**********************
Simulations And Trials
**********************

The first step is to create a **Simulation**, which is just a bundle of defaults for the various parameters that will be set. Having chosen those defaults you then create a **Trial**, which initially copies the settings of your Simulation, but which you can change, both initially and as the Trial unfolds.

This means that you can conveniently group together many Trials that are the same or almost the same, except for some tweaks you have made to see what difference they would make.

The various parameters you can change for a Simulation are under the menu on the left hand side of the simulator screen. Trial List simply shows the list of Trials, if any, for your Simulation, and Descriptions fields are just for you to add helpful text explaining your Simulation. The other menu items let you change parameter settings.

Whenever you are looking at the Trial List page the settings will be for the Simulation (i.e. will be defaults for any Trial created under that Simulation). Whenever you are looking at a particular Trial, the settings relate to that Trial only. (Defaults for other Trials under the Simulation are unaffected by changes to the settings for an individual Trial.)

Although Simulation settings and Trial settings are different numbers, the explanations of Simulation defaults are the same as for the Trial settings, so they are all explained once under the heading Parameters.


*******
Presets
*******

Although the simulations are, unavoidably, much simpler than real life, there are still many settings to think about and adjust. It's complicated work that takes time. To save time it helps to reuse elements as far as possible and the simulator has three mechanisms for this:

#.  Simulations, which are bundles of defaults used as the starting point for similar or identical Trials.
#.  Copying a Simulation to create a new one with a new name but the same settings as the first, for modification.
#.  Presets, which are named bundles of settings at various levels within the parameters.

(You may have seen presets in other software. They are used extensively in music effects software to save and reuse the many complex settings needed to specify music sounds.)

Some presets are already part of the simulator. If you select one, the settings it contains are put into the Simulation or Trial you are currently working on. You can still change those settings individually as usual.

You can also create presets yourself while working with a Simulation or Trial. This involves choosing to create the preset at a particular level (explained below), then giving a name to the new preset. The settings currently in force at the chosen level are then saved in the preset.

Existing presets can be renamed and deleted.

The levels of preset are:

*   *Complete simulation:* All the settings for a Simulation or Trial are saved. The settings used are those of the Simulation if you are working on the Simulation, but those of the Trial if you are working on a Trial.
*   *Section:* The settings are saved for sections of the Simulation/Trial. The sections are: Environment, Currency, Users, Exchanges, and Controls.
*   *Multiplier Definitions:* Just the settings for an individual multiplier definition that maps decision factors to the values used in some decisions. The simulator distinguishes between multiplier definitions that map decision factors to values in the range 0 to 1 and other multiplier decisions and shows which is which. It also will not allow you to use a multiplier definition for a decision with which it is not compatible. While you are editing multiplier definitions the option to create a preset using the definitions you are currently working on is available.


*****************
Examining Results
*****************

During a Trial, and once a Trial is finished, you can:

*   view a number of different charts showing how the Trial unfolded
*   download those charts as graphic files
*   download a data file (.csv format) for analysis in other tools
*   download a data file (.csv format) showing the settings that were in place during the Trial, including changes made and their timing.

You can also download a data file (.csv format) showing the defaults for a Simulation.
