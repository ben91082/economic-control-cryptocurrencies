####################
Parameters --- Users
####################

.. sectnum:: :start: 10

Users (of the alternative currency) in the simulator take many decisions and the rules governing those decisions can mostly be adjusted through parameter settings. By changing these you can change the behaviour of the Users and the whole simulated system.

The lifecycle of a User is:

*   Just a statistic in the Pool Of Potential Users.
*   An Individual Potential User, whose attention has been gained and who might decide to become...
*   an Active User in one or more roles.
*   An Opting Out User, who has opted out of all roles and now just wants to get rid of their remaining alternative currency.
*   Once again just a statistic in the Pool Of Potential Users.


.. _p_popu:

***************************
The Pool Of Potential Users
***************************

The Pool Of Potential Users represents the wider population of people who might become Users but for the time being are not involved. The simulator represents this Pool with simple counts of people in it.

Each day, some of these potential Active Users are taken from the Pool and created as Individual Potential Users. These represent the people who are prompted, by conversations, emails, articles, etc to consider using the alternative currency. During the simulated day, each of the Individual Potential Users considers becoming an Active User of the alternative currency in one or more of the four roles. In the real world people usually search the internet or talk to friends to help them decide on this. If they opt in then they continue as Active Users. If they do not opt in then they cease to exist as Individual Potential Users and are returned to the Pool. If, after being an Active User of the alternative currency for a while, a User opts out of all roles then they become an Opting Out User and, finally, cease to exist as a User and get returned to the Pool.

The rate at which people become Active Users of the alternative currency is critical to the evolution of a Trial.

The Pool Of Potential Users is divided into 50 sub-totals, equal-sized at first. Each of the 50 sub-pools represents people exposed to a different proportion of the Total Current Buzz. The distribution of these proportions is controlled by settings explained below.

For example, if a sub-pool contains 1,000 potential Users and receives 5% of the Total Current Buzz (Positive Current Buzz + Negative Current Buzz), and if the Total Current Buzz is 2,000 messages (per day), then the sub-pool will receive 100 messages (2,000 x 5%). If, for example, 5% of those messages result in considering use then that is 5 (100 x 5%) Individual Potential Users from that sub-pool.

To take this example a bit further, imagine that the sub-pool initially contained 1,000 potential Users but 400 of these have already become Users leaving only 600 potential Users. If the sub-pool still receives 5% of the total Buzz Rate of 2,000 messages then the sub-pool receives 100 messages as before, but only 60 of these go to people not already Users. That means 3 (60 x 5%) Individual Potential Users will come from that sub-pool.

The number of Individual Potential Users selected from each sub-pool is rounded and the total selected is the sum across all 50 sub-pools.

The settings for the Pool are:

*	**Total Pool Size:** The total number of potential Users at the start of the Trial. Numbers between thousands and hundreds of thousands are reasonable, but other settings should ensure that the total number of Active Users does not become too large, making the simulator too slow. This should be a positive integer divisible by 50.
*	**Time Between Releases:** This is the simulated time in days between more potential Users being picked from the Pool to consider becoming Active Users. This should be a positive integer greater than or equal to 1, and the default is 1 day. The new Users will not start using the currency at the same instant, but will make their decision to opt in or out during the period between releases. While they wait to take that first decision they are Individual Potential Users.
*	**Percentage Of Messages Received By Top 20%:** This is the percentage of Total Current Buzz that is received by the most attentive 20% of potential Users in the Pool. (That is, the top 10 sub-pools of the 50.) This should be more than 0 and less than 100%. The simulator calculates the parameter of a Zipfian distribution that matches this percentage setting. The Zipfian distribution is a famous one with a single parameter, :math:`s`, that determines how unequal the distribution is.
*   **Consideration Rate:** This is the percentage of messages received by a potential User in the Pool that give rise to an Individual Potential User who will consider opting in. It should be between 0% and 100%, but 0% will mean that no Individual Potential Users are created.

See :ref:`adr_popu_when` and :ref:`adr_popu_many` for use in decisions.


**************************
Individual Potential Users
**************************


.. _p_ipu_all:

Settings For All Roles
======================

Regardless of their role(s), all users need an Initial Holding of the fiat currency. As usual, we want our Users to be different from each other, as in real life. The settings are as follows:

*   **Lowest Initial Holding:** This is the least, in the fiat currency, that any User can hold when they first become Users of the alternative currency (but before they have actually spent any of this Initial Holding of fiat currency).
*   **Mean Initial Holding:** This is the mean of holdings by Users when they first become Users of the alternative currency, expressed in the fiat currency.
*   **Median Initial Holding:** This is the median of holdings by Users when they first become Users of the alternative currency, expressed in the fiat currency.

See :ref:`tech_dist_wealth` for more details of the distribution used and :ref:`adr_ipu_props` for use in decisions.

Some crucial decision factors are the personal propensities to adopt the User roles (see :doc:`decision factor list`). This is a way to capture the non-financial reasons for getting involved. These propensities are distributed randomly according to Normal distributions with mean and standard deviation parameters you set. The table looks like this:

+----------------------------+-----------+--------+
| **Propensity To Be A ...** | **Mean**  | **SD** |
+============================+===========+========+
| Speculator                 |           |        |
+----------------------------+-----------+--------+
| Customer                   |           |        |
+----------------------------+-----------+--------+
| Merchant                   |           |        |
+----------------------------+-----------+--------+
| Operator                   |           |        |
+----------------------------+-----------+--------+


The default value for the mean is zero but the more positive it is the more Users will tend to want to opt in to the role. The default value for the standard deviation is 1, but the larger it is the greater the variations in desire to opt in. Increasing the standard deviation but not the mean will still tend to cause more Users to opt in because it will produce more Users with an extremely positive propensity.

It is a good idea to think of these propensities as being part of the same system and to make them comparable. This will make it easier to calibrate them within decisions.

From these settings the simulator randomly generates specific propensity values for each new User agent created. See :ref:`adr_ipu_props` for use in decisions.


.. _p_ipu_opt_in:

Settings For Opting In
======================

This is one of the most important and complex sets of decisions in a simulation. Each role is considered independently and, if a decision is taken to opt in to any of the roles, then the Individual Potential User will become an Active User in the selected role(s).

Different factors are important to each role. Each of these decisions is random, but with a probability of opting in that is controlled by a number of criteria. You could think of each criterion as a hurdle to be cleared. The initial assumption is that the probability of opting in is 1 (i.e. it is certain) but multipliers (always in the range 0..1) reduce this step by step. The more worry the User has on each criterion the more the probability of opting in is reduced. Having worked out a probability of opting in the final step is to generate a random number between 0 and 1 and check if it is less than the probability of opting in. If it is, then the User opts in to the relevant role.

For each role, the factors to consider must be chosen as settings from the list offered by the system. (See :doc:`decision factor list` for the full set.)

Then, for each factor, a calibration function type must be chosen. With factors that have a value from zero upwards, labelled 'half' factors, the available function types are :math:`\text{half-asc-1}` and :math:`\text{half-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1'). With factors that can take any value, labelled 'full' factors, the available function types are :math:`\text{full-asc-1}` and :math:`\text{full-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1').

Finally, for each factor, calibration values must be chosen that precisely fix the relationship between the decision factor and the probability multiplier resulting. You are asked to set a Half Level, which is the value for the factor that would give a multiplier of 0.5 (and reduce the probability of opting in by a half), and a Zero Multiplier, which is the value for the multiplier when the factor is zero.

For :math:`\text{half-asc-1}` the Zero Multiplier must be less than 0.5. For :math:`\text{half-desc-1}` the Zero Multiplier must be more than 0.5.

When all the criteria are considered each gives a multiplier reducing the probability of opting in and the product of all these is the final probability of opting in. This means that if all the factors are very satisfactory to the decision-maker the probability of opting in will be near to 1, but even one criterion on its own can reduce that to near zero if it is worrying enough.


.. _p_ipu_opt_in_spec:

As A Speculator
---------------

This requires a multiplier definition (see :ref:`adr_ipu_opt_in`):

*   **IPU Speculator Opt In Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting in. Suggested factors for the Speculator opt in decision are as follows:

    *   Propensity To Be A Speculator with :math:`\text{full-asc-1}`
    *   Current Buzz Positivity with :math:`\text{half-asc-1}`. The bigger the ratio of Positive to Negative Current Buzz the more persuasive it is.
    *   Accumulated Buzz Positivity with :math:`\text{half-asc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-asc-1}`. Higher variability is attractive to Speculators.
    *   Trend Strength with :math:`\text{full-asc-1}`


.. _p_ipu_opt_in_cust:

As A Customer
-------------

This requires a multiplier definition (see :ref:`adr_ipu_opt_in`):

*   **IPU Customer Opt In Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting in. Suggested factors for the Customer opt in decision are as follows:

    *   Propensity To Be A Customer with :math:`\text{full-asc-1}`
    *   Current Buzz Positivity with :math:`\text{half-asc-1}`
    *   Accumulated Buzz Positivity with :math:`\text{half-asc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-desc-1}`. Customers prefer lower variability.
    *   Total For Sale In Alternative Currency with :math:`\text{half-asc-1}`


.. _p_ipu_opt_in_merch:

As A Merchant
-------------

This requires a multiplier definition (see :ref:`adr_ipu_opt_in`):

*   **IPU Merchant Opt In Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting in. Suggested factors for the Merchant opt in decision are as follows:

    *   Propensity To Be A Merchant with :math:`\text{full-asc-1}`
    *   Current Buzz Positivity with :math:`\text{half-asc-1}`
    *   Accumulated Buzz Positivity with :math:`\text{half-asc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-desc-1}`. Merchants prefer lower variability.
    *   Active User Count with :math:`\text{half-asc-1}`


.. _p_ipu_opt_in_oper:

As An Operator
--------------

This requires a multiplier definition (see :ref:`adr_ipu_opt_in`):

*   **IPU Operator Opt In Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting in. Suggested factors for the Operator opt in decision are as follows:

    *   Propensity To Be An Operator with :math:`\text{full-asc-1}`
    *   Current Buzz Positivity with :math:`\text{half-asc-1}`
    *   Accumulated Buzz Positivity with :math:`\text{half-asc-1}`
    *   Active User Count with :math:`\text{half-asc-1}`
    *   Operator Projected Profit with :math:`\text{full-asc-1}`


************
Active Users
************

Settings For All Roles
======================


.. _p_active_all_when_hold:

When to consider holding levels
-------------------------------

The ideal (average) time to next consider how much fiat currency and alternative currency to hold is different for each role held. The simulator calculates the ideal (average) time for each role and takes the earliest time as the average to use (see :ref:`adr_active_all_when_hold`).

The average delay for the Speculator role is determined by the following settings:

*   **Speculator Holding Default Average Delay:** The initial average delay in days.
*   **Speculator Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Speculators in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`
    *   Value Of User Holding with :math:`\text{half-desc}`
    *   Total Transaction Fee with :math:`\text{half-asc}`

The average delay for the Customer role is determined by the following settings:

*   **Customer Holding Default Average Delay:** The initial average delay in days.
*   **Customer Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Customers in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`
    *   Value Of User Holding with :math:`\text{half-desc}`
    *   Total Transaction Fee with :math:`\text{half-asc}`
    *   Customer Purchases (Volume) with :math:`\text{half-desc}`
    
The average delay for the Merchant role is determined by the following settings:

*   **Merchant Holding Default Average Delay:** The initial average delay in days.
*   **Merchant Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Merchants in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`
    *   Value Of User Holding with :math:`\text{half-desc}`
    *   Total Transaction Fee with :math:`\text{half-asc}`
    *   Number Of Goods Offered with :math:`\text{half-desc}`

The average delay for the Operator role is determined by the following settings:

*   **Operator Holding Default Average Delay:** The initial average delay in days.
*   **Operator Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Operators in this decision are:

    *   Value Of User Holding with :math:`\text{half-desc}`
    *   Fee Per Operator with :math:`\text{half-asc}`


.. _p_active_all_hold:

How much alternative currency and fiat currency to hold
-------------------------------------------------------

The holdings of fiat currency and alternative currency depend on the roles taken by the User. If the User is not a Speculator then its need for currency is determined by the estimated needs of its activities as a Customer, Merchant, and/or Operator, plus a 50% safety margin. See :ref:`adr_active_all_hold` for related decision details.

However, if the User is a Speculator then some calculation is needed to decide how much would ideally be invested in each currency, and then this is combined with information about the currency needs from any other roles, and the Initial Holding, to decide on the holdings to aim for.

Speculators allocate their holdings (the combination of fiat currency and alternative currency) according to Kelly's Betting Strategy, which is well-known. They decide the probability of the alternative currency rising in value relative to the fiat currency and then allocate their cash in accordance with the probability. If the probability of the alternative currency rising is judged to be :math:`p` then the proportion of the holdings to be held as alternative currency will be :math:`p` (valued in the fiat currency), with the rest in the fiat currency.

The judged probability of the alternative currency rising is based on decision factors, functions that map factors to probabilities (:math:`\text{half-asc-1}`, :math:`\text{half-desc-1}`, :math:`\text{full-asc-1}`, and :math:`\text{full-desc-1}`), and their calibration settings.

The settings are:

*   **Speculator Alt Holding Multiplier Definitions:** Decision factors, calibration function types, and calibration levels that define probabilities for your chosen decision factors. Suggested factors for this decision are:

    *   Current Buzz Positivity with :math:`\text{full-asc-1}`
    *   Trend Strength with :math:`\text{full-asc-1}`

*   **Speculator Alt Holding Factor Weights:** These weight the decision factors and must sum to 1.

Since the Trend Strength is a key factor it is necessary to specify the Speculator's approach to interpreting trends. Speculators use one of three different extrapolation rules and the percentage of each type generated (randomly) can be set. The percentages must add up to 100 of course.

*   **Continue Trend %:** This is the percentage of Speculators that expects current trends to continue.
*   **Contrarian %:** This is the percentage of Speculators that expects current trends to reverse.
*   **Regress To Mean %:** This is the percentage of Speculators that expects the next move to be towards a longer term moving average.

In addition, Trend Strength and some other decision factors use a recency weighting that is specific to each Speculator and represents their tendency to take a short- or long-term perspective.

*   **Lowest Recency Factor:** A number greater than 0 but less than 1 that represents the lowest possible long term weight given to the most recent value in a sequence.
*   **Highest Recency Factor:** A number greater than 0 but less than 1, and greater than or equal to the Lowest Recency Factor, that represents the highest possible long term weight given to the most recent value in a sequence. Each Speculator's Recency Factor is chosen randomly, with a uniform probability distribution, between the Lowest and Highest Recency Factor.

The final calculation of the individual probabilities is done using a modified weighted average which requires weights, :math:`w_i`, summing to 1 to be chosen. If there are :math:`n` factors to combine and each produces a probability, :math:`p_i`, then they are combined to make an overall probability as follows:

.. math::

    p_{all} = \sum_{i=0}^n w_i p_i, \ \ \ \text{where } p_0 = 0.5

The idea is that the initial view is that the probability of rising value is 0.5, but as more evidence is considered that view is modified. The weights determine how important each factor is and how much those revise the initial view. (The underlying idea is to use Bayesian updating of beta distributions, but the final formulae are simple.)

With this decided, the holdings of currency are then determined by considering the ideal levels for Speculation, the currency needs of other roles, and the Initial Holding of the User. This is specified in detail in :ref:`tech_holdings_mgmt_reconsider`.



Settings For The Speculator Role
================================


.. _p_active_spec_when_opt_in:

When next to consider opting in/out as a Speculator
---------------------------------------------------

Speculators consider opting in or out from time to time. The time delay between each consideration is random but with an average delay that you can set. However, you can also use decision factors to influence that default delay. (See :ref:`adr_active_spec_when_opt_in` for use in decisions.)

Either the Active User is a Speculator already and will next consider opting out of that role, or the User is not a Speculator (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **Speculator Opting Default Average Delay:** The initial average delay in days.
*   **Speculator Opting Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Speculators in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`


.. _p_active_spec_opt_in:

Opting In As A Speculator
-------------------------

The settings for opting in as a Speculator are those already covered under Individual Potential Users (:ref:`p_ipu_opt_in`), with specific suggestions for Speculators at :ref:`p_ipu_opt_in_spec`.


.. _p_active_spec_opt_out:

Opting Out From Being A Speculator
----------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors. Sometimes different criteria are used because Active Users have more information.

This requires a multiplier definition (see :ref:`adr_active_spec_opt_out`):

*   **Speculator Opt Out Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting out. Suggested factors for this decision are as follows:

    *   Propensity To Be A Speculator with :math:`\text{full-desc-1}`
    *   Current Buzz Positivity with :math:`\text{full-desc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-desc-1}`
    *   Speculator Gains with :math:`\text{full-desc-1}`
    *   Trend Strength with :math:`\text{full-desc-1}`


Settings For The Customer Role
==============================


.. _p_active_cust_when_opt_in:

When next to consider opting in/out as a Customer
-------------------------------------------------

Customers consider opting in or out from time to time. The time delay to the next consideration is random but with an average delay that you can set (see :ref:`tech_delays`). However, you can also use decision factors to adjust that default delay. (See :ref:`adr_active_cust_when_opt_in` for use in decisions.)

Either the Active User is a Customer already and will next consider opting out of that role, or the User is not a Customer (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **Customer Opting Default Average Delay:** The initial average delay in days.
*   **Customer Opting Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Customers in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`


.. _p_active_cust_opt_in:

Opting In
---------

The settings for opting in as a Customer are those already covered under Individual Potential Users (:ref:`p_ipu_opt_in`), with specific suggestions for Customers at :ref:`p_ipu_opt_in_cust`.


.. _p_active_cust_opt_out:

Opting Out From Being A Customer
--------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors. Sometimes different criteria are used because Active Users have more information.

This requires a multiplier definition (see :ref:`adr_active_cust_opt_out`):

*   **Customer Opt Out Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting out. Suggested factors for this decision are as follows:

    *   Propensity To Be A Customer with :math:`\text{full-desc-1}`
    *   Current Buzz Positivity with :math:`\text{full-desc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-asc-1}`
    *   Total For Sale In Alternative Currency with :math:`\text{half-desc-1}`
    *   Customer Purchases In Alternative Currency (Volume) with :math:`\text{half-desc-1}`


.. _p_active_cust_shop:

Shopping
--------

The simulator models only some of the complexities of everyday shopping behaviour. Merchants are not allowed to compete on price, other than accidentally and by choosing cheaper payment systems. Customers buy sub-sets of the Lone, Paired, and Exclusive Goods each day, selecting the Merchant offering the best price, or the Control Catalogue, or choosing at random if there is a tie.

Customers go shopping each day. Each Customer has their preferred time of day for shopping. How many items each Customer buys when shopping varies from day to day, and each Customer has their own average number. Some Customers buy more items, on average, each time they go shopping than others.

The exact number of purchases made daily is random but with a fixed average number for each person. The distribution of these averages is controlled by the following settings:

*   **Earliest Time Of Day:** When the Customer role is adopted, a time of day for shopping is selected and random. This is the earliest shopping time for any shopper.
*   **Latest Time Of Day:** This is the latest time of day for any shopper. Actual shopping times are chosen with a uniform probability distribution between the Earliest and Latest times.
*   **Lowest Average Fraction Of Lone Goods Purchased:** When the Customer role is adopted, an average fraction of the Lone Goods purchased daily is chosen based on the User's Initial Holding but between the Lowest and Highest Average Fraction Of Lone Goods Purchased. This is the lowest fraction.
*   **Highest Average Fraction Of Lone Goods Purchased:** This is the highest average fraction of Lone Goods that the Customer will purchase daily.
*   **Lowest Average Fraction Of Paired Goods Purchased:** When the Customer role is adopted, an average fraction of the Paired Goods purchased daily is chosen based on the User's Initial Holding but between the Lowest and Highest Average Fraction Of Paired Goods Purchased. This is the lowest fraction.
*   **Highest Average Fraction Of Paired Goods Purchased:** This is the highest average fraction of Paired Goods that the Customer will purchase daily.
*   **Lowest Average Fraction Of Exclusive Goods Purchased:** When the Customer role is adopted, an average fraction of the Exclusive Goods purchased daily is chosen based on the User's Initial Holding but between the Lowest and Highest Average Fraction Of Exclusive Goods Purchased. This is the lowest fraction.
*   **Highest Average Fraction Of Exclusive Goods Purchased:** This is the highest average fraction of Exclusive Goods that the Customer will purchase daily.
*   **Mean Substitute Valuation Multiple:** Some goods have a substitute that is a bit different in value to the Customer (but not in underlying fiat currency price). The value of the substitute divided by the value of the good is the Substitute Valuation Multiple. These are generated randomly for Users according to the Normal distribution, with the Mean Substitute Valuation Multiple.
*   **SD Of Substitute Valuation Multiple:** This is the standard deviation for the distribution used to generate Substitute Valuation Multiples.

See :ref:`adr_active_cust_shop` for use in decisions.


Settings For The Merchant Role
==============================


.. _p_active_merch_when_opt_in:

When next to consider opting in/out as a Merchant
-------------------------------------------------

Merchants consider opting in or out from time to time. The time delay to the next consideration is random but with an average delay that you can set (see :ref:`tech_delays`). However, you can also use decision factors to adjust that default delay. (See :ref:`adr_active_merch_when_opt_in` for use in decisions.)

Either the Active User is a Merchant already and will next consider opting out of that role, or the User is not a Merchant (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **Merchant Opting Default Average Delay:** The initial average delay in days.
*   **Merchant Opting Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Merchants in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`


.. _p_active_merch_opt_in:

Opting In As A Merchant
-----------------------

The settings for opting in as a Merchant are those already covered under Individual Potential Users (:ref:`p_ipu_opt_in`), with specific suggestions for Merchants at :ref:`p_ipu_opt_in_merch`.


.. _p_active_merch_opt_out:

Opting Out From Being A Merchant
--------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors. Sometimes different criteria are used because Active Users have more information.

This requires a multiplier definition (see :ref:`adr_active_merch_opt_out`):

*   **Merchant Opt Out Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting out. Suggested factors for this decision are as follows:

    *   Propensity To Be A Merchant with :math:`\text{full-desc-1}`
    *   Current Buzz Positivity with :math:`\text{full-desc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-asc-1}`
    *   Total For Sale In Alternative Currency with :math:`\text{half-desc-1}`
    *   Sales Made in Alternative Currency with :math:`\text{half-desc-1}`


.. _p_active_merch_goods:

Which goods to offer
--------------------

The following settings control the selection of goods offered by each Merchant.

*   **Lowest Fraction Of Lone Goods:** This is the lowest fraction of the total list of types of Lone Good that any Merchant can offer. Must be between 0 and 1.
*   **Highest Fraction Of Lone Goods:** This is the highest fraction of the total list of types of Lone Good that any Merchant can offer. Must be between 0 and 1 and not less than the Lowest Fraction Of Lone Goods.
*   **Lowest Fraction Of Paired Goods:** This is the lowest fraction of the total list of types of Paired Good that any Merchant can offer. Must be between 0 and 1.
*   **Highest Fraction Of Paired Goods:** This is the highest fraction of the total list of types of Paired Good that any Merchant can offer. Must be between 0 and 1 and not less than the Lowest Fraction Of Paired Goods.

The simulator chooses *how many* Lone Goods and Paired Goods are offered by each Merchant by considering the Merchant's Initial Holding (choosing between the Highest and Lowest Fractions). It then chooses *which* goods each Merchant will offer at random. The result of this is that the Merchant could be offering goods with no substitute and goods with their substitute.

Merchants choose a pricing policy and one alternative is to offer no prices in the alternative currency. This means that the substitute good in each pair of Paired Goods cannot be sold by the Merchant until the policy is changed.

See :ref:`p_active_merch_goods` and :ref:`p_env_goods` for related settings and :ref:`adr_env_goods` for details of the selection made.


.. _p_active_merch_when_policy:

When to reconsider the pricing policy
-------------------------------------

The pricing policy is considered immediately on opting in as a Merchant, and also after planned delays.

The delay to the next consideration is exponentially distributed (see :ref:`tech_delays`) with an average delay determined by the following settings:

*   **Merchant Price Policy Average Delay:** The initial average delay in days.
*   **Merchant Price Policy Delay Multiplier Definitions:** Decision factors, calibration functions, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors for Merchants in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`

See :ref:`adr_active_merch_when_policy` for use in decisions.


.. _p_active_merch_policy:

Which pricing policy to use
---------------------------

The Merchant's two-stage decision is the same regardless of what policy, if any, the Merchant is already using. The options are, first:

*   *No Alternative Currency Prices:* No prices in the alternative currency are offered. The Merchant will not accept alternative currency on any goods.
*   *Alternative Currency Prices:* This is done in two ways, considered in the second stage.

Then, if Alternative Currency Prices is chosen, there is a second choice between:

*   *Fixed:* Prices in alternative currency are set for all goods the Merchant offers and left fixed until a planned revision time and date.
*   *Floating:* Prices in alternative currency are set at the instant of enquiry/purchase by a Customer.

In all cases, the alternative currency price is the amount needed to obtain the fiat currency price of the good by obtaining fiat in the most efficient immediate way at the time of pricing. Typically, that means exchanging the alternative currency received for fiat currency on a currency exchange using orders already on the exchange.

For the choice between No Alternative Currency Prices and Alternative Currency Prices a probability is chosen in a way similar to the Opt In decisions, and the final choice is random according to that probability. The starting assumption is that Alternative Currency Prices will be set, but worries create multipliers that reduce the probability from 1 down to something lower. This requires a multiplier definition:

*   **Merchant Policy Choice 1 Multiplier Definitions:** Decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that decrease the probability of choosing Alternative Currency Prices. Suggested factors for this first decision are as follows:

    *   Current Buzz Positivity with :math:`\text{half-asc-1}`. The bigger the ratio of Positive to Negative Current Buzz the more persuasive it is.
    *   Accumulated Buzz Positivity with :math:`\text{half-asc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-desc-1}`. Higher variability is discouraging.

If Alternative Currency Prices are chosen then the second decision is made between Fixed and Floating prices. The approach is similar with an initial assumption of choosing Fixed prices which is reduced by multipliers reflecting worry about various factors --- typically related to volatility. This requires a multiplier definition:

*   **Merchant Policy Choice 2 Multiplier Definitions:** Decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that decrease the probability of choosing Fixed prices. The suggested factor for this second decision is:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc-1}`. Higher variability is off-putting.


.. _p_active_merch_when_price:

When to price next
------------------

If a Fixed policy has been chosen the prices are set immediately and after planned delays, influenced by decision factors.

The delay to the next re-pricing is exponentially distributed (see :ref:`tech_delays`) with an average delay determined by the following settings:

*   **Merchant Price Average Delay:** The initial average delay in days.
*   **Merchant Price Delay Multiplier Definitions:** Decision factors, calibration functions, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors for Merchants in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`

See :ref:`adr_active_merch_when_price` for use in decisions.


Settings For The Operator Role
==============================


.. _p_active_oper_operating:

Operating
---------

Operators provide services that allow the alternative currency to operate. They incur costs and these affect whether they continue in their role. These costs are of two types:

*   *Variable:* The variable cost is the cost, in the fiat currency, incurred by an Operator, on average, per transaction. This means the extra cost incurred through having to process the transaction and does not include costs incurred simply by having the equipment and leaving it switched on.
*   *Fixed:* The fixed cost, in fiat currency, incurred by an Operator, on average, daily. This excludes the variable costs that are driven by processing transactions.

The settings that control the distributions of these costs across different Operators are as follows:

*   **Lowest Variable Cost:** The lowest variable cost possible for any Operator. Must be zero or more and may be given to more precision than the fiat currency will allow.
*   **Mean Variable Cost:** The mean variable cost across all Operators. Must be more than the Lowest Variable Cost.
*   **Median Variable Cost:** The median variable cost across all Operators. Again, must be more than the Lowest Variable Cost.

*   **Lowest Fixed Cost:** The lowest fixed cost possible for any Operator. Must be zero or more and may be given to more precision than the fiat currency will allow.
*   **Mean Fixed Cost:** The mean fixed cost across all Operators. Must be more than the Lowest Fixed Cost.
*   **Median Fixed Cost:** The median fixed cost across all Operators. Must be more than the Lowest Fixed Cost.

These costs are used in decisions via a decision factor. See :doc:`decision factor list` for details.


.. _p_active_oper_when_opt_in:

When next to consider opting in/out as an Operator
--------------------------------------------------

Operators consider opting in or out from time to time. The time delay to the next consideration is random but with an average delay that you can set (see :ref:`tech_delays`). However, you can also use decision factors to adjust that default delay. (See :ref:`adr_active_oper_when_opt_in` for use in decisions.)

Either the Active User is an Operator already and will next consider opting out of that role, or the User is not an Operator (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **Operator Opting Default Average Delay:** The initial average delay in days.
*   **Operator Opting Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types for Operators in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`


.. _p_active_oper_opt_in:

Opting In As An Operator
------------------------

The settings for opting in as an Operator are those already covered under Individual Potential Users (:ref:`p_ipu_opt_in`), with specific suggestions for Operators at :ref:`p_ipu_opt_in_oper`.


.. _p_active_oper_opt_out:

Opting Out From Being An Operator
---------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors. Sometimes different criteria are used because Active Users have more information.

This requires a multiplier definition (see :ref:`adr_active_oper_opt_out`):

*   **Operator Opt Out Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that will decrease the probability of opting out. Suggested factors for this decision are:

    *   Propensity To Be An Operator with :math:`\text{full-desc-1}`
    *   Current Buzz Positivity with :math:`\text{full-desc-1}`
    *   SD Of Alternative Currency Value with :math:`\text{half-asc-1}`
    *   Operator Profit Made with :math:`\text{full-desc-1}`
    *   Operator Projected Profit with :math:`\text{full-desc-1}`


****************
Opting Out Users
****************

.. _p_outing_when_rid:

When next to try to get rid of alternative currency holding
===========================================================

On initially becoming an Opting Out User, the User will immediately try to get rid of its alternative currency holding. If this is not completely successful immediately then a delay until next consideration will be set.

The delay to the next attempt is exponentially distributed with an average delay determined by the following settings:

*   **Opting Out Default Average Delay:** The initial average delay in days.
*   **Opting Out Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors for Opting Out Users in this decision are:

    *   Value Of User Holding with :math:`\text{half-desc}`
    *   Current Buzz Negativity with :math:`\text{full-desc}`

See :ref:`adr_outing_when_rid` for details.


***************
Further Reading
***************

Office For National Statistics (2019). *Average household income, UK: Financial year ending 2018*. Available online `here`__.

.. __: https://www.ons.gov.uk/peoplepopulationandcommunity/personalandhouseholdfinances/incomeandwealth/bulletins/householddisposableincomeandinequality/yearending2018

Kelly Jr, J. L. (2011). A new interpretation of information rate. In *The Kelly Capital Growth Investment Criterion: Theory and Practice* (pp. 25-34). Available online `here`__.

.. __: http://www.herrold.com/brokerage/kelly.pdf

Kubińska, E., Markiewicz, Ł., & Tyszka, T. (2012). Disposition effect among contrarian and momentum investors. *Journal of Behavioral Finance*, 13(3), 214-225. Available online `here`__.

.. __: https://www.researchgate.net/profile/ukasz_Markiewicz/publication/244051564_Disposition_Effect_Among_Contrarian_and_Momentum_Investors/links/556de68d08aefcb861db945b.pdf

Morrin, M., Jacoby, J., Johar, G. V., He, X., Kuss, A., & Mazursky, D. (2002). Taking stock of stockbrokers: Exploring momentum versus contrarian investor strategies and profiles. *Journal of Consumer Research*, 29(2), 188-198. Available online `here`__.

.. __: https://www0.gsb.columbia.edu/mygsb/faculty/research/pubfiles/116/stockbrokers.pdf

