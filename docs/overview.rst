########
Overview
########

.. sectnum::

*******
Context
*******
Alternative currencies include a wide range of schemes, such as cryptocurrencies, which exist alongside national fiat currencies (and the regional Euro), and serve similar purposes. Typically they have relatively few users but are not necessarily restricted to particular territories.

As alternatives to more familiar forms of money, alternative currencies need to fulfill the usual roles of money. Their performance in this role is their economic performance. Achieving good economic performance requires careful control. In particular, the supply of the currency needs to keep pace with its use or prices expressed in the currency will begin to inflate or deflate.

The key challenges for an alternative currency include gaining new users and maintaining a reasonably steady valuation against other currencies and in terms of equivalent goods and services.

New alternative currencies need to be designed carefully if they are to perform economically.

*******
Purpose
*******

This simulator is designed to allow testing, through interactive simulation, of proposed economic control methods for alternative currencies. Typically, the alternative currency will be at the design stage and does not exist at the time its control mechanisms are being designed. 

In the simulator, it is possible to choose properties of the currency, its users, its ecosystem, and its economic environment and simulate possible futures for it.

The simulator is interactive so that you can repeatedly simulate a period of time, see what has happened so far, change your selected control settings, and continue the simulation for some more time. This provides an opportunity to learn more about what it takes to control the economic behaviour of an alternative currency.

********
Approach
********

Testing Controls
================

The simulation is agent-based, stochastic, and of discrete events. This means that it simulates the decision-making behaviour of individuals in detail, that some aspects of each simulation run are randomised to reflect uncertainty, and that the simulation is based on specific events happening at precise points in time. This means that you can see time series graphs showing the behaviour of the simulation over time and that it has the intricate detail characteristic of the real world. Also, if you run the simulation again from the start with the same settings (but not the same random number seed) you will get a different prediction.

With a system as complex and unpredictable as an economy, even when it is restricted to focus on a currency, it is not reasonable to expect precise predictions of the future. It might be possible to make probabilistic predictions and the simulator, properly used, should be better than intuition. However, the real goal of the simulator is to test control methods. Provided the control methods are based on relatively predictable phenomena it should be possible to explore their effectiveness against a variety of difficult market behaviour without the ability to make precise predictions about that market.

An analogy to this is the problem of predicting where sheep will wander in a field. They may be partly predictable, but left to themselves sheep may seem to roam randomly. However, using a well-trained sheep dog it is possible to harness sheep's predictable tendency to herd together and retreat from the dog. If we can control the dog then the dog will control the sheep and the future path of the sheep becomes much more predictable.

During the interactive simulations, the user of the simulator takes the role of Controller and adjusts various settings that influence the evolution of the simulation, attempting to achieve economically desirable results.


Principles For Agent Decisions
==============================

All the agents in a simulation make decisions. Modelling those decisions is one of the most complex and important aspects of simulation. The decision rules that agents can use in the simulator have been designed with some helpful principles in mind.

*	**Agents are diverse and error prone:** The way agents ‘think’ is not the same for all agents and they have differing priorities and circumstances. Consequently, even if they appear to be facing the same decision about the alternative currency they are usually not. In most cases this is modelled by having the decision process control the probability of each alternative being chosen in a decision, but the final choice is randomised. In addition, agents sometimes have explicitly different philosophies and sometimes make mistakes randomly. Most alternative currency users are not professional currency traders using mathematical models and automated trading, so the simulation reflects reality.

*   **Agent characteristics are controllable:** The mix of agents with different characteristics can usually be changed in the simulator as can some important characteristics of those agent types.

*	**Collective behaviour is broadly rational despite individual lapses:** This is a typical property of human thinking, but especially when people have different sources of evidence. The agents are partly rational and partly consistent, confronted with a theory of the world that is too complex and unquantified for them to deal with.

*	**Not blatantly stupid:** Although individuals may occasionally make blatantly stupid decisions the collective tendency should be to avoid behaviour that is clearly irrational. For example, opting in as a customer when no goods can be bought with the alternative currency, or when the exchange rate is chaotic, is illogical and few if any agents should do it in a simulation. (But it might still be logical for a speculator.)

*	**Limited intelligence:** Where a decision analyst should, in theory, go into detailed and sophisticated modelling but this is not what nearly everyone does, the simulator will sometimes avoid the detail and just choose a number randomly from a sensible range. This again reflects real thinking which is bounded and inconsistent.

*	**Consistent techniques:** Where a decision is similar to another taken in the same or a different role then the mechanism of the decision is also similar.

*	**Simplicity:** Where there is no strong reason for choosing something more complex, the system uses the simplest mathematical approach available. For example: uniform distributions and simple multiplicative or additive models to combine variables. It has been assumed that causes do not interact unless it is clear that they do.

*	**Real world variables:** Wherever possible, variables have a real world meaning rather than being arbitrary coefficients. For example, a dimensionless index of publicity is not as good as a variable representing combined publicity in a way that might be measured in the real world e.g. ‘number of positive messages received per day on average per person.’

*	**Real world calibration:** Where practical, variables have been chosen so that real world data are available to compare with the simulation’s numbers. The main limitation on this is that often real world numbers are not available. For example, the number of people using Bitcoin is unknown.

*	**Imaginable calibration situations:** For some simulation settings it is necessary for you to choose a value based on experience and judgement. To make this easier there will sometimes be suggested defaults and you will usually be asked for a value of something that can be imagined and judged, rather than a seemingly meaningless parameter within a complex mathematical function. In some cases, what you choose is then converted into a parameter within a complex mathematical function.

Lifecycle Of An Alternative Currency
====================================

Some alternative currencies begin life with no users and no coins issued. They have mechanisms where new coins are created and users can get them and start using them. However, some alternative currencies give themselves a flying start by having some kind of initial phase where future users are recruited and may even buy the coins in advance of them existing, or buy tokens of some kind that can be exchanged for the coins once they exist. As a result, within moments of the currency starting to operate it can have many users with holdings, or the user base and holdings can grow quickly as people convert their tokens into coins.

Subsequently, the currency is used in various ways either forever or until it fails, is withdrawn, or is replaced by something similar. In the simulator, currencies continue until the end point set but it may be that they are effectively dead because of having almost no transactions, almost no users, and not being supported by any exchanges. Replacement and orderly withdrawal of alternative currencies are not supported.

Lifecycle Of a User
===================

Deciding to become a user comes before decisions about how much of the currency to buy and usually involves some effort. For example, they will have to learn about the currency, consider alternative sources of the currency (e.g. different exchanges), decide how to store private information (such as private keys) securely, choose software or websites, install that software, and set themselves up on websites.

Choices of assumptions about user decisions in this area are important because the rate at which a currency gains or loses new users is crucial to its performance. 

In the world there are many more people than can be represented in the simulator but only a small proportion of them, in the foreseeable future, will have any interest in using an alternative currency. In the simulation, that huge pool of people in the world, the potential users, is represented by a table with just counts of how many people have each of a list of different levels of exposure to news of the currency.

Periodically, a few people are taken from this list and created as individual potential users who soon consider becoming active users of the currency in one or more roles. If they do not opt in then they return to the pool of potential users. If they opt in to any of the roles then their life becomes much more interesting as they become active users.

During the simulation they may opt in or out of the various roles and will own the currency. They will buy or sell goods, speculate on the exchanges, and perhaps also provide computer power to support the currency system. Their wealth will rise and fall both in fiat and alternative currency terms.

When they have opted out of all roles they may spend a bit of time getting rid of their remaining holding of alternative currency, but when they no longer own any of the alternative currency (having sold their last coins) they will return to the pool of potential users.

Types Of Agent and Their Roles
==============================

There are several types of agent.

**Token Holding Users:** Agents that have tokens they can exchange for alternative currency. When they do they will become Active Users.

**Pool Of Potential Users:** An agent represents the large pool of potential users out there in the world, currently unconcerned with the currency.

**Individual Potential Users:** These are User agents waiting to consider opting in as Active Users.

**Active Users:** Typically the most numerous agents are the people who use the currency in some way. These user agents can take any or all of the following roles:

*	*Speculator:* Someone who buys and sells the currency hoping to buy low, sell high, and make a profit.

*	*Customer:* Someone who wants to use the currency to pay for real goods and services.

*	*Merchant:* Someone who wants to accept the currency as payment for real goods and services.

*	*Operator:*  Someone who provides computer power that supports the currency system itself.

They may opt in and out of these roles at various times and as they do their behaviour changes accordingly. There may even be conflicts between the roles. If a user is not taking the role of Speculator then it usually wants to get rid of any currency it does not need. However, when a user adopts the Speculator role that changes and the user will hold what is expected to be profitable, which will often be more currency than is needed. This happens even if the user is also in other roles, such as Customer.

**Opting Out User:** This is a User who has opted out of all roles but still has a holding of alternative currency and wants to get rid of it.

**Exchange:** A much more specialised agent is a currency exchange.

**Economic Environment:** There is also an agent that represents the environment and generates challenging events such as news stories.

In addition to these software agents there is a participant called the **Controller** that is under the interactive control of the simulator's user and tries to manage the alternative currency system to achieve good economic outcomes.


What is left out
================

No simulation can be a faithful replica of a functioning economy. Simplifications have to be made. The main simplifications made in this simulator are as follows:

*   The buying and selling of goods is only that by Customers and Merchants who have decided to be Active Users of the alternative currency. In the real world there will be many more Customers and Merchants and many more transactions.
*   International purchases and sales are not explicitly modelled.
*   Most of the details of Merchant activity are left out, including stock levels and costs to purchase goods.
*   Merchants do not compete on price.
*   There is no inflation in the fiat currency, which instead serves as a settled benchmark for valuation.
*   There are no financial instruments --- not even loans.
*   The exchanges do not operate client accounts. Users of the exchanges just get the currency they want directly.


**********
Validation
**********

Agent-based simulations are very different from the more traditional statistical prediction models and from dynamical models based on differential equations. Agent-based simulations are based on intricate model components with realistic detail. It is very easy to compare them with our personal beliefs and mental models of people, decisions, and economies. It is natural to wonder how realistic each detailed assumption is and to seek validation of each element as well as overall predictive performance.

In contrast, the more abstract forms of mathematical model are usually based on ruthless simplification and validated mostly by their ability to make precise predictions of the real economy a short time into the future. We tend not to worry much about the realism of the elements of the abstract mathematical models and consider only their predictive performance. 

The simulator supports a wide range of different ideas about how agents make their decisions so it is not practical to provide a simulator that is already fully validated for you to use. Although we have made a lot of effort to provide realistic features, users of the simulator will need to think about the choices they make and may want to validate their choices and the overall simulation performance. There are many ways to do this but there are unavoidable limitations to what is possible, especially if the alternative currency is still at the design stage.

Even the performance of similar currencies in the past may be unreliable as a guide to the future because of different economic conditions, attracting different populations of market participants, and even different behaviours (including predatory behaviours) by participants who have learned from past experiences and developed new strategies.


******************************
Overview Of This Documentation
******************************

This documentation is at four levels. It begins with conceptual overview sections.

*   :doc:`overview`
*   :doc:`simulation workflow`
*   :doc:`conceptual reasons`
*   :doc:`conceptual buzz`
*   :doc:`conceptual factors`
*   :doc:`conceptual controls`

Then there are sections about how to work with the simulator, explaining the various parameters and what can be reported.

*   :doc:`parameters timing`
*   :doc:`parameters environment`
*   :doc:`parameters currency`
*   :doc:`parameters users`
*   :doc:`parameters exchanges`
*   :doc:`parameters controls`
*   :doc:`examining results`
*   :doc:`decision factor list`

The agent decisions are explained in more detail in:

*   :doc:`agent decision reference`

And, finally, most of the mathematical formulae and other nuts and bolts are given in:

*   :doc:`tech notes`

.. _further_reading:

***************
Further Reading
***************

This is a useful introductory overview of alternative currencies:

Parliamentary Office of Science & Technology (2014). *Alternative Currencies.* Available online `here`__.

.. __: http://researchbriefings.files.parliament.uk/documents/POST-PN-475/POST-PN-475.pdf

And this is a good example of agent-based simulation used to explore adoption of new technology and it shows some of the ways to do validation when you have relevant past data to work with:

Rai, V., & Robinson, S. A. (2015). Agent-based modeling of energy technology adoption: Empirical integration of social, behavioral, economic, and environmental factors. Environmental Modelling & Software, 70, 163-177. Available online `here`__.

.. __: https://www.sciencedirect.com/science/article/pii/S1364815215001231


