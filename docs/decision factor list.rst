####################
Decision Factor List
####################

.. sectnum:: :start: 14

The factors that can be used in decisions are listed in related groups and defined here. Each can range over a half range (from 0 upwards) or full range (any value) and this is indicated by '(half)' or '(full)' respectively.

Some factors are calculated for individual Users (e.g. concerning economic results from their activity) while others are the same for every user (but of course change over time).


****
Buzz
****

|  *Positive Current Buzz* (half):
|    See :ref:`tech_buzz`.
|  *Negative Current Buzz* (half):
|    See :ref:`tech_buzz`.
|  *Positive Accumulated Buzz* (half):
|    See :ref:`tech_buzz`.
|  *Negative Accumulated Buzz* (half):
|    See :ref:`tech_buzz`.
|  *Total Current Buzz* (half):
|    Positive Current Buzz + Negative Current Buzz.
|  *Current Buzz Positivity* (full):
|    Positive Current Buzz / Negative Current Buzz. A value of 1 if both are zero and a very high value given if only Negative Current Buzz is zero.
|  *Current Buzz Negativity* (full):
|    Negative Current Buzz / Positive Current Buzz. A value of 1 if both are zero and a very high value given if only Positive Current Buzz is zero.
|  *Accumulated Buzz Positivity* (full):
|    Positive Accumulated Buzz / Negative Accumulated Buzz. A value of 1 if both are zero and a very high value given if only Negative Current Buzz is zero.
|  *Accumulated Buzz Negativity* (full):
|    Negative Accumulated Buzz / Positive Accumulated Buzz. A value of 1 if both are zero and a very high value given if only Positive Current Buzz is zero.


*********
User Base
*********

|  *Pool Of Potential Users Size* (half)
|    The current number of potential Users in the Pool Of Potential Users. 
|  *Individual Potential User Count* (half)
|    The current number of Individual Potential Users.
|  *Active User Count* (half):
|    The current number of Active Users.
|  *Speculator Count* (half)
|    The current number of Active Users taking the Speculator role. 
|  *Customer Count* (half)
|    The current number of Active Users taking the Customer role. 
|  *Merchant Count* (half)
|    The current number of Active Users taking the Merchant role. 
|  *Operator Count* (half)
|    The current number of Active Users taking the Operator role. 
|  *Opting Out Count* (half)
|    The current number of Opting Out Users. 


*********************
Personal Propensities
*********************

|  *Propensity To Be A Speculator* (full)
|    The User's propensity to opt in to the role of Speculator. (These are generated randomly using the Normal distribution.)
|  *Propensity To Be A Customer* (full)
|    The User's propensity to opt in to the role of Customer. (These are generated randomly using the Normal distribution.)
|  *Propensity To Be A Merchant* (full)
|    The User's propensity to opt in to the role of Merchant. (These are generated randomly using the Normal distribution.)
|  *Propensity To Be An Operator* (full)
|    The User's propensity to opt in to the role of Operator. (These are generated randomly using the Normal distribution.)
|  *Propensity To Support Exchange* (full)
|    The Exchange's propensity to start offering an exchange service for the alternative currency. (These are generated randomly using the Normal distribution.)


********
Holdings
********

|  *Value Of User Holding* (half)
|    Current value, in the fiat currency, of the User's current holding of alternative currency.
|  *User Holding Of Alternative Currency* (half)
|    The User's current holding of alternative currency.
|  *User Holding Of Fiat Currency* (half)
|    The User's current holding of fiat currency.
|  *Total Alternative Currency Issued* (half)
|    The current total quantity of alternative currency issued.


*********
Economics
*********

|  *User Transaction Fixed Fee* (half):
|    The fixed fee for each transaction paid by Users to Operators, expressed in the fiat currency.
|  *Fee Per Operator* (half):
|    The total transaction fee divided by the current number of Operators.
|  *Number Of Goods Offered* (half):
|    The number of goods offered for sale by the Merchant.
|  *Total For Sale In Alternative Currency* (half):
|    The sum of the fiat currency prices of goods that can be paid for using the alternative currency.
|  *Customer Purchases In Alternative Currency (Volume)* (half)
|    See :ref:`tech_trans_volumes`.
|  *Customer Purchases In Fiat Currency (Volume)* (half)
|    See :ref:`tech_trans_volumes`.
|  *Customer Purchases (Volume)* (half):
|    Customer Purchases In Alternative Currency (Volume) + Customer Purchases In Fiat Currency (Volume).
|  *Merchant Sales In Alternative Currency (Volume)* (half)
|    See :ref:`tech_trans_volumes`.
|  *Merchant Sales In Fiat Currency (Volume)* (half)
|    See :ref:`tech_trans_volumes`.
|  *Operator Transactions (Volume)* (half)
|    See :ref:`tech_trans_volumes`.
|  *Exchange Transactions (Volume)* (half)
|    See :ref:`tech_trans_volumes`.
|  *Speculation Level (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Customer Purchases In Alternative Currency (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Customer Purchases In Fiat Currency (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Merchant Sales In Alternative Currency (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Merchant Sales In Fiat Currency (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Merchant Resupply Purchases (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Operator Transaction Fees (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Operator Expenses (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Exchange Transactions (Value)* (half)
|    See :ref:`tech_trans_values`.
|  *Speculation Gain* (full)
|    See :ref:`tech_trans_gains`.
|  *Buying Gain* (full)
|    See :ref:`tech_trans_gains`.
|  *Selling Gain* (full)
|    See :ref:`tech_trans_gains`.
|  *Exchange Gain* (full)
|    See :ref:`tech_trans_gains`.
|  *Operating Gain* (full)
|    See :ref:`tech_trans_gains`.
|  *Market Making Gain* (full)
|    See :ref:`tech_trans_gains`.
|  *Daily Fiat Currency Adjustments* (full)
|    The daily total value of fiat currency added to the User's holding or taken away by transfers with other wealth.



*****************
Exchange Activity
*****************

|  *Best Competing Exchange Rate To Buy Alternative Currency* (half)
|    (Focuses on the prices where other exchanges are offering to buy alternative currency.) The highest current order to buy price or Bid price on all other exchanges.
|  *Best Competing Exchange Rate To Sell Alternative Currency* (half)
|    (Focuses on the prices where other exchanges are offering to sell alternative currency.) The lowest current order to sell price or Ask price on all other exchanges.
|  *Alternative Currency Buy Volume* (half)
|    Relevant to Market Makers only, the total amount of the fiat currency used to buy alternative currency from the Market Maker in the past 24 hours.
|  *Alternative Currency Sell Volume* (half)
|    Relevant to Market Makers only, the total amount of the fiat currency paid for alternative currency by the Market Maker in the past 24 hours.
|  *Order Level Of Market Maker* (half)
|    The Alternative Currency Buy Volume of the agent plus its Alternative Currency Sell Volume.
|  *Buy Volume / Sell Volume* (half)
|    The Alternative Currency Buy Volume of the agent divided by the Alternative Currency Sell Volume of the agent.
|  *Overall Alternative Currency Buy Volume* (half)
|    The total amount of the fiat currency used to buy alternative currency on all exchanges (Continuous Order Driven and Market Maker) in the past 24 hours.
|  *Overall Alternative Currency Sell Volume* (half)
|    The total amount of the fiat currency paid for alternative currency on all exchanges (Continuous Order Driven and Market Maker) in the past 24 hours.
|  *Overall Buy Volume / Sell Volume* (half)
|    The Overall Alternative Currency Buy Volume divided by the Overall Alternative Currency Sell Volume.
|  *Overall Order Level Of Exchanges* (half)
|    The Overall Alternative Currency Buy Volume plus the Overall Alternative Currency Sell Volume.
|  *Proportion Of Volume Captured* (half)
|    Relevant to Market Makers only, the Order Level Of Market Maker divided by the Overall Order Level Of Exchanges.
|  *Stock Of Alternative Currency* (half)
|    The agent's current holding of alternative currency, expressed in alternative currency.
|  *Velocity Of Alternative Currency Stock Change* (full)
|    The agent's current Stock Of Alternative Currency less the agent's Stock Of Alternative Currency 24 hours earlier.
|  *Acceleration Of Alternative Currency Stock Change* (full)
|    The agent's current Velocity Of Alternative Currency Stock Change less the agent's Velocity Of Alternative Currency Stock Change 24 hours earlier.
|  *Speed Of Alternative Currency Stock Change* (half)
|    The absolute value of the Velocity Of Alternative Currency Stock Change.
|  *Deviation From Target Stock Of Alternative Currency* (full)
|    The agent's current holding of alternative currency less its current target stock of alternative currency, expressed in alternative currency. (Relevant for Exchanges that are Market Makers.)




**************************************
Alternative Currency Valuation History
**************************************

|  *Alternative Currency Value* (half)
|    The current value of the alternative currency expressed in fiat currency according to the rules of the Alternative Currency Valuation (see :ref:`tech_valuation_history`).
|  *SD Of Alternative Currency Value* (half):
|    The standard deviation of the Alternative Currency Daily Value History over the past 28 days (or fewer if 28 days are not available).
|  *Trend Strength* (full):
|    Definition depends on the Speculator's extrapolation rule:
|      - Recency Weighted Average Daily Percentage Gain: The average daily percentage increase in value (based on the Alternative Currency Daily Value History, see :ref:`tech_valuation_history`), but with a weighting that puts more importance on the most recent value changes. Only available to Speculators using the Continue Trend extrapolation rule.
|      - Recency Weighted Average Daily Percentage Loss: -1 :math:`\times` Recency Weighted Average Daily Percentage Gain. Only available to Speculators using the Contrarian extrapolation rule.
|      - Current Value Digression: The difference between the average value over the past 28 days (or fewer if 28 are not available) and the current value (all using the Alternative Currency Daily Value History, see :ref:`tech_valuation_history`). Only available to Speculators using the Regress To Mean extrapolation rule.


