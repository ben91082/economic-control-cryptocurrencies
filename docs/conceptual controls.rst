##################
Control Mechanisms
##################

.. sectnum:: :start: 6

.. _control_stocks:

***************************
Controlling Currency Stocks
***************************

A number of potentially important control mechanisms require a Controller that takes currency in and sends it out. In real life this might be a done by a group of people or it might be automated. In the simulator, the Controller is an agent controlled by you, the user of the simulator. The control mechanisms are as follows:

*   *Gift Freshly Minted:* Users may receive a fixed amount of alternative currency, freshly minted for the purpose, when they first become Active Users.
*   *Sell Freshly Minted:* Users buy newly minted alternative currency from the Controller using fiat currency.
*   *Buy Back And Melt:* Users sell alternative currency back to the Controller and receive fiat currency; the Controller then destroys the alternative currency.
*   *Sell From Holding:* Users buy existing alternative currency from the Controller using fiat currency.
*   *Buy Back And Hold:* Users sell alternative currency back to the Controller and receive fiat currency; the Controller does not destroy the alternative currency.
*   *Operator Payment In Freshly Minted:* The Controller pays Operators for processing transactions using newly minted alternative currency.
*   *Control Catalogue:* The Controller buys goods (wholesale) using fiat currency and sells them to alternative currency users in return for alternative currency.

The Controller has to be careful to manage three stocks of currency:

*   *Fiat Stock:* its own stock of fiat currency.
*   *Alt Stock:* its own stock of alternative currency.
*   *Alt Issued:* the total stock of alternative currency issued (including its own stocks).

The effects of the methods can be summarised in this table.

.. |nbsp| unicode:: 0xA0 
   :trim:

.. |plus| replace:: |nbsp| |nbsp| |nbsp| |nbsp| |nbsp| |nbsp| |nbsp| **+** |nbsp| |nbsp| |nbsp| |nbsp|
.. |minus| replace:: |nbsp| |nbsp| |nbsp| |nbsp| |nbsp| |nbsp| |nbsp| **---** |nbsp| |nbsp| |nbsp| |nbsp|

+------------------------------------+------------+-----------+------------+
| Method                             | Fiat Stock | Alt Stock | Alt Issued |
+====================================+============+===========+============+
| Gift Coins Minted                  |            |           | |plus|     |
+------------------------------------+------------+-----------+------------+
| Sell Freshly Minted                | |plus|     |           | |plus|     |
+------------------------------------+------------+-----------+------------+
| Buy Back And Melt                  | |minus|    |           | |minus|    |
+------------------------------------+------------+-----------+------------+
| Sell From Holding                  | |plus|     | |minus|   |            |
+------------------------------------+------------+-----------+------------+
| Buy Back And Hold                  | |minus|    | |plus|    |            |
+------------------------------------+------------+-----------+------------+
| Operator Payment In Freshly Minted |            |           | |plus|     |
+------------------------------------+------------+-----------+------------+
| Control Catalogue                  | |minus|    | |plus|    |            |
+------------------------------------+------------+-----------+------------+
| Generating Buzz                    | |minus|    |           |            |
+------------------------------------+------------+-----------+------------+


Overall, the Controller's position requires careful management but in general it should:

*   gain fiat as long as more alternative currency is issued than taken back (though it might have to hold on to it if using it to back the alternative currency)
*   gain fiat if selling goods profitably through the Control Catalogue
*   defend against predatory tactics by traders trying to exploit control mechanisms.

Ultimately, a lot depends on developing a more efficient payment system and/or some non-financial benefits.

For detail on the parameter settings for these controls see :ref:`p_contr_stocks`.


.. _control_buzz:

***************
Generating Buzz
***************

The Controller can also increase or decrease the level of Positive Current Buzz. This costs fiat currency, so cannot be done to an unlimited extent. This is mentioned in the table of control mechanisms involving stocks of currency.

For detail on the parameter settings for these controls see :ref:`p_contr_buzz`.

.. _control_regulating_transactions:

***********************
Regulating Transactions
***********************

The Controller can also set rates that might have a useful controlling effect:

*   transaction fees paid by Users
*   choosing the rule used by a Market Maker that is a controlled Exchange to revise exchange rates (affecting sensitivity).

For detail on the parameter settings for these controls see :ref:`p_contr_other`.


***************
Further Reading
***************

A very interesting discussion of the idea of restricting the supply of alternative currency coins to manage the money supply:

Selgin, G. (2015). Synthetic commodity money. *Journal of Financial Stability*, 17, 92-99. Available online `here`__.

.. __: http://www.article78againstnydfs.com/docs/VariousDocs/synthetic-commodity-money.pdf

