##########################
Parameters --- Environment
##########################

.. sectnum:: :start: 8

These settings specify the economic environment in which the alternative currency is imagined to be operating.


.. _p_env_goods:

*****
Goods
*****

Merchants and the Control Catalogue (if used) offer real goods for sale to Customers. In real life these may be physical goods or services. The range of goods and their underlying prices in the fiat currency are held constant throughout simulation trials and no attempt is made to model inflation in the fiat currency.

Although the underlying fiat currency price is fixed for each good, the actual fiat currency price offered by a Merchant may be slightly different because of transaction fees. Also, alternative currency prices offered are affected by both transaction fees and changes in the value of the alternative currency relative to the fiat currency. The Controller can set its own 'exchange rate' for goods offered in the Control Catalogue but still pays transaction fees like everyone else.

Some goods have a substitute good that is similar but a bit better and that can only be purchased using the alternative currency. These goods may provide a motive for some Users opting in. In real life these 'goods' might be sessions in an online game or casino, for example. The Control Catalogue may also offer goods that cannot be offered by any Merchant.

Goods that have no substitute are called **Lone Goods**. Goods that have a substitute are called **Paired Goods**. Goods that can only be offered by the Control Catalogue are called **Exclusive Goods**. Goods can be priced in fiat currency, or in both the fiat currency and the alternative currency. Substitutes can only be priced in alternative currency.

The settings are:

*   **Number Of Lone Goods:** This is the number of different types of good that can be offered by Merchants and the Control Catalogue to be priced in either fiat currency or fiat *and* alternative currency (at the Merchant/Controller's option). These goods have no substitute available exclusively in alternative currency. This should be kept quite small e.g. 10.
*   **Number Of Paired Goods:** This is the number of goods with a paired substitute. These pairs will be given the same fiat currency price but have different values in the eyes of Users.
*   **Lowest Price Of A Good:** The lowest price of a good that any Merchant/Control Catalogue can offer, expressed in the fiat currency. Must be more than zero and specified with no more than the appropriate number of decimal places for the fiat currency.
*   **Mean Price Of A Good:** The mean price of all goods offered, expressed in the fiat currency. Must be more than the Lowest Price Of A Good, and specified with no more than the appropriate number of decimals.
*   **Median Price Of A Good:** The median price of all goods offered, expressed in the fiat currency. Must be more than the Lowest Price Of A Good, and specified with no more than the appropriate number of decimals.

From this information the system randomly generates a set of goods with initial prices in the fiat currency. See :ref:`adr_env_goods`. The Control Catalogue's count of Exclusive Goods is set in :ref:`p_contr_stocks`.

A final setting is:

*   **Cost Multiple For Goods:** This is a number that, when multiplied onto the underlying price of a good, gives its cost to the Merchant or Control Catalogue. This is simply a standard number for the whole simulation and is used to give an indication of profits resulting from sales. The default is 0.75.


.. _p_env_fiat:

*************
Fiat Currency
*************

In the simulator there is a fiat currency alongside the alternative currency. The settings for this are as follows:

*   **FC Name:** This is for the name of the fiat currency. The default is 'Pounds'.
*   **FC Abbreviation:** This is for an abbreviation of the name of the fiat currency and is the most commonly used way to identify it on exchanges and elsewhere. Usually it has three letters (as with USD, GBP, RMB, and BTC for example). The default is 'FC1'.
*   **FC Symbol:** This is a single character symbol for the fiat currency used as with symbols like $ and £. The default is 'F'.
*   **FC Decimal Places:** This is the maximum number of decimal places used to state amounts in the fiat currency. For example, USD, GBP, and EUR all use two decimal places. Bitcoin has eight.
*   **FC User Paid Fixed Transaction Fee** The fixed fee paid in fiat currency by users of the fiat currency for each transaction.
*   **FC User Paid Percentage Transaction Fee** The percentage fee paid in fiat currency by users of the fiat currency for each transaction.
*   **FC Who Pays Fees:** This is who pays the transaction fees for a payment for goods using the fiat currency. The values are Customer and Merchant, so either Customer Pays Fees or Merchant Pays Fees. (See :ref:`tech_trans_fees_purch` for full details.)

This **fiat currency** has special significance in the simulator because it is the main measure of value, assumed to have the same purchasing power throughout a Trial.


.. _p_env_buzz_gen:

***************
Buzz Generation
***************
For more detailed information on Buzz, see :ref:`tech_buzz`. For an overview see :doc:`conceptual buzz`.

Some important settings decide how much Buzz is generated in Trials. The table to fill in looks like this:

+----------------------------+------------------+---------------+---------------+-------------+
|                            | | Speculators &  | | Merchants & | | Journalists | | Promoters |
|                            | | Customers      | | Operators   | |             | |           |
+============================+==================+===============+===============+=============+
| +ve Transient Message Rate |                  |               |               |             |
+----------------------------+------------------+---------------+---------------+-------------+
| +ve Enduring Message Rate  |                  |               |               |             |
+----------------------------+------------------+---------------+---------------+-------------+
| +ve Enduring Reading Rate  |                  |               |               |             |
+----------------------------+------------------+---------------+---------------+-------------+
| -ve Transient Message Rate |                  |               |               |             |
+----------------------------+------------------+---------------+---------------+-------------+
| -ve Enduring Message Rate  |                  |               |               |             |
+----------------------------+------------------+---------------+---------------+-------------+
| -ve Enduring Reading Rate  |                  |               |               |             |
+----------------------------+------------------+---------------+---------------+-------------+

For each group of people generating messages that contribute to Buzz there are settings for positive messages and settings for negative messages. The meanings are the same in each case:

*   **Transient Message Rate:** This is the average number of transient messages *per year* generated by the group collectively, per £100,000 of total alternative currency holdings of all holders. (This is the value of all the alternative currency held by everyone expressed in the fiat currency.) The idea is that people are more motivated to create and send messages (especially positive messages) when they hold more valuable alternative currency and that interest in the currency generally is greater when it is more valuable in total. These transient messages may be emails or conversations, but are not messages that stay visible, such as social media/blog postings, articles, and reports.
*   **Enduring Message Rate:** This is the average number of enduring messages *per year* generated by the group collectively, per £100,000 of total alternative currency holdings of all holders. (This is the value of all the alternative currency held by everyone expressed in the fiat currency.) These enduring messages may be social media/blog postings, articles, and reports i.e. messages that remain visible for a long time. It does not matter to this number how many people read each message.
*   **Enduring Reading Rate:** This is the average number of readings per enduring message within the *first 24 hours* of the message being released. For example, if an article is posted on a news website and is read by 543 people in the first 24 hours then its reading rate is 543. However, the setting number needs to be the average reading rate over all enduring messages.


.. _p_env_buzz_accum:

*****************
Buzz Accumulation
*****************

Positive and negative enduring messages are consolidated into Postive Accumulated Buzz and Negative Accumulated Buzz, representing enduring messages available to look at on the internet.

The Accumulated Buzz figures are updated at the end of each day by depreciating the current Accumulated Buzz by one day then adding the relevant enduring message rates for the day.

Buzz accumulation is controlled by the following settings:

*   **Initial Positive Accumulated Buzz:** This is the initial value of Positive Accumulated Buzz at go-live of the alternative currency, representing what has accumulated up to that point.
*   **Initial Negative Accumulated Buzz:** This is the initial value of Negative Accumulated Buzz at go-live of the alternative currency, representing what has accumulated up to that point.
*   **Half Life Of Enduring Messages:** This is the number of days it takes for the value given to a message within Positive and Negative Accumulated Buzz to reduce by 50%. This might be because the message is deleted, becomes obsolete, or becomes less credible. From this the simulator works out the daily depreciation of messages.


.. _p_env_news:

*****************
Newsworthy Events
*****************

In the simulator, newsworthy events that modify either Positive or Negative Current Buzz (and later, Positive Accumulated Buzz and Negative Accumulated Buzz) can be generated from time to time by the environment both (a) in response to simulated decisions by Exchange operators and (b) at random. Exchange operators deciding to support, or stop supporting, the currency can be important news stories. Random stories might be described as 'Security Worry', 'Crime Discovered', 'Arrest Made', and so on. They can be positive or negative.

There are two settings that determine the frequency of positive and negative *random* events:

*   **Average Days Between Positive Events:** This is the average number of days between positive newsworthy events. For a sense of realism, these events are randomly given the descriptions 'Regulatory Support' or 'Retailer Support', but these descriptions have no significance in the simulation.
*   **Average Days Between Negative Events:** This is the average number of days between negative newsworthy events. For a sense of realism, these events are randomly given the descriptions 'Regulatory Restriction', 'Security Worry', 'Crime Discovered', or 'Arrest Made', but these descriptions have no significance in the simulation.

See :ref:`tech_delays` for more information on how random time delays are generated, and :ref:`adr_env_when_news`, :ref:`adr_env_news`, and :ref:`adr_env_news_exch` for use in decisions.

The effect of an event is reflected in Buzz by multiplying the Positive and Negative Current Buzz by multipliers related to the event. The same is done for Accumulated Buzz figures. The idea is that the effect of an event on Buzz will be larger for currencies that already have higher Buzz. A multiplier of 1 will cause no change to Buzz. A multiplier of 2 will double it. The multipliers are taken at random from within a range determined by a **Lowest** and **Highest** value for the multiplier, with uniform probability distribution. (See :ref:`tech_buzz` for details of the calculations.)

The settings are in the table looking like this:

+----------------------------+---------------------+---------------------+
| Event type                 | +ve Buzz Multiplier | -ve Buzz Multiplier |
+----------------------------+----------+----------+----------+----------+
|                            | Lowest   | Highest  | Lowest   | Highest  |
+----------------------------+----------+----------+----------+----------+
| Exchange Support Increase  |          |          |          |          |
+----------------------------+----------+----------+----------+----------+
| Exchange Support Decrease  |          |          |          |          |
+----------------------------+----------+----------+----------+----------+
| Random Positive Event      |          |          |          |          |
+----------------------------+----------+----------+----------+----------+
| Random Negative Event      |          |          |          |          |
+----------------------------+----------+----------+----------+----------+


