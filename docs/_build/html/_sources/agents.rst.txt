Agents
======

This is a page describing agent decisions. 

We can put inline math symbols as below

When :math:`a \ne 0`, there are two solutions to :math:`ax^2 + bx + c = 0` and they are
:math:`x = {-b \pm \sqrt{b^2-4ac} \over 2a}`.

Or blocks of equations as below

.. math::
    
    x = {-b \pm \sqrt{b^2-4ac} \over 2a}

    x = {-b \pm \sqrt{b^2-4ac} \over 2a}


.. math::

    \langle \alpha, \beta  \rangle
    \in
    \Biggl \lbrace
    {
    M,\text{ if }
    {
        l(\underline{x}) =
        \frac { p(\underline{x}|M ) } { p(\underline{x}|U) }
        \geq
        \frac { p(U) }{ p(M) } }
    \atop
    U, \text{ otherwise }
    }

enough math now
