########################
Parameters --- Exchanges
########################

.. sectnum:: :start: 11

These are for choosing the features of the currency Exchange or Exchanges to be used in the simulation, allowing User agents to exchange alternative currency and fiat currency.


.. _p_exch_agents:

***************
Exchange Agents
***************

The simulation may contain 1, 2 or 3 Exchanges. One Exchange may be 'owned' by the operators of the alternative currency and will be available at all times and under the direction of the Controller. The other Exchanges must be independent and, although their properties are determined by the parameter settings below, their agents will decide during the simulation which currency exchanges they will support on their Exchanges.

The settings for this are as follows:

*   **Number Of Exchanges:** This can be 1, 2, or 3. The default is 1.
*   **E1 Controlled:** This should be 'Yes' if the first Exchange is controlled by the promoters of the alternative currency, and 'No' otherwise. If it is controlled then the Exchange will always try to offer exchange between the alternative currency and all fiat currencies in the Trial. If it is not controlled then the Exchange agent will decide what to support as the Trial unfolds.


.. _p_exch_common:

****************************
Settings Common To Exchanges
****************************

.. _p_exch_common_when_offer:

When next to consider offering an Exchange service for the alternative currency
-------------------------------------------------------------------------------

Either the Exchange is offering an Exchange service already and will next consider stopping, or it is not offering a service and will next consider starting.

The delay to the next consideration is exponentially distributed (see :ref:`tech_delays`) with an average delay determined by the following settings:

*   **Exchange Support Default Average Delay:** The initial average delay in days.
*   **Exchange Support Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors for Exchanges in this decision are:

    *   SD Of Alternative Currency Value with :math:`\text{half-desc}`
    *   Total Current Buzz with :math:`\text{half-desc}`
    *   Value Of User Holding with :math:`\text{half-desc}`
    *   Average Daily Purchases with :math:`\text{half-desc}`

See :ref:`adr_exch_common_when_offer` for use in decisions.


.. _p_exch_common_start:

Whether to start offering an Exchange service for the alternative currency
--------------------------------------------------------------------------

The decision is similar to an opt in decision by a User. The decision is random, but with a probability of opting in that is controlled by a number of criteria.

*   **Exchange Support Start Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can decrease the probability of starting to offer an exchange service for the alternative currency. Suggested factors for the Exchange start support decision are as follows:

    *   Propensity To Support Exchange with :math:`\text{full-asc-1}`
    *   Current Buzz Positivity with :math:`\text{half-asc-1}`
    *   Accumulated Buzz Positivity with :math:`\text{half-asc-1}`
    *   Active User Count with :math:`\text{half-asc-1}`
    *   Value Of User Holding with :math:`\text{half-asc-1}`
    *   Average Daily Purchases with :math:`\text{half-asc-1}`

See :ref:`adr_exch_common_start` for details.

Having started offering an Exchange service the Exchange will offer all possible exchange pairs involving the alternative currency.


.. _p_exch_common_stop:

Whether to stop offering an Exchange service for the alternative currency
-------------------------------------------------------------------------

The decision to stop support is similar to the decision to start, but the directions are reversed. It starts with a probability of 1 for stopping, but this is reduced by multipliers driven by the decision factors. The factors considered can be chosen and then linked to multiplier values using calibration function types and levels. See :ref:`adr_exch_common_stop` for use in decisions.

*   **Exchange Support Stop Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can decrease the probability of deciding to stop offering an Exchange service for the alternative currency. Suggested factors for the Exchange stop support decision are as follows:

    *   Propensity To Support Exchange with :math:`\text{full-desc-1}`
    *   Current Buzz Positivity with :math:`\text{half-desc-1}`
    *   Accumulated Buzz Positivity with :math:`\text{half-desc-1}`
    *   Active User Count with :math:`\text{half-desc-1}`
    *   Value Of User Holding with :math:`\text{half-desc-1}`
    *   Average Daily Purchases with :math:`\text{half-desc-1}`

Having stopped, all exchange pairs are withdrawn and any existing unsettled orders are cancelled.


.. _p_exch_1:

***********************
Settings For Exchange 1
***********************

*   **E1 Name:** The name of the first Exchange.
*   **E1 Propensity:** The value of the Exchange's propensity to support the alternative currency. This is not relevant if E1 is Controlled.
*   **E1 Type:** The alternatives are 'Market Maker' and 'Continuous Order Driven'. The default is 'Continuous Order Driven', which is typical for cryptocurrency exchanges. The price is driven by the ebb and flow of orders placed by users hoping to buy or sell within price ranges they specify in their orders. The 'Market Maker' type is where an agent owns a significant stock of the currency and offers prices for buying and selling.

If this Exchange is a Market Maker then additional settings are required to define its autonomous decision making behaviour:

*   **E1 MM Who Pays Fees:** This is who pays the transaction fees for a transaction on the first Exchange, if it is a Market Maker. The values are Exchange, User, and Initiators (i.e. the party sending each currency pays the transaction fee for that movement). For Continuous Order Driven Exchanges the fees are always paid by each initiator. (See :ref:`tech_trans_fees_exch` for full details.)
*   **E1 MM Initial Fiat Currency Holding:** This is the amount of fiat currency the Market Maker has initially. (See :ref:`tech_minting` for all the ways that alternative currency can be minted.)
*   **E1 MM Initial Alternative Currency Target Holding:** If the Exchange type is Market Maker then it may use a Target as part of its deliberations over how much alternative currency to occur. This parameter gives just the initial target.

Also, for the Market Maker decision on when to set/revise prices (see :ref:`adr_exch_MM_when_price`), the delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **E1 MM Price Default Average Delay:** The initial average delay in days.
*   **E1 MM Price Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types are:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-asc}`.
    *   Speed Of Alternative Currency Stock Change with :math:`\text{half-desc}`.
    *   Overall Order Level Of Exchanges with :math:`\text{half-desc}`.

Two decision rules are needed for revising prices (see :ref:`adr_exch_MM_price`), each requiring a multiplier definition:

*   **E1 MM Price Mean Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the geometric mean of the Bid and Ask prices from their previous values. Although the choice of decision factors is open to users of the simulator to decide, it is strongly recommended to use (at least) the suggested decision factors, with calibration function types. For adjusting the geometric mean of the Bid and Ask prices:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-desc}` calibrated to strongly push prices up when stocks are low so that more alternative currency will flow in.
    *   Deviation From Target Stock Of Alternative Currency with :math:`\text{full-desc}`.
    *   Velocity Of Alternative Currency Stock Change with :math:`\text{full-desc}`.
    *   Acceleration Of Alternative Currency Stock Change with :math:`\text{full-desc}`.
    *   Proportion Of Volume Captured* with :math:`\text{half-desc}`.

*   **E1 MM Price Ratio Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the ratio of the Ask price over the Bid price. Suggestions for this are:

    *   Estimated Exchange Profit Per Transaction with :math:`\text{full-desc}`.
    *   Proportion Of Volume Captured with :math:`\text{half-asc}`.

Other decision factors that might be experimented with for these two multiplier definitions include:

*   Best Competing Exchange Rate
*   Average Competing Exchange Rate
*   Order Level Of Market Maker
*   Overall Order Level Of Exchanges
*   Buy Volume / Sell Volume
*   Current Buzz Positivity

Also for a Market Maker, if a target for alternative currency holding is used, then it will be reconsidered after a delay (see :ref:`adr_exch_MM_when_target`) defined by:

*   **E1 MM Target Holding Default Average Delay:** The initial average delay in days.
*   **E1 MM Target Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggestions are:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-asc}`.
    *   Speed Of Alternative Currency Stock Change with :math:`\text{half-desc}`.
    *   Overall Order Level Of Exchanges with :math:`\text{half-desc}`.

The target holding itself (see :ref:`adr_exch_MM_target`) needs a multiplier definition:

*   **E1 MM Target Holding Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the target holding of alternative currency from its previous value. Suggested decision factors and calibration function types are as follows:

    *   Overall Order Level Of Exchanges with :math:`\text{half-asc}`.
    *   Total Current Buzz with :math:`\text{half-asc}`.
    *   SD Of Alternative Currency Value with :math:`\text{half-asc}`.


.. _p_exch_2:

***********************
Settings For Exchange 2
***********************

If there is a second Exchange:

*   **E2 Name:** The name of the second Exchange.
*   **E2 Propensity:** The value of the Exchange's propensity to support the alternative currency.
*   **E2 Type:** The alternatives are the same as for the first Exchange.

If this Exchange is a Market Maker then additional settings are required to define its autonomous decision making behaviour:

*   **E2 MM Who Pays Fees:** This is who pays the transaction fees for a transaction on the second Exchange, if it is a Market Maker. The values are Exchange, User, and Initiators (i.e. the party sending each currency pays the transaction fee for that movement). For Continuous Order Driven Exchanges the fees are always paid by each initiator. (See :ref:`tech_trans_fees_exch` for full details.)
*   **E2 MM Initial Fiat Currency Holding:** This is the amount of fiat currency the Market Maker has initially.
*   **E2 MM Initial Alternative Currency Target Holding:** If the Exchange type is Market Maker then it may use a Target as part of its deliberations over how much alternative currency to occur. This parameter gives just the initial target.

Also, for the Market Maker decision on when to set/revise prices (see :ref:`adr_exch_MM_when_price`), the delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **E2 MM Price Default Average Delay:** The initial average delay in days.
*   **E2 MM Price Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types are:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-asc}`.
    *   Speed Of Alternative Currency Stock Change with :math:`\text{half-desc}`.
    *   Overall Order Level Of Exchanges with :math:`\text{half-desc}`.

Two decision rules are needed for revising prices (see :ref:`adr_exch_MM_price`), each requiring a multiplier definition:

*   **E2 MM Price Mean Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the geometric mean of the Bid and Ask prices from their previous values. Although the choice of decision factors is open to users of the simulator to decide, it is strongly recommended to use (at least) the suggested decision factors, with calibration function types. For adjusting the geometric mean of the Bid and Ask prices:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-desc}` calibrated to strongly push prices up when stocks are low so that more alternative currency will flow in.
    *   Deviation From Target Stock Of Alternative Currency with :math:`\text{full-desc}`.
    *   Velocity Of Alternative Currency Stock Change with :math:`\text{full-desc}`.
    *   Acceleration Of Alternative Currency Stock Change with :math:`\text{full-desc}`.
    *   Proportion Of Volume Captured* with :math:`\text{half-desc}`.

*   **E2 MM Price Ratio Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the ratio of the Ask price over the Bid price. Suggestions for this are:

    *   Estimated Exchange Profit Per Transaction with :math:`\text{full-desc}`.
    *   Proportion Of Volume Captured with :math:`\text{half-asc}`.

Other decision factors that might be experimented with for these two multiplier definitions include:

*   Best Competing Exchange Rate
*   Average Competing Exchange Rate
*   Order Level Of Market Maker
*   Overall Order Level Of Exchanges
*   Buy Volume / Sell Volume
*   Current Buzz Positivity

Also for a Market Maker, if a target for alternative currency holding is used, then it will be reconsidered after a delay (see :ref:`adr_exch_MM_when_target`) defined by:

*   **E2 MM Target Holding Default Average Delay:** The initial average delay in days.
*   **E2 MM Target Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggestions are:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-asc}`.
    *   Speed Of Alternative Currency Stock Change with :math:`\text{half-desc}`.
    *   Overall Order Level Of Exchanges with :math:`\text{half-desc}`.

The target holding itself (see :ref:`adr_exch_MM_target`) needs a multiplier definition:

*   **E2 MM Target Holding Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the target holding of alternative currency from its previous value. Suggested decision factors and calibration function types are as follows:

    *   Overall Order Level Of Exchanges with :math:`\text{half-asc}`.
    *   Total Current Buzz with :math:`\text{half-asc}`.
    *   SD Of Alternative Currency Value with :math:`\text{half-asc}`.




.. _p_exch_3:

***********************
Settings For Exchange 3
***********************

If there is a third Exchange:

*   **E3 Name:** The name of the third Exchange.
*   **E3 Propensity:** The value of the Exchange's propensity to support the alternative currency.
*   **E3 Type:** The alternatives are the same as for the first Exchange.

If this Exchange is a Market Maker then additional settings are required to define its autonomous decision making behaviour:

*   **E3 MM Who Pays Fees:** This is who pays the transaction fees for a transaction on the third Exchange, if it is a Market Maker. The values are Exchange, User, and Initiators (i.e. the party sending each currency pays the transaction fee for that movement). For Continuous Order Driven Exchanges the fees are always paid by each initiator. (See :ref:`tech_trans_fees_exch` for full details.)
*   **E3 MM Initial Fiat Currency Holding:** This is the amount of fiat currency the Market Maker has initially.
*   **E3 MM Initial Alternative Currency Target Holding:** If the Exchange type is Market Maker then it may use a Target as part of its deliberations over how much alternative currency to occur. This parameter gives just the initial target.

Also, for the Market Maker decision on when to set/revise prices (see :ref:`adr_exch_MM_when_price`), the delay to the next consideration is exponentially distributed with an average delay determined by the following settings:

*   **E3 MM Price Default Average Delay:** The initial average delay in days.
*   **E3 MM Price Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggested decision factors and calibration function types are:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-asc}`.
    *   Speed Of Alternative Currency Stock Change with :math:`\text{half-desc}`.
    *   Overall Order Level Of Exchanges with :math:`\text{half-desc}`.

Two decision rules are needed for revising prices (see :ref:`adr_exch_MM_price`), each requiring a multiplier definition:

*   **E3 MM Price Mean Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the geometric mean of the Bid and Ask prices from their previous values. Although the choice of decision factors is open to users of the simulator to decide, it is strongly recommended to use (at least) the suggested decision factors, with calibration function types. For adjusting the geometric mean of the Bid and Ask prices:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-desc}` calibrated to strongly push prices up when stocks are low so that more alternative currency will flow in.
    *   Deviation From Target Stock Of Alternative Currency with :math:`\text{full-desc}`.
    *   Velocity Of Alternative Currency Stock Change with :math:`\text{full-desc}`.
    *   Acceleration Of Alternative Currency Stock Change with :math:`\text{full-desc}`.
    *   Proportion Of Volume Captured* with :math:`\text{half-desc}`.

*   **E3 MM Price Ratio Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the ratio of the Ask price over the Bid price. Suggestions for this are:

    *   Estimated Exchange Profit Per Transaction with :math:`\text{full-desc}`.
    *   Proportion Of Volume Captured with :math:`\text{half-asc}`.

Other decision factors that might be experimented with for these two multiplier definitions include:

*   Best Competing Exchange Rate
*   Average Competing Exchange Rate
*   Order Level Of Market Maker
*   Overall Order Level Of Exchanges
*   Buy Volume / Sell Volume
*   Current Buzz Positivity

Also for a Market Maker, if a target for alternative currency holding is used, then it will be reconsidered after a delay (see :ref:`adr_exch_MM_when_target`) defined by:

*   **E3 MM Target Holding Default Average Delay:** The initial average delay in days.
*   **E3 MM Target Holding Delay Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the average delay from the initial default. Suggestions are:

    *   Stock Of Alternative Currency (which cannot go below zero) with :math:`\text{half-asc}`.
    *   Speed Of Alternative Currency Stock Change with :math:`\text{half-desc}`.
    *   Overall Order Level Of Exchanges with :math:`\text{half-desc}`.

The target holding itself (see :ref:`adr_exch_MM_target`) needs a multiplier definition:

*   **E3 MM Target Holding Multiplier Definitions:** The decision factors, calibration function types, and calibration levels that define multipliers for your chosen decision factors that can both increase and decrease the target holding of alternative currency from its previous value. Suggested decision factors and calibration function types are as follows:

    *   Overall Order Level Of Exchanges with :math:`\text{half-asc}`.
    *   Total Current Buzz with :math:`\text{half-asc}`.
    *   SD Of Alternative Currency Value with :math:`\text{half-asc}`.

