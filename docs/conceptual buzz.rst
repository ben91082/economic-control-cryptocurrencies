##################
Understanding Buzz
##################

.. sectnum:: :start: 4

Interest in, and valuation of, alternative currencies is influenced by information reaching users and potential users. This takes the form of newspaper stories, blog posts, video posts, television news stories, emails and texts from friends and other contacts, conversations, and so on. These messages collectively, excluding statistics about the market such as market price and volume, are represented by the idea of **Buzz**.  A user's view of the alternative currency is conceptually different and modelled in other ways. (In academic literature on this subject the word 'sentiment' is more often used, but this potentially confuses users' views with the news they are receiving. Buzz is about the news.)

***************
Real World Buzz
***************

The following are some observations about Buzz in real life. With some simplifying exceptions, the Buzz scheme implemented by the simulator reflects these.

Positive And Negative Information
=================================

The messages will include positive and negative information. Positive information encourages use and higher valuation of the alternative currency. Negative information discourages use and argues for lower valuation of the alternative currency.

Positive information is likely to be generated steadily, with the occasional positive event such as a supportive announcement by an exchange. Negative information may also be generated steadily, especially if there is a general swing away from a currency or a spreading realisation of a problem with it (e.g. the inefficiency and electricity consumption of Bitcoin). However, negative information is also more likely to be received due to shocking events such as crimes and regulatory restrictions.

Sources Of Information
======================

Positive messages are likely to be produced by:

*   existing users, especially those with valuable holdings, customers wishing to use the currency and wanting merchants to accept it, and merchants hoping to gain new customers
*   journalists
*   people offering services to support the currency, such as software and exchange services
*   currency promoters, often the ones behind the technology.

Negative messages are likely to be produced by:

*   ex-users of the alternative currency, perhaps having realised some drawback with it
*   journalists
*   promoters of competing alternative currencies.

Quantity And Influence
======================

The influencing effect of messages received depends on both their quantity and other aspects, such as the source, the medium, and what is said. Modelling these intricacies is too complex for the simulator so only the simplest aspects will be represented and randomisation used to represent the other complexities.

Changes Over Time
=================

At any point in time some potential users will be attending to messages about alternative currencies more than others. They will also have a certain level of familiarity with the field. Over time, this can change, with people either losing or gaining interest, and familiarity either rising or falling. Although the simulator represents Buzz as changing, it does not attempt to model changing familiarity of people.


Receiving And Searching For Information
=======================================

People passively receive messages initiated by others and also actively search for information.

For someone who knows nothing about an alternative currency to become a user there must be some prompt to get them interested (typically passively received), and enough information for them to find and consider for them to make up their minds (typically, actively searched for). Also, that information they find must be sufficiently positive about the currency for them to decide to try it. Those potential users will range from those who already know a lot about other alternative currencies and are regularly scanning for information about new ones to those who have no previous interest and perhaps are just prompted by a friend to take a look at something confusingly new.

The initial prompt might be, for example:

*   an email from a friend
*   a conversation with a work colleague who talks about how she started using the new currency
*   a YouTube video noticed while browsing in an idle moment
*   an article on a specialist online news site about alternative currencies
*   seeing the currency listed for the first time on a currency exchange.


Current Rate Of New Messages And Accumulated Messages
=====================================================

These prompts include messages that are sent and then forgotten (e.g. conversations and emails). It also includes items that have been published recently, such as new videos on YouTube being offered to browsers. Finally, it includes material that may have been published some time ago but can still be found on the internet. The tendency is for these prompts to be dominated by the current creation of messages about the alternative currency.

The further information they find and study might include, for example:

*   a website set up to promote the alternative currency
*   articles on specialist news sites about alternative currencies
*   blog postings about the currency
*   market statistics about the currency, such as the past exchange values and number of users.

These information sources are not so dominated by current output of messages and even quite old information can be influential if it is clear, helpful, and positive.

Clearly there can be a huge difference between the size of the accumulated body of messages and the current rate of new messages. Both will affect the rate at which new people consider becoming users and the results of their deliberations.


***************
The Buzz Scheme
***************

This is how Buzz has been implemented in the simulator. Details of the calculations used are given in :ref:`tech_buzz`.

Buzz Generation
===============

Conceptually, Buzz is modelled as a stream of positive messages and a stream of negative messages emitted by various people in proportion to the total holdings of the alternative currency, valued in the first fiat currency. These messages are of two types: **transient** and **enduring**. Transient messages, such as conversations, are received and then disappear from view. Enduring messages, such as blog posts, are created then remain public for a long period.

Enduring messages also tend to be messages that are sent to many people at the same time, such as news articles. The number of messages currently being received is a function of the transient messages and the enduring messages multiplied by the number of times the enduring messages are read in the first 24 hours, on average.

For each group of people generating messages that contribute to Buzz there are settings for positive messages and settings for negative messages. The meanings are the same in each case:

*   **Transient Message Rate:** This is the average number of transient messages per year generated by the group collectively, per £100,000 of total alternative currency holdings of all holders. (This is the value of all the alternative currency held by everyone expressed in the first fiat currency.) The idea is that people are more motivated to create and send messages (especially positive messages) when they hold more valuable alternative currency and that interest in the currency generally is greater when it is more valuable in total. These transient messages may be emails or conversations, but are not messages that stay visible, such as social media/blog postings, articles, and reports.
*   **Enduring Message Rate:** This is the average number of enduring messages per year generated by the group collectively, per £100,000 of total alternative currency holdings of all holders. (This is the value of all the alternative currency held by everyone expressed in the first fiat currency.) These enduring messages may be social media/blog postings, articles, and reports i.e. messages that remain visible for a long time. It does not matter to this number how many people read each message.
*   **Enduring Reading Rate:** This is the average number of readings per enduring message within the first 24 hours of the message being released. For example, if an article is posted on a news website and is read by 543 people in the first 24 hours then its reading rate is 543. However, the set number needs to be the average reading rate over all enduring messages generated by the group.

In addition to these messages generated by the environment, the Controller can also generate messages.

These are consolidated into two variables for use in agent decision-making: **Positive Current Buzz** and **Negative Current Buzz**. These two variables may be modified during a simulated day by newsworthy events.


Buzz Accumulation
=================

The enduring messages are also important because they feed into two accumulated stocks of messages that depreciate over time: **Positive Accumulated Buzz** and **Negative Accumulated Buzz**. These are calculated at the end of each day.


Newsworthy Events
=================

Newsworthy Events during a day have an immediate effect on Positive Current Buzz and Negative Current Buzz, and also an effect at the end of the day when the Accumulated Buzz figures are updated.


Use In Decisions
================

Different decisions can be made to weight Positive Current Buzz, Negative Current Buzz, Positive Accumulated Buzz, and Negative Accumulated Buzz differently. Suggested approaches are:

*   How many potential users consider opting in: mostly dependent on the Current Buzz.
*   How many potential users actually opt in having considered it: all Buzz variables.
*   Amount of alternative currency to hold: mostly depend on the Current Buzz.
*   Whether to opt out: mostly dependent on the Current Buzz.



***************
Further Reading
***************

Earlier academic work on 'sentiment' focused on the meaning of the concept, distinguishing it from rational thinking about market value. More recently the focus has been on statistical links between social media content and prices. Academic work on 'buzz' is more typical of work in the area of marking. This is a fairly typical example of academic work on 'sentiment' in recent years:

Karalevicius, V., Degrande, N., & De Weerdt, J. (2018). Using sentiment analysis to predict interday Bitcoin price movements. *The Journal of Risk Finance*, 19(1), 56-75. Available online `here`__.

.. __: https://www.emeraldinsight.com/doi/abs/10.1108/JRF-06-2017-0092

