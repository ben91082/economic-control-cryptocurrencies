##############################
Unused Material - REMOVE LATER
##############################

This is for material that might be useful - until we know it isn't.

******************************
Speculator currency prediction
******************************

To decide how strong the trend is, up or down, Speculators consider three factors and the relative importance of each can be set using their weights:

*   *Weight Of Current Hour:* This weight is applied to the change in value of the alternative currency, expressed in the first fiat currency, since the last hour. For example, if the time is now 16:37 then the Speculator considers the price change since 16:00.
*   *Weight Of Current Day:* This weight is applied to the change in value of the alternative currency, expressed in the first fiat currency, since the previous midnight.
*   *Weight Of Current 7 Days:* This weight is applied to the change in value of the alternative currency, expressed in the first fiat currency, since the midnight before 7 days earlier. In other words, go back to the last midnight, then go back 7 more days. If this takes us before the start of the simulation trial then the opening price is used instead.
*   *Weight Of Net Buzz:* This weight is applied to the current Net Buzz, which is the difference between Positive Buzz and Negative Buzz. For example, if what people are saying and writing about the currency is more negative than positive then the Speculator is more likely to consider the trend to be negative.

Since price changes over longer periods tend to be larger, on average, using equal weights for price changes will tend to make the Speculator's assessments mostly driven by the 7 day change.

When Speculators are created they don't all have the same weights for the above factors. Their weights are chosen randomly according to the following settings:

*   **Lowest Weight Of Current Hour:**
*   **Highest Weight Of Current Hour:**
*   **Lowest Weight Of Current Day:**
*   **Highest Weight Of Current Day:**
*   **Lowest Weight Of Current 7 Days:**
*   **Highest Weight Of Current 7 Days:**
*   **Lowest Weight Of Net Buzz:**

Real Speculators are not always consistent or logical, and this is modelled by an error rate.

*   **Speculation Error Rate:** This is the percentage of decions on holdings that fail to follow the Speculator's usual decision making method and instead are just random decisions to buy or sell up to a given percentage of the current holding. The actual amount is chosen randomly using a uniform distribution over the relevant range.
*   **Speculation Error Size:** This is the maximum error expressed as a percentage of the current holding.




Purchasing Gain
---------------

This is relevant only for Users that are Customers.

*   Buy a good with :math:`f\text{£}`: no effect.

*   Buy a good with a first fiat currency price of :math:`p` with :math:`a\text{c}`:

.. math:: Q' = Q - a, \ \ \ C' = C - a {C \over Q}, \ \ \ G = p - a {C \over Q}

Sellling Gain
-------------

This is relevant only for Users that are Merchants.

*   Receive :math:`f\text{£}` for a good: no effect.

*   Receive :math:`a\text{c}` for good with a first fiat currency price of :math:`p`:

.. math:: Q' = Q + a, \ \ \ C' = C + p, \ \ \ G = 0

Operator Gain
-------------

This is relevant only for Users who are Operators.

*   Support a transaction and (perhaps) get paid :math:`a\text{c}` for it in the alternative currency:

.. math:: Q' = Q' + a, \ \ \ C' = C + c_v + c_f, \ \ \ G = a {C \over Q} - c_v - c_f

where :math:`c_v` is the variable cost per transaction for the operator, :math:`c_f` is the fraction of the daily fixed costs incurred since the last transaction.





****************************************
Some Text On The Pool Of Potential Users
****************************************

There are many more of these potential users than active users. The effect of this is that there are almost always more people who might be persuaded to get involved but the early adopters soon opt in and involving the remaining pool requires stronger encouragement.

To understand how this process works, think about the way people get involved with an alternative currency (such as a cryptocurrency) for the first time, starting from having little or no interest in it. Their journey begins with something that prompts them to think about getting involved. That might be a friend saying they should do it, or an article about money to be made, or a Facebook post, or a YouTube video.

Getting involved is a significant step because it involves some hard mental work, learning the terminology, choosing which software to install or website to use, where to go to get started, and how to make that first purchase. The first investment, in reality, is one of mental effort and time, but once they're started and feeling more skilled they can start to think more seriously about what to buy with the currency or how to speculate with it. Later, they may lose interest and either forget about the alternative coins they have or sell them off and delete the software.

So, the starting point is a prompt to think about getting involved. Every time a message about the alternative currency is encountered there is a chance that it will prompt this consideration.

If they decide to consider it then they will start looking for more information. They will probably search online for more information, watch more of the videos, ask friends, and ask anyone more expert than them for advice. If this research reveals a lot of negative information about the currency then their decision will probably be to keep out. If they find lots of positive information they are more likely to opt in, download some software, register on a website, and maybe make a small purchase of the currency. (It's a bit different with new coins where you are putting up money to get coins later, when the software is written.)

In summary, the amount of information received is important in prompting people to consider getting involved with the currency but the content of that information is important in persuading them to opt in.




**********************
Distribution Of Wealth
**********************

Within the simulator one way this is achieved is to use a geometric sequence so that each person's holdings are a fixed multiple of the next wealthiest user. The parameters of this distribution are its total value, the number of users, and the fixed multiplier.

If :math:`T` is the total value to be distributed, :math:`M` is the multiplier, and :math:`N` is the total number of users then the ideal value of the smallest holding is:

.. math:: 

    ={T(1-M) \over 1-M^N} 

and the ideal value of the :math:`n^{th}` holding is:

.. math:: 

    ={T(1-M) \over 1-M^N}M^{n-1}

However, these are just the ideal values and need to be rounded to the appropriate precision. Then a procedure is used to adjust the values so that the total value is correct, even if the multiples are not always exactly correct from one user to the next.

If the total of rounded values is too low, the extras required (in the smallest value amounts for the given precision) are allocated to users, starting with the wealthiest user, one at a time. If this results in an extra unit being given to all users then a second pass is made, again starting with the wealthiest user.

If the total of rounded values is too high then single units are deducted from each user in turn, starting with the wealthiest user and continuing until the extras have been removed. If this would mean giving a user zero then the deduction is not made and a second pass is made, again starting with the wealthiest user.

An approximate Gini Index (a measure of inequality) can be calculated from just the multiplier and number of people.

.. math:: 

    G={{N+1 \over 2} - {N \over 1-M^N} + {M \over 1-M} \over {N+1 \over 2}}

The level of inequality measured this way rises with both the multiplier and the number of people.

***************************
Generating Wealth At Random
***************************

In other situations, the simulator needs to generate individual values randomly according to a distribution so that some people are wealthier than others. The settings given are the Minimum, Maximum, and Median values, with the distribution being exponential with this form:

.. math:: 

    V=ae^{bx}+c

Given :math:`x`, a value taken at random between 0 and 1, uniformly distributed, this returns a value, :math:`V`. The constants are based on the Minimum (:math:`m`), Maximum (:math:`M`), and Median (:math:`x`) as follows:

.. math:: 

    &c = {Mm-x^2 \over M + m -2x}\\
    &a = m - c\\
    &b = ln\bigg[{M - c \over a}\bigg]



***********************************
Another version of the same problem
***********************************

*   **Average Number of Purchases Daily (ANPD):** This is the number per person, per day, on average over all customers in the overall population.

*   **Maximum Average Number of Purchases Daily (MANPD):** This is the average number of purchases per day made by the person with the highest personal average of purchases per day. On some days they will purchase even more. This maximum is the average for that person.

From these two pieces of information you supply the system works out the parameters of the specific curve used to generate average purchase numbers for each person. That curve is as follows:

.. math:: ADP = {MANPD \over \left( 100x + 1 \right)^s}

Where :math:`ADP` is the Average Daily Purchases for the customer, :math:`x` is a random number distributed uniformly over the range zero to one, and :math:`s` is a parameter that decides the shape of the curve, and is calculated from the Average Number of Purchases Daily you provide.

NOTES ON THIS CALCULATION

Because :math:`x` is uniformly distributed with a probability density of 1, the mean of the function is:

.. math:: mean =\int_0^1 \! {MANPD \over \left(100x + 1 \right)^s}dx

For :math:`s = 1`,

.. math:: 

    &=\int_0^1 \! {MANPD \over 100x + 1}dx \\
    &= MANPD \left[ {ln\left[ 100x + 1\right] \over 100} \right]_0^1  \\
    &= MANPD { ln\left[ 101 \right] \over 100}

For :math:`s \ne 1`,

.. math:: 

    &= MANPD \left[ { 1 \over 100 \left( 100x + 1 \right)^{s-1} \left( 1-s \right)   } \right]_0^1  \\
    &= MANPD \left( { 1 \over 100 \left( 1 - s \right) 101^{s-1}  } - { 1 \over 100 \left(1 - s \right) }\right)  \\
    &= MANPD \left( { 1 - 101^{s-1} \over 100 \left( 1 - s \right) 101^{s-1}  } \right) 

For a given value of the mean, finding :math:`s` requires an iterative approach. A reasonable initial value for :math:`s` is:

.. math:: s = {MANPD \over 100 \times ANPD}


For Customers
=============

Initial delay before considering opting in
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The initial delay between being released from the pool of potential users and deciding to opt in (or stay out) is random and uniformly distributed. It ranges between zero and the Time Between Releases (:math:`TBR`), set for the pool of potential users.

.. math:: delay \: \mathtt{\sim} \: U\left[0,TBR\right]

This helps to spread out the arrival of new joiners rather than have them all joining at the same instant.

Subsequent delay before next considering opting in
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When a user opts into any other role then, at that moment, the delay until the next consideration of becoming a customer is set. The agent might be one who is passively influenced by buzz, in which case the delay is related to the degree of negative and positive buzz at the time, as if being exposed to a certain number of words in total is enough to trigger reconsideration. Alternatively, the agent might be one who plans ahead and responds to how close the decision was. The closer the decision last time the shorter the delay before thinking again. The parameters for these decisions are as follows:

*	Baseline Buzz (:math:`BB`): The delay in days is random and uniformly distributed, driven by the Negative Buzz level (:math:`NB`) and Positive Buzz level (:math:`PB`). It ranges between zero and a value calculated from :math:`NB`, :math:`PB`, and :math:`BB`. In choosing :math:`BB`, think of the level of buzz that would, on average, have people reconsidering again after one week:

.. math:: delay \: \mathtt{\sim} \: U\left[0,{14×BB \over NB+PB}\right]

*	Baseline Difference (:math:`BD`): The delay is random and uniformly distributed between zero and a delay directly proportional to a value arising from considering opting in. In considering opting in a value is calculated that combines the effect of all relevant variables and is then compared with a criterion. If it does not meet the criterion then the agent does not opt in. However, the shortfall is the value used to calculate the next delay. If the decision was a close one the shortfall is small and so is the delay until the next consideration.

.. math:: delay \: \mathtt{\sim} \: U\left[0,{d \over SD_D}\right]


