#####################
Parameters --- Timing
#####################

.. sectnum:: :start: 7

.. _ds_time:

The parameters here are simply:

*   **Trial Length:** This is the total Trial length in simulated days.
*   **Days Per Step:** Each time you click the button to continue a Trial this number of days is simulated before control is returned to you.

