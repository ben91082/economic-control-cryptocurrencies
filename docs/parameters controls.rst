#######################
Parameters --- Controls
#######################

.. sectnum:: :start: 12

Settings of these parameters specify the control mechanisms available to you initially and as a Trial unfolds. Many of these are also explained and available under Parameters for other sections, but are duplicated here to bring together all the settings with value as controls.


.. _p_contr_stocks:

***************************
Managing Stocks Of Currency
***************************

The first group of parameters allows the Controller (you interacting with the simulator) to manage stocks of currency. The mechanisms are explained in :ref:`control_stocks`.

The table allows you to specify which of these mechanisms are in operation (i.e. On or Off) and what rate is used for each:

+-------------------------------------+--------------+-------------------------------------------------------------+
| **Mechanism**                       | **On / Off** | **Rate**                                                    |
+=====================================+==============+=============================================================+
| Gift Freshly Minted                 | Not needed   | | **Gift Coins** (also under :doc:`parameters currency`)    |
|                                     |              | | Alternative currency given to new Active Users            |
+-------------------------------------+--------------+-------------------------------------------------------------+
| Sell Freshly Minted                 |              | | **Mint Purchase Price**                                   |
|                                     |              | | (also under :doc:`parameters currency`)                   |
|                                     |              | | Fiat price of an alternative currency coin sold to a User |
+-------------------------------------+--------------+-------------------------------------------------------------+
| Buy Back And Melt                   |              | | **Buy Back To Melt Price**                                |
|                                     |              | | Fiat price offered to buy an alternative currency coin    |
+-------------------------------------+--------------+-------------------------------------------------------------+
| Sell From Holding                   |              | | **Alt Purchase Price**                                    |
|                                     |              | | Fiat sale price of an alternative currency coin           |
+-------------------------------------+--------------+-------------------------------------------------------------+
| Buy Back And Hold                   |              | | **Buy Back To Hold Price**                                |
|                                     |              | | Fiat price offered to buy an alternative currency coin    |
+-------------------------------------+--------------+-------------------------------------------------------------+
| | Operator Payment In               | Not needed   | | **Controller Paid Fixed Transaction Fee (Minted)**        |
| | Freshly Minted, Fixed             |              | | (also under :doc:`parameters currency`)                   |
|                                     |              | | A fixed amount per transaction in alternative currency    |
+-------------------------------------+--------------+-------------------------------------------------------------+
| | Operator Payment In               | Not needed   | | **Controller Paid Percentage Transaction Fee (Minted)**   |
| | Freshly Minted, %                 |              | | (also under :doc:`parameters currency`)                   |
|                                     |              | | A % amount per transaction in alternative currency        |
+-------------------------------------+--------------+-------------------------------------------------------------+
| Control Catalogue                   |              | | **Control Catalogue Pricing Rate**                        |
|                                     |              | | The rate used to calculate alternative currency           |
|                                     |              | | prices of goods. (Alt price = fiat price / rate.)         |
+-------------------------------------+--------------+-------------------------------------------------------------+


The Controller also needs an initial holding of first fiat currency and an initial holding of alternative currency. These can be set to zero of course.

*   **Initial First Fiat Currency Of Controller:** The amount of first fiat currency held by the Controller when the Trial begins. Must not be negative. (Also under :ref:`p_curr_launch_controller`.)
*   **Initial Alternative Currency Of Controller:** The amount of alternative currency held by the Controller when the Trial begins. Must not be negative. (Also under :ref:`p_curr_launch_controller`.)

See :ref:`tech_minting` for all the ways that alternative currency gets minted.

If the Control Catalogue is in operation then there are other settings that specify it.

*   **Number Of Control Catalogue Exclusive Goods:** This is the number of types of good offered in the Control Catalogue that are not available to be sold by Merchants. Their prices are determined randomly using the same distribution parameters as other goods (see :ref:`p_env_goods`). They have no substitutes.
*   **Fraction Of Control Catalogue Lone Goods:** This is the fraction of the types of Lone Good that are offered in the Control Catalogue. These are the same goods available to be sold by Merchants.
*   **Fraction Of Control Catalogue Paired Goods:** This is the fraction of the types of Paired Good that are offered in the Control Catalogue. These are the same goods available to be sold by Merchants.

The Controller can offer goods with prices in first fiat currency and/or alternative currency. These prices will be worked out from the underlying first fiat currency price, the transaction fees involved, and the Control Catalogue Pricing Rate. The Controller needs to specify which prices are offered by switching them On or Off:

+--------------------------+------------------------+----------------------------+
| **Good**                 | | **First Fiat Price** | | **Alternative Currency** |
|                          | | **On/Off**           | | **Price On/Off**         |
+==========================+========================+============================+
| Exclusive Good           |                        |                            |
+--------------------------+------------------------+----------------------------+
| Lone Good                |                        |                            |
+--------------------------+------------------------+----------------------------+
| Paired Good - ordinary   |                        |                            |
+--------------------------+------------------------+----------------------------+
| Paired Good - substitute | Not used               |                            |
+--------------------------+------------------------+----------------------------+

The Controller pays the same for goods as Merchants, as determined by the parameter: Cost Multiple For Goods (see :ref:`p_env_goods`). This amount is taken off the Controller's fiat currency stock when a sale is made to a User.

Once the goods and their fixed fiat prices have been determined the Control Catalogue Rate can be used to (in effect) define the value of the alternative currency using the goods offered in the Control Catalogue.


.. _p_contr_buzz:

***************
Generating Buzz
***************

The Controller can generate positive transient and enduring messages and so increase Positive Current Buzz and, later, Positive Accumulated Buzz. This might be to generate interest initially or perhaps in response to a negative event. The settings needed are:

*   **Cost Per 1,000 Transient Messages:** The first fiat currency cost to the Controller of generating 1,000 positive transient messages in a day.
*   **Cost Per 1,000 Enduring Messages:** The first fiat currency cost to the Controller of generating 1,000 positive enduring messages in a day.
*   **Controller Generated Transient Messages Per Day:** The number of transient messages generated by the Controller per day. Each day this many messages will be sent, affecting the day's Positive Current Buzz and, subsequently, its Positive Accumulated Buzz. The stock of fiat currency will be reduced by the cost of generating these messages.
*   **Controller Generated Enduring Messages Per Day:** The number of enduring messages generated by the Controller per day. Each day this many messages will be sent, affecting the day's Positive Current Buzz and, subsequently, its Positive Accumulated Buzz. The stock of fiat currency will be reduced by the cost of generating these messages.
*   **Controller Generated Positive Enduring Reading Rate:** The number of people who read the enduring messages generated by the Controller in the first 24 hours.


.. _p_contr_other:

**********************
Other Control Settings
**********************

Other control settings regulate transactions:

*   **User Paid Fixed Transaction Fee:** The total fixed fee per transaction paid to Operators in the alternative currency, paid by taking a cut of the transaction. Whether it is the payer who has to pay the extra or the receiver who has to go without depends on the commercial deal between them and not on the currency.
*   **User Paid Percentage Transaction Fee:** This is the percentage of transaction value paid to Operators in the alternative currency, paid by taking a cut of the transaction.

There are also parameters for the User paid transaction fees when first fiat currency is used, but these are not the Controller's responsibility. See :ref:`p_env_fiat`.

If the first exchange is controlled and a Market Maker then all the controls over the Market Maker's decision making are part of the Controller's responsibility and can be changed during a Trial. These are given in :ref:`p_exch_1`.

