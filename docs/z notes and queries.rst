################################
Notes And Queries - REMOVE LATER
################################

*****
To Do
*****


*****************
Not yet supported
*****************

#. International sales - or withdraw second fiat currency and revise references to 'first' fiat currency. More expensive fiat transactions to some merchants, whose goods are similar but worse, but also less expensive than usual.
#. Pump and dump operations.
#. Arbitrage between exchanges.


*****
Notes
*****

#. For the Customer purchase decision, in the not-clear-cut case, there is a more sophisticated alternative to equal probability, which is to look at how close the alternative currency is to (a) the price that would make it the clear cut better price and (b) the price that would make it the clear cut worse price. The Customer could then choose the price accordingly.
#. What about exchanges that have client accounts and fees for paying out money from them? It's common but not reflected in the simulator at this time.
