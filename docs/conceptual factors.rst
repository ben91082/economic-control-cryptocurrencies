#############################
Working With Decision Factors
#############################

.. sectnum:: :start: 5


****************
Decision Factors
****************

For many of the most important decisions taken by agents in the simulator, you can specify the **factors** considered and how these are used in the decision method. The :doc:`Decision Factor List <decision factor list>` gives all the available factors and these are also shown by the system as you work to specify a decision method.

The decision factors are labelled as either 'half' or 'full'. A 'half' factor is one that starts at 0 and then rises (e.g. number of users). A 'full' factor is one that can take any value (e.g. average profit/loss, with losses being negative).


******************************
Linking Factors To Multipliers
******************************

The decision factors need to be **calibrated** to values of **multipliers**. Once the calibration is defined, each possible value of the factor is linked to a value of multiplier. These multipliers are numbers used to combine the effect of each decision factor and multipliers are used differently in different types of decision. Some multipliers are constrained between 0 and 1, but others are not.

To specify the calibration for a decision factor you need to choose a **calibration function type** and a small number of **calibration values** that are used to fix the precise mapping of decision factor values to multiplier values.

There are 8 calibration function types, but they are systematically named. The 'asc' is short for 'ascending' while 'desc' is short for 'descending'. A '1' on the end means the range (i.e. the output) is constrained to be between 0 and 1:

*   :math:`\text{half-asc}`: For factors that start at zero, and where higher factor values should link to higher multipliers.
*   :math:`\text{half-asc-1}`: For factors that start at zero, higher factor values should link to higher multipliers, and the multipliers must be between 0 and 1.
*   :math:`\text{half-desc}`: For factors that start at zero, and where higher factor values should link to lower multipliers.
*   :math:`\text{half-desc-1}`: For factors that start at zero, higher factor values should link to lower multipliers, and the multipliers must be between 0 and 1.
*   :math:`\text{full-asc}`: For factors that can take any value, and where higher factor values should link to higher multipliers.
*   :math:`\text{full-asc-1}`: For factors that can take any value, higher factor values should link to higher multipliers, and the multipliers must be between 0 and 1.
*   :math:`\text{full-desc}`: For factors that can take any value, and where higher factor values should link to lower multipliers.
*   :math:`\text{full-desc-1}`: For factors that can take any value, higher factor values should link to lower multipliers, and the multipliers must be between 0 and 1.

This example shows the characteristic shapes of :math:`\text{full-asc}` and :math:`\text{full-desc}`. Different calibration values will produce similar but different curves.

.. image:: test.png


***************************
Calibration Values Required
***************************

The calibration values needed depend on the type of calibration function. The functions restricted to the range 0 to 1 are simpler and require only:

*   *Half Level*: The value of the factor that would give a multiplier of 0.5.
*   *Zero Multiplier*: The multiplier when the factor is 0.
   
The calibration functions with ranges you can specify all require:

*   *Highest Multiplier*: The highest possible multiplier.
*   *Lowest Multiplier*: The lowest possible multiplier.
*   *Unit Level*: The value of the factor that would give a multiplier of 1.

In addition, :math:`\text{full-asc}` requires:

*   *Double Level*: The value of the factor that gives a multiplier of 2.

And :math:`\text{full-desc}` requires:

*   *Half Level*: The value of the factor that gives a multiplier of 0.5.

For full details of the formulae used, see :ref:`tech_calibration_functions`.



***************
Further Reading
***************

Two examples of using agent-based simulations to explore adoption of technology:

Moglia, M., Podkalicka, A., & McGregor, J. (2018). An Agent-Based Model of Residential Energy Efficiency Adoption. *Journal of Artificial Societies and Social Simulation*, 21(3). Available online `here`__.

.. __: http://jasss.soc.surrey.ac.uk/21/3/3/3.pdf

Palmer, J., Sorda, G., & Madlener, R. (2015). Modeling the diffusion of residential photovoltaic systems in italy: An agent-based simulation. *Technological Forecasting and Social Change*, 99, 106-131. Available online `here`__.

.. __: https://www.sciencedirect.com/science/article/pii/S004016251500178X

