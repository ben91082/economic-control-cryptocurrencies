Economic Control of Alternative Currencies - Interactive Simulator
==================================================================

Projective Objectives
---------------------

* To develop an economic simulator capable of simulating characteristics of hypothetical alternative ocurrencies (e.g. cryptocurrencies), their users, environment, exchanges, etc.
* To be freely available to use online.
* Supporting design, development, control, and also regulation of alternative currencies.
* With the goal of making them genuinely useful and competitive as currencies and electronic payment systems, in the long term too.

This Documentation
------------------

* This is user documentation for a system that so far has not been created. Its rich detail captures a detailed design for the system.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   simulation workflow
   conceptual reasons
   conceptual buzz
   conceptual factors
   conceptual controls
   parameters timing
   parameters environment
   parameters currency
   parameters users
   parameters exchanges
   parameters controls
   examining results
   decision factor list
   agent decision reference
   tech notes