########################
Agent Decision Reference
########################

.. sectnum:: :start: 15

********
Overview
********

Agents are the simulated decision makers. They make many decisions. These can be divided into:

*   **Timing Decisions:** These are simply decisions about when to take another decision.
*   **Other Decisions:** These are any other type of decision.

*****************
List Of Decisions
*****************

|  **Environment**
|      Taken at the start of the simulation:
|        `Choose the goods used`_
|      Taken at the start of each day (midnight):
|        `The initial levels of Current Buzz`_
|      Taken at the end of each day (midnight):
|        `The levels of Accumulated Buzz`_
|      Taken at (a) the start of the simulation, and (b) just after considering generating a positive/negative newsworthy event:
|        `When next to generate a positive/negative random newsworthy event`_
|      Taken at the planned time:
|        `The characteristics of a positive/negative random newsworthy event`_
|      Taken when an exchange decides to increase/decrease support for the alternative currency:
|        `The characteristics of the Exchange support increase/decrease event resulting`_
|  **Alternative Currency**
|      Taken at the start of the simulation:
|        `The characteristics of the Initial Users in their roles`_
|        `The characteristics of the Token Holding Users initially`_
|      Taken when a transaction in the alternative currency takes place:
|        `Which operators process the transaction and who wins the transaction fee`_
|  **Token Holding Users**
|      Taken when the Token Holding User is created at the start of the simulation:
|        `When the Token Holding User will convert tokens into coins`_
|      Taken at the planned time for converting tokens:
|        `Which role(s) the Token Holding User will adopt`_
|      Taken when converting tokens:
|        `The extra characteristics of the Token Holding Users`_
|  **Pool Of Potential Users**
|      Taken (a) at the start of the simulation and (b) just after creating new users:
|        `When next to generate Individual Potential Users`_
|      Taken at the planned time:
|        `How many Individual Potential Users to create`_
|  **Individual Potential Users**
|      Taken at the creation of the Individual Potential User:
|        `Initial properties of the Individual Potential User`_
|        `When to consider opting in`_
|      Taken at the planned time:
|        `Whether to opt in and for which role(s)`_
|      Taken just after deciding when to opt in and for which roles:
|        `Initial properties as an Active user`_
|  **Active Users**
|    *In All Roles*
|      Taken on (a) becoming a User and (b) just after considering holding level:
|        `When to consider holding levels`_
|      Taken at planned time to reconsider holding levels:
|        `How much alternative currency and fiat currency to hold`_
|      Taken just after deciding how much alternative currency to hold:
|        `How to adjust currency holdings`_
|    *As Speculators*
|      Taken on (a) becoming a User and (b) just after considering opting in/out as a Speculator:
|        `When next to consider opting in/out as a Speculator`_
|      Taken at the planned time for considering opting in/out:
|        `Whether to opt in as a Speculator`_
|        `Whether to opt out as a Speculator`_
|    *As Customers*
|      Taken on (a) becoming a user and (b) just after considering opting in/out as a Customer:
|        `When next to consider opting in/out as a Customer`_
|      Taken at planned time for considering opting in/out:
|        `Whether to opt in as a Customer`_
|        `Whether to opt out as a Customer`_
|      Taken on (a) becoming a User and (b) just after shopping:
|        `When to shop next`_
|      Taken when shopping, for each item bought:
|        `What to purchase and how`_
|    *As Merchants*
|      Taken on (a) becoming a User and (b) just after considering opting in/out as a Merchant:
|        `When next to consider opting in/out as a Merchant`_
|      Taken at planned time:
|        `Whether to opt in as a Merchant`_
|        `Whether to opt out as a Merchant`_
|      Taken on becoming a Merchant:
|        `Which goods to offer`_
|      Taken on (a) becoming a Merchant and (b) considering the pricing policy:
|        `When to reconsider the pricing policy`_
|      Taken on (a) becoming a Merchant and (b) at planned time:
|        `Which pricing policy to use`_
|      Taken at (a) deciding to use Fixed prices and (b) just after pricing:
|        `When to price next`_
|      Taken at (a) the planned time to revise Fixed prices and (b) when a Customer considers purchasing with Floating prices:
|        `The prices to use`_
|    *As Operators*
|      Taken on (a) becoming a User and (b) just after considering opting in/out as an Operator:
|        `When next to consider opting in/out as an Operator`_
|      Taken at planned time:
|        `Whether to opt in as an Operator`_
|        `Whether to opt out as an Operator`_
|  **Opting Out Users**
|      Taken just after trying to sell all holdings if not entirely successful:
|        `When next to try to get rid of alternative currency holding`_
|      Taken (a) after opting out of all User roles and (b) at planned time:
|        `How to get rid of alternative currency holding`_
|  **Exchanges**
|    *All Exchanges*
|      Taken at the start of the simulation
|        `Initial characteristics of the exchange`_
|      Taken at (a) the start of the simulation and (b) just after considering offering an exchange service:
|        `When next to consider offering an exchange service for the alternative currency`_
|      Taken at the planned time:
|        `Whether to start offering an exchange service for the alternative currency`_
|        `Whether to stop offering an exchange service for the alternative currency`_
|    *As Continuous Order Driven Exchange (CODE)*
|      Taken when an order is received:
|        `CODE actions on receiving an order`_
|    *As Market Maker (MM)*
|      Taken when an order is received:
|        `MM actions on receiving an order`_
|      Taken on (a) starting to offer an exchange service and (b) on changing prices:
|        `When to consider changing prices`_
|      Taken at planned time:
|        `Set/revise Bid and Ask prices`_
|      Taken on (a) starting to offer an exchange service and (b) on reconsidering the stock of alternative currency:
|        `When to reconsider the target stock of alternative currency`_
|      Taken at the planned time:
|        `What to use as target for currency stocks`_
|        `How to adjust currency stocks`_



***********
Environment
***********


.. _adr_env_goods:

Choose the goods used
=====================

Merchants and the Control Catalogue (if used) offer real goods for sale to Customers. The set of goods used in a simulation, with its underlying fiat currency price list, is created at the start of a simulation and not changed.

Some goods have a substitute good that is similar but a bit better and that can only be purchased using the alternative currency. Goods that have no substitute are called **Lone Goods**. Goods that have a substitute are called **Paired Goods**. There may also be goods that can only be offered by the Control Catalogue, called **Exclusive Goods**. Goods can be priced in fiat currency, or in both the fiat currency and the alternative currency. Substitutes can only be priced in alternative currency.

Individual Merchants, and perhaps also the Control Catalogue, offer some or all of the goods and their substitutes.

The goods to be priced are:

*   *Lone Goods:* The number of these is set by Number Of Lone Goods (see :ref:`p_env_goods`).
*   *Paired Goods:* These are really pairs of a good and its substitute, both with the same price in fiat currency. (The differences are that Users see their values as being different and the substitute can only be purchased using the alternative currency.) The number of these pairs is set by Number Of Paired Goods (see :ref:`p_env_goods`).
*   *Exclusive Goods:* These are the goods that can only be offered by the Control Catalogue. the number of these is set by Number Of Control Catalogue Exclusive Goods (see :ref:`p_contr_stocks`).

The underlying prices of the goods in fiat currency are generated randomly with a Log-normal distribution using the parameters give by Lowest Price Of A Good, Mean Price Of A Good, and Median Price Of A Good. (See :ref:`tech_dist_wealth` for details and see :ref:`p_env_goods` for the related settings.) These are the underlying fiat currency prices, reflecting the value of the goods. The fiat currency prices quoted to customers may be different in order to cover transaction fees, where the Merchant pays the fees.


.. _adr_env_curr_buzz:

The initial levels of Current Buzz
==================================

The level of Positive Current Buzz and Negative Current Buzz is calculated at the start of each day (at midnight) but may be modified by newsworthy events during the following day. The Buzz includes that generated by the environment and the Buzz generated by the Controller. For full details see :ref:`tech_buzz` and for related settings see :ref:`p_env_buzz_gen` and :ref:`p_contr_buzz`.


.. _adr_env_acc_buzz:

The levels of Accumulated Buzz
==============================

The level of Positive Accumulated Buzz and Negative Accumulated Buzz is calculated at the end of each day (at midnight) based on the Positive and Negative Current Buzz of the day, as modifid by any newsworthy events happening during the day and Buzz from the Controller. For full details see :ref:`tech_buzz` and for related settings see :ref:`p_env_buzz_accum` and :ref:`p_contr_buzz`.


.. _adr_env_when_news:

When next to generate a positive/negative random newsworthy event
=================================================================

The delay until the next positive/negative random newsworthy event is generated is exponentially distributed, based on the set Average Days Between Positive Events and Average Days Between Negative Events respectively. See :ref:`tech_delays` for details and :ref:`p_env_news` for related settings.


.. _adr_env_news:

The characteristics of a positive/negative random newsworthy event
==================================================================

Each random event has two multipliers, one for the Positive Current Buzz and the other for Negative Current Buzz.

The value of each multiplier is selected randomly using a uniform distribution between the Lowest and Highest relevant multiplier, taken from the appropriate cell of the table below.

+----------------------------+-------------------------------+-------------------------------+
| Event type                 |      +ve Buzz Multiplier      |     -ve Buzz Multiplier       |
+----------------------------+---------------+---------------+---------------+---------------+
|                            |    Lowest     |    Highest    |    Lowest     |    Highest    |
+----------------------------+---------------+---------------+---------------+---------------+
| Random Positive Event      |:math:`m_{1,1}`|:math:`m_{1,2}`|:math:`m_{1,3}`|:math:`m_{1,4}`|
+----------------------------+---------------+---------------+---------------+---------------+
| Random Negative Event      |:math:`m_{2,1}`|:math:`m_{2,2}`|:math:`m_{2,3}`|:math:`m_{2,4}`|
+----------------------------+---------------+---------------+---------------+---------------+

See :ref:`p_env_news` for related settings.


.. _adr_env_news_exch:

The characteristics of the Exchange support increase/decrease event resulting
=============================================================================

Each exchange support change event has two multipliers, one for the Positive Current Buzz and the other for Negative Current Buzz.

The value of each multiplier is selected randomly using a uniform distribution between the Lowest and Highest relevant multiplier, taken from the appropriate cell of the table below.

+----------------------------+-------------------------------+-------------------------------+
| Event type                 |      +ve Buzz Multiplier      |     -ve Buzz Multiplier       |
+----------------------------+---------------+---------------+---------------+---------------+
|                            |    Lowest     |    Highest    |    Lowest     |    Highest    |
+----------------------------+---------------+---------------+---------------+---------------+
| Exchange Support Increase  |:math:`m_{1,1}`|:math:`m_{1,2}`|:math:`m_{1,3}`|:math:`m_{1,4}`|
+----------------------------+---------------+---------------+---------------+---------------+
| Exchange Support Decrease  |:math:`m_{2,1}`|:math:`m_{2,2}`|:math:`m_{2,3}`|:math:`m_{2,4}`|
+----------------------------+---------------+---------------+---------------+---------------+

See :ref:`p_env_news` for related settings.



********************
Alternative Currency
********************


.. _adr_curr_init_users:

The characteristics of the Initial Users in their roles
=======================================================

Simulations may start with an initial population of Active Users. Each of these Initial (i.e launch) Users has to be created with the properties specified for all Users and the properties for each role the User is taking.

Each Initial User is created with an initial *Holding In The Fiat Currency* and an initial *Holding In The Alternative Currency*. (See :ref:`tech_minting` for all the ways that alternative currency can be minted.) These are calculated in three stages. 

1. The total of these two, with the alternative currency valued in the fiat currency, is drawn randomly according to the Log-normal distribution specified by the settings: Lowest Initial Holding, Mean Initial Holding, and Median Initial Holding (see :ref:`p_ipu_all`). (See :ref:`tech_dist_wealth` for the formulae used.)

2. The fraction of this total held as alternative currency is selected randomly according to the beta distribution with parameters specified by the Mean Fraction Held As Alternative Currency and the Variance Of Fraction Held As Alternative Currency (see :ref:`p_curr_launch`).

The :math:`\alpha` and :math:`\beta` parameters of the beta distribution are calculated from the mean, :math:`m`, and variance, :math:`v`, as follows:

.. math::

    &\alpha = {m^2(1-m) \over v} - m \\
    &\beta = {(1-m)(m(1-m)-v) \over v}

The values of :math:`\alpha` and :math:`\beta` will be rounded to the nearest integer and must be greater than or equal to 1. Having made these adjustments, the simulator displays the mean, variance, P10, and P90 levels for the beta distribution resulting.

3. The amount of alternative currency held is calculated by converting the fiat currency value using the rate specified in Initial Alternative Currency Value (see :ref:`p_curr_launch`). The remaining value is held as fiat currency.


All Initial Users that take the role of Speculator initially will have further properties decided as follows:

*   *Extrapolation Rule:* Chosen at random from the three alternatives based on the percentages set for each in Continue Trend %, Contrarian %, and Regress To Mean % (see :ref:`p_active_all_hold`).
*   *Recency Factor:* Chosen at random with a uniform probability with the range from Lowest to Highest Recency Factor (see :ref:`p_active_all_hold`).

All Initial Users that take the role of Customer initially will have further properties decided as follows (see :ref:`p_active_cust_shop` for settings):

*   *Shopping Time:* The time of day the Customer will go shopping. Selected with uniform density between the Earliest and Latest Time Of Day set.
*   *Average Fraction Of Lone Goods Purchased Daily:* This is located between the Lowest and Highest Average Fraction Of Lone Goods Purchased and is found using the function :math:`\text{full-asc}` (see :ref:`tech_calibration_functions`) with :math:`m_t` equal to the Highest Fraction Of Lone Goods Purchased, :math:`m_b` equal to the Lowest Fraction Of Lone Goods Purchased, :math:`k=1`, and :math:`x_0` equal to the Mean Initial Holding.
*   *Average Fraction Of Paired Goods Purchased Daily:* This is located between the Lowest and Highest Average Fraction Of Paired Goods Purchased and is found using the function :math:`\text{full-asc}` (see :ref:`tech_calibration_functions`) with :math:`m_t` equal to the Highest Fraction Of Paired Goods Purchased, :math:`m_b` equal to the Lowest Fraction Of Paired Goods Purchased, :math:`k=1`, and :math:`x_0` equal to the Mean Initial Holding.
*   *Average Fraction Of Exclusive Goods Purchased Daily:* This is located between the Lowest and Highest Average Fraction Of Exclusive Goods Purchased and is found using the function :math:`\text{full-asc}` (see :ref:`tech_calibration_functions`) with :math:`m_t` equal to the Highest Fraction Of Exclusive Goods Purchased, :math:`m_b` equal to the Lowest Fraction Of Exclusive Goods Purchased, :math:`k=1`, and :math:`x_0` equal to the Mean Initial Holding. Exclusive goods are those only available from the Control Catalogue (if one is in use).
*   *Substitute Valuation Multiple:* Expresses how much more value the User places on a substitute compared to its paired ordinary good.

All Initial Users that take the role of Merchant initially will need sets of goods to offer for sale, decided as follows:

*   *Number Of Lone Goods Offered:* This is located between the Lowest and Highest Fraction Of Lone Goods set and found using the function :math:`\text{full-asc}` (see :ref:`tech_calibration_functions`) with :math:`m_t` equal to the Highest Fraction Of Lone Goods, :math:`m_b` equal to the Lowest Fraction Of Lone Goods, :math:`k=1`, and :math:`x_0` equal to the Mean Initial Holding.
*   *Number Of Paired Goods Offered:* This is located between the Lowest and Highest Fraction Of Paired Goods set and found using the function :math:`\text{full-asc}` (see :ref:`tech_calibration_functions`) with :math:`m_t` equal to the Highest Fraction Of Paired Goods, :math:`m_b` equal to the Lowest Fraction Of Paired Goods, :math:`k=1`, and :math:`x_0` equal to the Mean Initial Holding.

The required numbers of goods are then chosen randomly from the total sets of goods created for the simulation. See :ref:`p_active_merch_goods` for the settings involved.

All Initial Users that take the role of Operator initially will have additional properties decided as follows:

*   *Variable Cost Per Transaction:* This is generated randomly using the Log-normal distribution with parameters Lowest Variable Cost, Mean Variable Cost, and Median Variable Cost.
*   *Fixed Cost Per Day:* This is generated randomly using the Log-normal distribution with parameters Lowest Fixed Cost, Mean Fixed Cost, and Median Fixed Cost.

See :ref:`p_active_oper_operating` for related settings.


.. _adr_curr_token_users:

The characteristics of the Token Holding Users initially
========================================================

Each Token Holding User is created with an initial *Holding In The Fiat Currency* and an initial *Holding Of Tokens*. (The initial *Holding Of Alternative Currency* is zero.) The total of these, with the tokens valued in the fiat currency, is drawn randomly according to the Log-normal distribution specified by the settings: Lowest Initial Holding, Mean Initial Holding, and Median Initial Holding. (See :ref:`tech_dist_wealth` for the formulae used and :ref:`p_curr_launch` for settings.)

The fraction of this total held as tokens is selected randomly according to the beta distribution with parameters specified by the Mean Fraction Held As Tokens and the Variance Of Fraction Held As Tokens. Each token is worth one coin of the alternative currency, and each coin is worth the amount of fiat currency specified by Initial Token Price (see :ref:`p_curr_launch`). This is done in a similar way to the wealth of Initial Users. (See :ref:`tech_minting` for all the ways that alternative currency can be minted.)


.. _adr_curr_operators:

Which Operators process the transaction and who wins the transaction fee
========================================================================

The settings Maximum Operators Per Transaction and Maximum Percentage Of Operators Per Transaction both put a limit on how many Operators can process a transaction. The smaller of these limits, if it exists is compared with the current number of Operators. If the current number of Operators is not larger than the smaller limit then all Operators are chosen to process the transaction. However, if there are more Operators then just the maximum numbered allowed are chosen randomly to process the transaction. These have been chosen to process the transaction and so will incur variable costs of processing and may get rewarded with transaction fees.

If Just To Winner is set to Yes (see :ref:`p_curr_support`) then an Operator is chosen randomly from the set chosen to process the transaction and is allocated the entire transaction fees for the transaction. If Just To Winner is set to No then all Operators that processed the transaction receive an equal share of the transaction fees for the transaction.



*******************
Token Holding Users
*******************


.. _adr_token_when_convert:

When the Token Holding User will convert tokens into coins
==========================================================

The simulated delay from the start of the Trial to the time when a Token Holding User exchanges all its tokens for alternative currency is exponentially distributed with an average delay given by the Mean Delay Until Using Tokens. See :ref:`tech_delays` for more details and :ref:`p_curr_launch` for related settings.


.. _adr_token_roles:

Which role(s) the Token Holding User will adopt
===============================================

Token Holding Users have already committed to adopting at least one of the four roles for Users, but still have to decide which role(s) they will adopt.

The role combination for each Token Holding User is chosen randomly according to the probabilities in the table of Initial Token Holder Roles. It is not responsive to events in the Trial because it is assumed that Token Holding Users convert their tokens quite quickly. See :ref:`p_curr_launch` for related settings.


.. _adr_token_extras:

The extra characteristics of the Token Holding Users
====================================================

The Token Holding Users have already been given the properties of every User, but also need additional properties according to the roles they have adopted. This is done in the same way as for Initial Users (see :ref:`adr_curr_init_users`).



***************************
The Pool Of Potential Users
***************************


.. _adr_popu_when:

When next to generate Individual Potential Users
================================================

The Pool Of Potential Users considers how many Individual Potential Users to generate at the start of each day, with the number of days between each release being determined by the Time Between Releases. See :ref:`p_popu` for related settings.


.. _adr_popu_many:

How many Individual Potential Users to create
=============================================

The Pool Of Potential Users is represented by 50 sub-totals, equal-sized at first. (The Total Pool Size must be divisible by 50.) Each of the 50 sub-totals represents a sub-pool of people exposed to a different proportion of Total Current Buzz.

The proportion of Total Current Buzz going to each sub-pool is distributed according to a Zipfian distribution,

.. math:: \text{Z}[k; s, N] = {1 \over k^s \sum\limits_{r=1}^{N}{1 \over r^s}}

where :math:`k` is the sub-pool number (with 1 being the sub-pool exposed to the most Buzz), and :math:`N=50`.

By iterative search, the simulator software automatically finds the value of :math:`s` that gives the required Percentage Of Messages Received By Top 20%.

The number of Individual Potential Users created on one occasion, :math:`I` is driven by the Positive Current Buzz, :math:`P`, Negative Current Buzz, :math:`N`, Consideration Rate, :math:`c`, starting number of potential users in each sub-pool, :math:`S_r`, and current number of potential users in each sub-pool, :math:`C_r`, as follows:

.. math:: I = \sum_{r=1}^{50} {\text{round}\bigg( \text{Z}[r;s,50](P+N){C_r \over S_r}c \bigg)}

The number of Individual Potential Users selected from each sub-pool is rounded and the total selected is the sum across all 50 sub-pools.

As an example of the calculation for a single sub-pool, if a sub-pool contains 1,000 potential Users and receives 5% of the Total Current Buzz (Positive Current Buzz + Negative Current Buzz), and if the Total Current Buzz is 2,000 messages (per day), then the sub-pool will receive 100 messages (2,000 x 5%). If, for example, 5% of those messages result in considering use then that is 5 (100 x 5%) Individual Potential Users from that sub-pool.

To take this example a bit further, imagine that the sub-pool initially contained 1,000 potential Users but 400 of these have already become Users leaving only 600 potential Users. If the sub-pool still receives 5% of the Total Current Buzz of 2,000 messages then the sub-pool receives 100 messages as before, but only 60 of these go to people not already Users. That means 3 (60 x 5%) Individual Potential Users will come from that sub-pool.

See :ref:`p_popu` for related settings.
 

 
**************************
Individual Potential Users
**************************


.. _adr_ipu_props:

Initial properties of the Individual Potential User
===================================================

Each Individual Potential User is created with:

*   *an Initial Holding*, which is an initial cash amount in the fiat currency. The amount is drawn randomly according to the Log-normal distribution specified by the settings: Lowest Initial Holding, Mean Initial Holding, and Median Initial Holding. (See :ref:`tech_dist_wealth` for the formulae used and :ref:`p_ipu_all` for related settings.)
*   *propensities to be each of the roles*, generated according to the Normal distribution and using the parameters set (see :ref:`p_ipu_all` for settings).

Other properties related to particular roles are created when a role is adopted for the first time.


.. _adr_ipu_when_opt_in:

When to consider opting in
==========================

The time at which each Individual Potential User will consider opting in is uniformly distributed across a period equal to the Time Between Releases, starting from the creation of the Individual Potential User. This helps to spread out the arrival of new joiners rather than have them all joining at the same instant.


.. _adr_ipu_opt_in:

Whether to opt in and for which role(s)
=======================================

This is one of the most important and complex sets of decisions in a simulaton. Each role is considered independently and, if a decision is taken to opt in to any of the roles, then the Individual Potential User will become an Active User in the selected role(s).

Different factors are important to each role. Each of these decisions is random, but with a probability of opting in that is controlled by a number of criteria. You could think of each criterion as a hurdle to be cleared. The initial assumption is that the probability of opting in is 1 (i.e. it is certain) but multipliers (always in the range 0..1) reduce this step by step. The more worry the User has on each criterion the more the probability of opting in is reduced. Having worked out a probability of opting in the final step is to generate a random number between 0 and 1 and check if it is less than the probability of opting in. If it is, then the User opts in.

For each role, the factors to consider must be chosen as settings from the list offered by the system. (See :doc:`Decision Factor List <decision factor list>` for the full set.)

Then, for each factor, a calibration function type must be chosen. With factors that have a value from zero upwards, labelled 'half' factors, the available function types are :math:`\text{half-asc-1}` and :math:`\text{half-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1'). With factors that can take any value, labelled 'full' factors, the available function types are :math:`\text{full-asc-1}` and :math:`\text{full-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1').

Finally, for each factor, calibration values must be chosen that precisely fix the relationship between the decision factor and the probability multiplier resulting. You are asked to set a Half Level, which is the value for the factor that would give a multiplier of 0.5 (and reduce the probability of opting in by a half), and a Zero Multiplier, which is the value for the multiplier when the factor is zero.

For :math:`\text{half-asc-1}` the Zero Multiplier must be less than 0.5. For :math:`\text{half-desc-1}` the Zero Multiplier must be more than 0.5.

Names for the calibration settings are constructed like this:

  <role> <decision> <factor> <value name>
 
For example, 'Speculator Opt In Current Buzz Positivity Half Level', whose parts are 'Speculator | Opt In | Current Buzz Positivity | Half Level'. 

When all the criteria are considered each gives a multiplier reducing the probability of opting in and the product of all these is the final probability of opting in. This means that if all the factors are very satisfactory to the decision-maker the probability of opting in will be near to 1, but even one criterion on its own can reduce that to near zero if it is worrying enough.

See :ref:`p_ipu_opt_in` for suggested decision factors and calibration function types.


.. _adr_ipu_active_props:

Initial properties as an Active User
====================================

If the Individual Potential User opts in to any of the four roles then it is converted into an Active User with the properties needed for its role(s). If it does not opt in to any role then the Individual Potential User is deleted and the sub-pool from which it was created is incremented by one to put the User back in the Pool Of Potential Users.

The Initial Holding of the Active User is copied from when it was an Individual Potential User. If the Controller provides Gift Coins to new Active Users then these coins are added to the User's holding of alternative currency (see :ref:`p_contr_stocks`). All other properties are determined in the same way as for Initial Users, explained in `The characteristics of the Initial Users in their roles`_.



************
Active Users
************

.. _adr_active_all:

In All Roles
============

.. _adr_active_all_when_hold:

When to consider holding levels
-------------------------------

The best time to reconsider holdings depends on the roles adopted. The ideal (average) time until next reconsideration is calculated for each role the User is in and then the earliest of these times is taken as the reconsideration time.

In each role the delay to the next consideration of holding level is exponentially distributed with an average delay determined by a setting or calculation. This is then multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen for each role. The multipliers can both increase and decrease the average delay. See :ref:`tech_delays` for details.

The initial average is taken from a parameter setting as follows:

+------------+------------------------------------------+
|**Role**    | **Parameter**                            |
+============+==========================================+
| Speculator | Speculator Holding Default Average Delay |
+------------+------------------------------------------+
| Customer   | Customer Holding Default Average Delay   |
+------------+------------------------------------------+
| Merchant   | Merchant Holding Default Average Delay   |
+------------+------------------------------------------+
| Operator   | Operator Holding Default Average Delay   |
+------------+------------------------------------------+

See :ref:`p_active_all_when_hold` for related settings.


.. _adr_active_all_hold:

How much alternative currency and fiat currency to hold
-------------------------------------------------------

The holdings of fiat currency and alternative currency depend on the roles taken by the User. If the User is not a Speculator then its need for currency is determined by the estimated needs of its activities as a Customer, Merchant, and/or Operator, plus a 50% safety margin. See :ref:`tech_holdings_mgmt_reconsider` for details of how these are combined, :ref:`tech_predictions` for details of how the individual predictions are made, and :ref:`p_active_all_hold` for related settings.

However, if the User is a Speculator then some calculation is needed to decide how much would ideally be invested in each currency, and then this is combined with information about the currency needs from any other roles, and the Initial Holding, to decide on the holdings to aim for.

Speculators allocate their holdings according to Kelly's Betting Strategy, which is well-known. They decide the probability of the alternative currency rising in value relative to the fiat currency and then allocate their cash in accordance with that probability. If the probability of the alternative currency rising is judged to be :math:`p` then the proportion of the holdings to be held as alternative currency will be :math:`p` (valued in the fiat currency), with the rest in the fiat currency.

The judged probability of the alternative currency rising is based on decision factors, functions that map factors to probabilities (:math:`\text{half-asc-1}`, :math:`\text{half-desc-1}`, :math:`\text{full-asc-1}`, and :math:`\text{full-desc-1}`), and their calibration settings.

However, the final calculation of the individual probabilities is done using a modified weighted average which requires weights, :math:`w_i`, summing to 1 to be chosen. If there are :math:`n` factors to combine and each produces a probability, :math:`p_i`, then they are combined to make an overall probability as follows:

.. math::

    p_{all} = \sum_{i=0}^n w_i p_i, \ \ \ \text{where } p_0 = 0.5

The idea is that the initial view is that the probability of rising value is 0.5 (represented by :math:`p_0` and :math:`w_0`, which are the initial view rather than decision factors), but as more evidence is considered that view is modified. The weights determine how important each factor is and how much those revise the initial view. (The underlying idea is to use Bayesian updating of beta distributions, but the final formulae are simple.)

With this decided, the holdings of currency are then determined by considering the ideal levels for Speculation, the currency needs of other roles, and the Initial Holding of the User. This is specified in detail in :ref:`tech_holdings_mgmt_reconsider`.


.. _adr_active_all_adj:

How to adjust currency holdings
-------------------------------

The way the User adjusts its holding of *alternative currency* does not require immediate adjustment and is explained in detail in :ref:`tech_adj_delay`.

The way the User adjusts its holding of *fiat currency* is to increase or decrease its holding of fiat currency by drawing on, or returning money to, its other wealth. The value of other wealth is not modelled by the simulator, so really all that is seen is an adjustment to the fiat currency amount. These adjustments are also recorded as history.


As Speculators
==============


.. _adr_active_spec_when_opt_in:

When next to consider opting in/out as a Speculator
---------------------------------------------------

Either the Active User is a Speculator already and will next consider opting out of that role, or the User is not a Speculator (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by the Speculator Opting Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` for technical details and also :ref:`p_active_spec_when_opt_in` for related settings.


.. _adr_active_spec_opt_in:

Whether to opt in as a Speculator
---------------------------------

The logic and settings for the opt in decision as a Speculator while an Active User are the same as those used for opting in as an Individual Potential User. See :ref:`adr_ipu_opt_in` for details.


.. _adr_active_spec_opt_out:

Whether to opt out as a Speculator
----------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors.

Sometimes different criteria are used because Active Users have more information. The factors considered can be chosen and mapped to the final probability of opting out using calibration function types and levels. See :ref:`p_active_spec_opt_out` for related settings.

Also, if an Active User opts out of all roles then it becomes an Opting Out User and will not consider becoming an Active User again in any role. That means that planned considerations of roles are cancelled (or ignored) and instead the Opting Out User just focuses on getting rid of any remaining alternative currency holding.



As Customers
============

.. _adr_active_cust_when_opt_in:

When next to consider opting in/out as a Customer
-------------------------------------------------

Either the Active User is a Customer already and will next consider opting out of that role, or the User is not a Customer (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by Customer Opting Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` and see :ref:`p_active_cust_when_opt_in` for related settings.


.. _adr_active_cust_opt_in:

Whether to opt in as a Customer
-------------------------------

The logic and settings for the opt in decision as a Customer while an Active User are the same as those used for opting in as an Individual Potential User. See :ref:`adr_ipu_opt_in` for details.


.. _adr_active_cust_opt_out:

Whether to opt out as a Customer
--------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors.

Sometimes different criteria are used because Active Users have more information. The factors considered can be chosen and mapped to the final probability of opting out using calibration function types and levels. See :ref:`p_active_cust_opt_out` for related settings.

Also, if an Active User opts out of all roles then it becomes an Opting Out User and will not consider becoming an Active User again in any role. That means that planned considerations of roles are cancelled (or ignored) and instead the Opting Out user just focuses on getting rid of any remaining alternative currency holding.


.. _adr_active_cust_when_shop:

When to shop next
-----------------

Customers shop daily at their chosen time of day.


.. _adr_active_cust_shop:

What to purchase and how
------------------------

When they shop, Customers randomly select a number of Lone Goods to try to buy, a number of Paired Goods to try to buy, and a number of Exclusive Goods to try to buy according to their personal characteristics, and then randomly select goods to try to buy until they have attempted the intended numbers of purchases. For each purchase they must decide what exactly to buy, from whom, and with which payment mechanism.

Looking across all Merchants and the Control Catalogue (if there is one), the Customer identifies the best available:

*   fiat price of the good
*   alternative currency price of the good
*   alternative currency price of the good's substitute.

Each of these may be paid for by using existing stocks of the relevant currency or by obtaining more of the currency at short notice. Therefore, there are up to 6 possibilities to be considered, but some may be ruled out because of low currency stocks or because goods or substitutes are not currently offered by Merchants or the Control Catalogue. All the prices considered allow correctly for the transaction fees involved, as do calculations of the cost of exchanges of currency at short notice. These are explained in :ref:`tech_trans_fees`.

Some comparisons are simple but others are not.

*   The Customer makes no purchase if no Merchant/Control Catalogue offers the good or its substitute, or if the Customer has insufficient currency (of any kind) left. This still counts as a purchase for the purposes of making the intended number of purchases that day.
*   If the Customer has enough currency to purchase the good at an advertised price without obtaining more of the currency at short notice then using the existing stock of currency will be preferred.
*   If the good and its substitute are both offered at a price in the same currency then the item with the best value per unit of the currency will be preferred.
*   The Customer will be indifferent between a good priced in fiat currency but paid for by obtaining alternative currency at short notice and a good priced in alternative currency and paid for by obtaining fiat currency at short notice. In the rare event of a choice being needed, it will be random with equal probabilities. The reason this is likely to be rare is that the Customer has a good stock of both currencies and will be able to buy using the advertised currency more efficiently.

More complicated comparisons are between goods priced in fiat currency and goods priced in alternative currency.

*   In clear cut cases one price will be so much better that even obtaining its currency at short notice (a costly approach) would still be better than using that currency on the other price. The Customer will prefer that price.
*   Where a good is priced in one currency and its substitute is priced in the other, a similar logic applies but now comparing the value per unit currency to see if the situation is clear cut.
*   If a clear cut difference between a fiat currency price and an alternative currency price does not exist then the Customer will choose at random between the prices with equal probability.

For more detail see :ref:`tech_customer_purchase_decisions` and see :ref:`p_active_cust_shop` for related settings.


As Merchants
============


.. _adr_active_merch_when_opt_in:

When next to consider opting in/out as a Merchant
-------------------------------------------------

Either the Active User is a Merchant already and will next consider opting out of that role, or the User is not a Merchant (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by the Merchant Opting Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` and :ref:`p_active_merch_when_opt_in`.


.. _adr_active_merch_opt_in:

Whether to opt in as a Merchant
-------------------------------

The logic and settings for the opt in decision as a Merchant while an Active User are the same as those used for opting in as an Individual Potential User. See :ref:`adr_ipu_opt_in` for details.


.. _adr_active_merch_opt_out:

Whether to opt out as a Merchant
--------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors.

Sometimes different criteria are used because Active Users have more information. The factors considered can be chosen and mapped to the final probability of opting out using calibration function types and levels. See :ref:`p_active_merch_opt_out` for related settings.

Also, if an Active User opts out of all roles then it becomes an Opting Out User and will not consider becoming an Active User again in any role. That means that planned considerations of roles are cancelled (or ignored) and instead the Opting Out user just focuses on getting rid of any remaining alternative currency holding.


.. _adr_active_merch_goods:

Which goods to offer
--------------------

On opting into the role of Merchant:

*   The number of Lone Goods is chosen randomly using a uniform distribution and the Lowest Fraction Of Lone Goods, Highest Fraction Of Lone Goods, and Number Of Lone Goods set.
*   The selected number of Lone Goods are then chosen randomly from the full set, to be offered by the Merchant.
*   The number of Paired Goods is chosen randomly using a uniform distribution and the Lowest Fraction Of Paired Goods, Highest Fraction Of Paired Goods, and Number Of Paired Goods set.
*   The selected number of Paired Goods are then chosen randomly from the full set, to be offered by the Merchant.

See :ref:`p_active_merch_goods` for related settings.


.. _adr_active_merch_when_policy:

When to reconsider the pricing policy
-------------------------------------

The pricing policy is considered immediately on opting in as a Merchant, and also after planned delays.

The delay to the next consideration is exponentially distributed with an average delay determined by Merchant Price Policy Average Delay set, multiplied by multipliers determined by the decision factors, calibration functions, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` and :ref:`p_active_merch_when_policy`.


.. _adr_active_merch_policy:

Which pricing policy to use
---------------------------

The Merchant's two-stage decision is the same regardless of which policy, if any, the Merchant is already using. The options are, first:

*   *No Alternative Currency Prices:* No prices in the alternative currency are offered and the Merchant will not accept alternative currency on any goods.
*   *Alternative Currency Prices:* These are offered in one of two ways, decided in the second stage.

Then, if Alternative Currency Prices is chosen, there is a second choice between:

*   *Fixed:* Prices in alternative currency are set for all goods the Merchant offers and left fixed until a planned revision time.
*   *Floating:* Prices in alternative currency are set at the instant of purchase/enquiry by a Customer.

In all cases, the alternative currency price is the amount needed to obtain the underlying fiat currency price of the good by obtaining fiat in the most efficient immediate way at the time of pricing. Typically, that means exchanging the alternative currency received for fiat currency on a currency exchange using orders already on the Exchange.

For the choice between No Alternative Currency Prices and Alternative Currency Prices a probability is chosen in a way similar to the Opt In decisions, and the final choice is random according to that probability. The starting assumption is that Alternative Currency Prices will be set, but worries create multipliers that reduce the probability from 1 down to something lower.

The factors to consider must be chosen as settings from the list offered by the system. (See :doc:`decision factor list` for the full set.)

Then, for each factor, a calibration function type must be chosen. With factors that have a value from zero upwards, labelled 'half' factors, the available function types are :math:`\text{half-asc-1}` and :math:`\text{half-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1'). With factors that can take any value, labelled 'full' factors, the available function types are :math:`\text{full-asc-1}` and :math:`\text{full-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1').

Finally, for each factor, calibration values must be chosen that precisely fix the relationship between the decision factor and the probability multiplier resulting. You are asked to set a Half Level, which is the value for the factor that would give a multiplier of 0.5 (and reduce the probability of opting in by a half), and a Zero Multiplier, which is the value for the multiplier when the factor is zero.

For :math:`\text{half-asc-1}` the Zero Multiplier must be less than 0.5. For :math:`\text{half-desc-1}` the Zero Multiplier must be more than 0.5.

Names for the calibration settings are constructed like this:

  Merchant Pricing Policy <factor> <value name>
 
For example, 'Merchant Pricing Policy Current Buzz Positivity Half Level', whose parts are 'Merchant Pricing Policy | Current Buzz Positivity | Half Level'. 

When all the criteria are considered each gives a multiplier reducing the probability of Alternative Currency Prices and the product of all these is the final probability of Alternative Currency Prices.

If Alternative Currency Prices are chosen then the second decision is made between Fixed and Floating prices. The approach is similar with an initial assumption of choosing Fixed prices which is reduced by multipliers reflecting worry about various factors --- typically related to volatility.


.. _adr_active_merch_when_price:

When to price next
------------------

When the pricing policy is changed in any way all relevant prices are immediately revised. If No Alternative Currency Prices was chosen then all alternative currency prices the Merchant has are removed. If Floating prices are chosen, any existing Fixed prices are removed.

If a Fixed policy has been chosen the prices are set immediately and after planned delays, influenced by decision factors. If a Fixed policy is confirmed (i.e. it was Fixed before) then prices are not revised until the currently planned time for revising those prices.

The delay to the next re-pricing is exponentially distributed with an average delay determined by Merchant Price Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay. See :ref:`tech_delays` and see :ref:`p_active_merch_when_price` for use in decisions.


.. _adr_active_merch_price:

The prices to use
-----------------

The *alternative currency price* set for a good is always the price that, given the current best immediate exchange method and transaction fees would yield the specified fiat price for the good. There is no attempt to undercut or anticipate rate changes. Prices might be competitive if the exchange and transaction costs are low enough.

The *fiat currency price* to set for a good is always the price that, given the current transaction fees for the fiat currency, would yield the underlying specified price for the good.


As Operators
============


.. _adr_active_oper_when_opt_in:

When next to consider opting in/out as an Operator
--------------------------------------------------

Either the Active User is an Operator already and will next consider opting out of that role, or the User is not an Operator (but has some other role(s) instead) and will next consider opting in to that role.

The delay to the next consideration is exponentially distributed with an average delay determined by Operator Opting Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` and see :ref:`p_active_oper_when_opt_in` for related settings.


.. _adr_active_oper_opt_in:

Whether to opt in as an Operator
--------------------------------

The logic and settings for the opt in decision as an Operator while an Active User are the same as those used for opting in as an Individual Potential User. See :ref:`adr_ipu_opt_in` for details.


.. _adr_active_oper_opt_out:

Whether to opt out as an Operator
---------------------------------

Opt out decisions are similar to opt in but the directions are reversed. It starts with a probability of 1 for opting out, but this is reduced by multipliers driven by the decision factors.

Sometimes different criteria are used because Active Users have more information. The factors considered can be chosen and mapped to the final probability of opting out using calibration function types and levels. See :ref:`p_active_oper_opt_out` for related settings.

Also, if an Active User opts out of all roles then it becomes an Opting Out User and will not consider becoming an Active User again in any role. That means that planned considerations of roles are cancelled (or ignored) and instead the Opting Out user just focuses on getting rid of any remaining alternative currency holding.


****************
Opting Out Users
****************

.. _adr_outing_when_rid:

When next to try to get rid of alternative currency holding
===========================================================

On initially becoming an Opting Out User, the User will immediately try to get rid of the its alternative currency holding. If this is not completely successful immediately then a delay until next consideration will be set.

The delay to when next to try to get rid of the alternative currency holding is exponentially distributed with an average delay determined by the Opting Out Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration functions, and calibration levels chosen. The multipliers can both increase and decrease the average delay. See :ref:`tech_delays` and see :ref:`p_outing_when_rid` for related settings.


.. _adr_outing_rid:

How to get rid of alternative currency holding
==============================================

To get rid of alternative currency, the Opting Out User will purchase goods from Merchants, selecting items at random, until the amount of alternative currency held is too little to buy the chosen good.

To sell the remaining alternative currency, the Opting Out user will use the method that does not require immediate exchange, explained in detail in :ref:`tech_adj_delay`.


*********
Exchanges
*********

All Exchanges
=============

.. _adr_exch_common_init:

Initial characteristics of the exchange
---------------------------------------

At the start of the simulation, the exchanges are created with the initial attributes specified by their settings, see :ref:`p_exch_agents`.


.. _adr_exch_common_when_offer:

When next to consider offering an exchange service for the alternative currency
-------------------------------------------------------------------------------

Either the Exchange is offering an exchange service already and will next consider stopping, or it is not offering a service and will next consider starting.

The delay to the next consideration is exponentially distributed with an average delay determined by Exchange Support Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` and see :ref:`p_exch_common_when_offer` for related settings.


.. _adr_exch_common_start:

Whether to start offering an exchange service for the alternative currency
--------------------------------------------------------------------------

The decision is similar to an opt in decision by a User. The decision is random, but with a probability of opting in that is controlled by a number of criteria. You could think of each criterion as a hurdle to be cleared. The initial assumption is that the probability of starting to offer support is 1 (i.e. it is certain) but multipliers (always in the range 0..1) reduce this step by step. The more worry the Exchange has on each criterion the more the probability of starting support is reduced. Having worked out a probability of starting support the final step is to generate a random number between 0 and 1 and check if it is less than the probability of starting support. If it is, then the Exchange starts support for the alternative currency.

The factors to consider must be chosen as settings from the list offered by the system. (See :doc:`decision factor list` for the full set.)

Then, for each factor, a calibration function type must be chosen. With factors that have a value from zero upwards, labelled 'half' factors, the available function types are :math:`\text{half-asc-1}` and :math:`\text{half-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1'). With factors that can take any value, labelled 'full' factors, the available function types are :math:`\text{full-asc-1}` and :math:`\text{full-desc-1}` (short for 'full-ascending in the range 0-1' and 'full-descending in the range 0-1').

Finally, for each factor, calibration values must be chosen that precisely fix the relationship between the decision factor and the probability multiple resulting. You are asked to set a Half Level, which is the value for the factor that would give a multiplier of 0.5 (and reduce the probability of opting in by a half), and a Zero Multiplier, which is the value for the multiplier when the factor is zero.

For :math:`\text{half-asc-1}` the Zero Multiplier must be less than 0.5. For :math:`\text{half-desc-1}` the Zero Multiplier must be more than 0.5.

Names for the calibration settings are constructed like this:

  Exchange Start <factor> <value name>
 
For example, 'Exchange Start Current Buzz Positivity Half Level', whose parts are 'Exchange Start | Current Buzz Positivity | Half Level'. 

When all the criteria are considered each gives a multiplier reducing the probability of starting support and the product of all these is the final probability of starting support. This means that if all the factors are very satisfactory to the decision-maker the probability of starting support will be near to 1, but even one criterion on its own can reduce that to near zero if it is worrying enough.

See :ref:`p_exch_common_start` for suggested decision factors and calibration function types.

What the Exchange starts support a newsworthy event is generated by the Environment.


.. _adr_exch_common_stop:

Whether to stop offering an exchange service for the alternative currency
-------------------------------------------------------------------------

The decision to stop support is similar to the decision to start, but the directions are reversed. It starts with a probability of 1 for stopping, but this is reduced by multipliers driven by the decision factors.

The factors considered can be chosen and mapped to the final probability of stopping support using calibration function types and levels. See :ref:`p_exch_common_stop` for related settings.

When the Exchange stops support a newsworthy event is generated by the Environment.


Each Continuous Order Driven Exchange (CODE)
============================================


.. _adr_exch_CODE_order:

CODE actions on receiving an order
-----------------------------------

In the simulator, a Continuous Order Driven Exchange maintains no stocks of alternative or fiat currency and simply matches orders that are then settled directly between those placing the orders.

On receipt of a new order, the Exchange tries to match it with orders already lodged on the Exchange until the full quantity has been matched or there are no more orders with acceptable prices. To decide the sequence in which matches are made it uses criteria in the following order:

*   *Price Attractiveness:* The primary criterion is price attractiveness with precedence for orders with better prices. For a new order to buy currency, the existing orders with the lowest prices are taken first. For a new order to sell currency, the existing orders with the highest prices are taken first.
*   *Order Size:* If orders have the same price attractiveness, the next criterion to consider is the quantity. Precedence is given to larger orders so that transactions are simpler.
*   *Order Age:* If orders have the same price attractiveness and size, the next criterion to consider is the date on which the existing order was placed. The earlier orders have precedence.

If there is still a tie then tied orders can be matched in any order.

Regardless of the number of existing orders matched with a new order, the Users collectively are charged for one transaction and the Operators are paid for one transaction.


Each Market Maker (MM)
======================


.. _adr_exch_MM_order:

MM actions on receiving an order
--------------------------------

When an order is received, a Market Maker fulfills it if possible using existing stocks of currency.

*   If the order is to buy alternative currency then the Market Maker receives fiat currency (less the transaction fees charged for fiat currency transactions), calculates the appropriate amount of alternative currency using the advertised Ask price, then transfers alternative currency to the buyer, with more transaction fees being paid.
*   If the order is to buy fiat currency (with alternative currency) then the Market Maker receives alternative currency (less the transaction fees charged for alternative currency transactions), calculates the appropriate amount of fiat currency using the advertised Bid price, then transfers fiat currency to the buyer, with more transaction fees being paid.

If the Market Maker lacks the currency stocks to fulfil the order then it tries to obtain further stocks, either by selling fiat currency to buy alternative currency, or selling alternative currency to buy fiat currency. It does these in the most efficient immediate way it considers. (See :ref:`tech_adj_immediate` for details.) It then fulfills the order if it can.

If the Market Maker cannot obtain sufficient currency stocks to fulfill the order then the order is not met.

Having responded to the order, the Market Maker considers changing its Bid and Ask prices and decides when to review them if no order is received in the meantime.


.. _adr_exch_MM_when_price:

When to consider changing prices
--------------------------------

Each Market Maker considers changing prices immediately on responding to an order and after planned delays. The Market Maker sets those delays immediately on reviewing prices, cancelling previously planned reviews.

The delay to the next consideration is exponentially distributed with an average delay determined by the Market Maker Pricing Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

Each Market Maker has its own rules for this decision.

See :ref:`tech_delays` and see :ref:`p_exch_1`, :ref:`p_exch_2`, and :ref:`p_exch_3` for related settings.


.. _adr_exch_MM_price:

Set/revise Bid and Ask prices
-----------------------------

Market Makers aim to profit from currency exchange rather than from speculation. They want to receive orders and compete with other exchanges (if there are any). The difference between their Bid and Ask prices is crucial to their profit but the levels of those prices drive the level of orders received.

Each Market Maker needs initial Bid and Ask prices and then adjusts them by working out multipliers. The initial prices have a geometric mean that is the same as the current valuation of the alternative currency in the valuation history. If there is no price in the valuation history at the time then the price is taken as 1 (it should quickly adjust). The initial Ask price is 2% higher than the Bid price.

Each exchange whose type is Market Maker has its own rules for adjusting prices, set using decision factors, calibration function types, and calibration values. The objective of these is to arrive at:

*   a multiplier to be applied to the current price level (the geometric mean of the Bid and Ask prices) to produce the new price level; and
*   a multiplier to be applied to the ratio between Bid and Ask prices to produce the new ratio between the Bid and Ask prices.

The new geometric mean and difference in Bid and Ask prices can then be combined to calculate the new Bid and Ask prices.

In symbols, if :math:`B` and :math:`B'` are the Bid price before and after adjustment, :math:`A` and :math:`A'` are the Ask price before and after adjustment, :math:`G` and :math:`G'` are the geometric mean of the prices before and after adjustment, :math:`R` and :math:`R'` are the ratios of Ask and Bid prices before and after adjustment, :math:`m_G` is the multiplier for the geometric mean, and :math:`m_R` is the multiplier for the ratio, then:

.. math::

    G = \sqrt{AB}, \ \ \ G' = m_G G  \\
    R = {A \over B}, \ \ \ R' = m_R R  \\
    A' = m_G A \sqrt{m_R}, \ \ \ B' = m_G B \sqrt{{1 \over m_R}}


.. _adr_exch_MM_when_target:

When to reconsider the target stock of alternative currency
-----------------------------------------------------------

If the decision on Bid and Ask prices uses Deviation From Target Stock Of Alternative Currency as a decision factor then a target is needed. If a target for the stock of alternative currency is not used then it is not necessary to set up this decision. Otherwise, each Market Maker considers the target stock of alternative currency on deciding to offer exchange services, and reconsiders it after planned delays.

Each Market Maker has its own rules for this decision.

The delay to the next consideration is exponentially distributed with an average delay determined by the Market Maker Target Default Average Delay set, multiplied by multipliers determined by the decision factors, calibration function types, and calibration levels chosen. The multipliers can both increase and decrease the average delay.

See :ref:`tech_delays` and see :ref:`p_exch_1`, :ref:`p_exch_2`, and :ref:`p_exch_3` for related settings.


.. _adr_exch_MM_target:

What to use as target for currency stocks
-----------------------------------------

If a target for the stock of alternative currency is not used then it is not necessary to set up this decision. Otherwise, the target level will be set when each Market Maker decides to offer an exchange service for the alternative currency and at planned times.

Each Market Maker has its own rules for this decision.

The target currency stock should balance the need to avoid running out with the risk of holding stock and having it lose value. Exchange operators are not modelled as speculators.

When first offering an exchange service, the target will simply be the E1/2/3 MM Initial Alternative Currency Target Holding set. Later, it is decided by calculating a multiplier that is applied to the current target to produce a new target.


.. _adr_exch_MM_adj:

How to adjust currency stocks
-----------------------------

In most situations Market Makers adjust their currency stocks only by adjusting Bid and Ask prices so that exchanges have the desired effect. The exceptions are:

*   when they run out of alternative currency --- and so have to buy some from other Exchanges or the Controller at short notice (see :ref:`tech_adj_immediate`)
*   when they begin offering an exchange service for the alternative currency --- and so they obtain some more but will tolerate a delay (see :ref:`tech_adj_delay`).


