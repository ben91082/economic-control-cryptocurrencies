#################
Examining Results
#################

.. sectnum:: :start: 13

When a Trial pauses for further instructions, and when a Trial has ended, a wide range of results can be viewed and downloaded as charts, and downloaded as data files in .csv format.

These are of three types:

*   **Snapshot:** This is the current value of the attribute(s) at the time (either the end of a day or the start of a Trial). All Snapshots can be downloaded as .csv files. If a Snapshot is viewed for more than one agent (e.g. all Active Users) then the Snapshot will also be available as a histogram to view and download in .png format.
*   **Event History:** This is for events happening at particular times during a Trial. By default the period of time is the whole Trial so far, but smaller time ranges can be specified. A list of events can be downloaded in .csv format. If the events change a numeric value (e.g. the price on an Exchange) then a chart of this is also available to view and download in .png format.
*   **Daily History:** This is for values that have been recorded at the start/end of each day. By default the period of time is the whole Trial so far, but smaller time ranges can be specified. A list of daily values can be downloaded as a .csv file. If the history is for a numeric value (e.g. valuation of the alternative currency) then a chart is also available to view and download in .png format.

All attributes are available for Snapshots and all Simulation and Trial parameters have a complete Event History. However there are some additional Event and Daily Histories available, as listed below.


+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| **Data**                   | **Event History**                                        | **Daily History**                                                 |
+============================+==========================================================+===================================================================+
| Environment                | * Alternative currency value                             | * Alternative currency valuation                                  |
|                            | * Newsworthy events: descriptions and multipliers        | * Positive Current Buzz, Negative Current Buzz                    |
|                            |                                                          | * Positive Accumulated Buzz, Negative Accumulated Buzz            |
|                            |                                                          | * Controller generated messages and contribution to Buzz          |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| Currency                   |                                                          | * Alternative currency coins issued                               |
|                            |                                                          |                                                                   |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| Token Holding Users        | * Token exchanges                                        | * Count of Token Holding Users                                    |
|                            |                                                          | * Holdings of fiat currency and tokens                            |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| | Pool Of Potential        |                                                          | * Counts of users in each sub-pool                                |
| | Users                    |                                                          |                                                                   |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| | Individual Potential     |                                                          | * Count of Individual Potential Users                             |
| | Users                    |                                                          | * Holdings of fiat currency                                       |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| Active Users               | * Opt in and opt out events                              | * Counts of Active Users, in each role, and each role combination |
|                            | * Transactions                                           | * Holdings of fiat and alternative currency                       |
|                            |                                                          | * Goods offered in total, counts, values, prices                  |
|                            | * Purchases/sales of goods                               | * Total purchases/sales of goods with fiat and alt. values        |
|                            |                                                          | * Gains/losses on transactions and speculation                    |
|                            |                                                          | * Fiat currency adjustments                                       |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| Opting Out Users           | * New Opting Out Users                                   | * Count of Opting Out Users                                       |
|                            | * Deletion of Opting Out Users                           |                                                                   |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| | Exchange ---             | * Exchanges starting/stopping support                    | * Count of Exchanges supporting the alternative currency          |
| | Continuous Order Driven  | * Orders received                                        | * Number of unmatched sale and purchase orders on hand            |
|                            | * Orders matched and completed                           |                                                                   |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| | Exchange ---             | * Exchanges starting/stopping support                    | * Count of Exchanges supporting the alternative currency          |
| | Market Maker             | * Orders received                                        | * Holdings of fiat and alternative currency                       |
|                            | * Orders not met                                         |                                                                   |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+
| Controller                 | * All changes made, one type at a time or all            | * None                                                            |
|                            |                                                          | * Holdings of fiat and alternative currency                       |
+----------------------------+----------------------------------------------------------+-------------------------------------------------------------------+

