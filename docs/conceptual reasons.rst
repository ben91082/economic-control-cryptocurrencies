########################################
Reasons For Using Alternative Currencies
########################################

.. sectnum:: :start: 3

People will use alternative currencies for a variety of reasons. Not all of these can be captured by the simulator but many can, at least approximately.


*********************
Non-Financial Reasons
*********************

Some potentially important reasons are as follows:

*   **Exclusive Goods:** To buy goods/services that can only be bought using the alternative currency.
*   **Smart Contracting:** To use electronic systems that help to enforce and execute contracts, potentially enabling trade between parties that initially have low mutual trust.
*   **Exploration:**  To learn about a technology that might become important.
*   **Social Reasons:** For political or moral reasons, or to foster a community.
*   **Style:** To demonstrate qualities such as modernity or willingness to take risks.
*   **Crime:** A wide range of dishonest behaviours have been linked to alternative currencies, particularly cryptocurrencies. The motive might be to hide transactions, income, or wealth to evade tax, hide crime, or avoid identification. It might be part of a scam to cheat gullible speculators.

Most of these reasons cannot be modelled explicitly by the simulator. However, it is possible to create personal propensities to use the alternative currency that represent the effect of these reasons. The propensities of individuals will be randomly distributed, but according to a distribution that is controllable. For example, it is possible to make people, on average, more or less willing to get involved, or to change the difference in willingness between the most and least enthusiastic.


*****************
Financial Reasons
*****************

To Gain From Financial Speculation
==================================

This has been a key reason for Bitcoin especially and other cryptocurrencies. Speculators may hope to gain:

*   **honestly** by being smarter, better informed, faster, or luckier than other speculators; or
*   **dishonestly** by market manipulation (especially using pump and dump schemes i.e. manipulating news about the currency while buying and selling at just the right time).


To Exploit Flawed Prices
========================

Two examples of this are as follows:

*   **Differences Between Exchanges:** If two currency exchanges offer different prices for the same alternative currency simultaneously it can be profitable to buy on one and simultaneously sell on the other. People doing this help keep prices across different exchanges closer together.
*   **Dual Priced Goods:** If goods are advertised for sale at two prices --- one a fiat currency price and the other an alternative currency price --- then it might be that the prices are not exactly consistent with the best current exchange rate so that it is cheaper to pay the alternative currency price. People exploiting pricing errors like this encourage frequent price revision, but if prices are not changed and the goods are sold in large quantities then it could be that the exchange rate is brought into line with the prices of goods rather than the other way around.


For Certainty Of Value
======================

Even well-established fiat currencies of leading developed countries change their value over time due to inflation and exchange rate changes. Alternative currencies can use a number of methods to control their value.

*   **Link To A Fiat Currency:** An alternative currency can be tied to a trusted fiat currency so that the alternative currency can always be exchanged for a predictable quantity of the fiat currency. 
*   **Link To Real Goods:** Alternative currencies tied effectively to well-defined goods can provide certainty, at least in respect of those goods. For example, if you buy a first class postage stamp and leave it, forgotten, in a drawer for five years, it will still be good for sending a letter first class, even if the price of a new stamp (and of sending a first class letter) has changed. Alternative currencies might be linked to things like a chocolate bar, a haircut, a kilogram of steel, or a kilowatt-hour of electricity.
*   **Money Supply Control:** One reason for inflation is a mis-match between the supply and demand for a currency. In future there might be electronically controlled currencies that manage their supply so well that inflation is all but eliminated.


To Save Money
=============

Alternative currencies face two inherent disadvantages compared with typical fiat currencies when it comes to users saving money: (1) they are typically operating on a much smaller scale, so do not benefit from economies of scale, and (2) they are not accepted in payment for as many goods.

Alternative currencies can sometimes be more efficient and, even when not more efficient, they may be cheaper for some users in some situations than fiat currency alternatives. Some of these apparent price advantages cannot be continued indefinitely.

*   **True Efficiency Per Transaction:** A system that is very, very frugal could be competitive with fiat currency alternatives and consistently offer more efficient transactions when both buyer and seller costs are considered. It might be more efficient in its use of computer resources or be more completely automated.
*   **International Circulation:** When a customer in one country buys goods from a merchant in another country there are usually costs related to currency exchange and the payment transaction. Converting from one fiat currency to an alternative currency and then on to the second fiat currency would usually be inefficient because it involves, in effect, two exchange transactions rather than one, plus the purchase transaction. However, if both parties maintain a balance of the alternative currency and participate in a community of alternative currency users who collectively sell goods in both directions between the two countries it may not be necessary to exchange the alternative currency for any fiat currency. The alternative currency then operates as a global currency, albeit with a small group of users. To gain this benefit a user has to both send and receive the alternative currency (ideally to about the same extent) and collectively the cross-border trade in both directions needs to be about equal. This is something that companies do with USD and some other globally respected currencies, but an alternative currency might achieve it for ordinary individuals, who find each other, in part, through their use of the currency.
*   **No Frills Service:** A payment system might be less expensive because it cuts out some benefits provided by competitors, such as for security, reporting, fraud protection, recovery from errors, and insurance.
*   **Better Competition:** There may be situations where alternative currency is cheaper to use because the established alternative is provided by a monopolist, or oligopoly.
*   **Accepting Less Profit:** Operators of alternative currency payment systems might be prepared to accept less profit than providers of fiat currency payment systems (at least initially) and so might offer a cheaper service even though their technology and processes are not more efficient.
*   **Temporary Loss Leaders:** Promoters of an alternative currency might subsidise it for a period.
*   **Funding By Speculators:** An alternative currency might be paid for by giving operators of its systems amounts of newly minted alternative currency that are exchangeable for fiat currency they can spend. How this works is hard to understand but the effect is that the operators are being paid to a large extent by speculators when they buy the coins. Viewed in the long term it would be seen that the speculators on average lose fiat currency and the operators gain it.
*   **Cross Subsidy:** Another way that the true costs of a payment system can be hidden is by charging them to some users but not to others. In particular, some users might pay a high fee to gain some special service level while other users get their transactions free.
*   **Merchant Pays All:** A typical arrangement is for merchants to pay the entire cost of transactions when they receive money, making the process seem free to customers who are paying. Once it is established that customers do not have to pay more for using the system (as in the case of debit and credit cards) the merchant just has to accept less money (though of course it may be possible to increase the price of goods to compensate).


***************
Further Reading
***************

Ali, R., Barrdear, J., Clews, R., & Southgate, J. (2014). The economics of digital currencies. *Bank of England Quarterly Bulletin*, Q3. Available online `here`__.

.. __: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2499418

Khairuddin, I. E., Sas, C., Clinch, S., & Davies, N. (2016, May). Exploring motivations for bitcoin technology usage. In *Proceedings of the 2016 CHI Conference Extended Abstracts on Human Factors in Computing Systems* (pp. 2872-2878). ACM. Available online `here`__.

.. __: http://eprints.lancs.ac.uk/78254/1/Exploring_Motivations_among_Bitcoin_Users_Final_Submission.pdf

Spenkelink, H. (2014). *The adoption process of cryptocurrencies. Identifying factors that influence the adoption of cryptocurrencies from a multiple stakeholder perspective*. Available online `here`__.

.. __: https://pdfs.semanticscholar.org/5c0d/bbf5c9aa38766d61eac90a0258b4d7d97f6f.pdf
