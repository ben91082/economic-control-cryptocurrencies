import random
from datetime import timedelta
import math
from .exchange import BuyOrder, SellOrder
from .simulator import Event

from .constants import (
    PROB_LEAVE, PROB_JOIN, BASETIME, DAY, NUMBER_OF_AGENTS, TOTAL_NUMBER_DAYS, AVERAGE_WAIT_FOR_REVIEW, 
    PROBABILITY_COLLAPSE, AVG_FIAT_HOLDING, STARTING_CRYPTO_PRICE, AVG_CC_HOLDING, BUZZ
)

class Agents(object):
    """ 
    One class to represent all agents in the simulation
    All callbacks need to be passed the agent id
    """

    def __init__(self, simulation, exchange):
        self.simulation = simulation
        self.exchange = exchange
        # Add a special event handler for a collapse in the cryptocurrency
        simulation.subscribe('collapse', self.onCurrencyCollapse)

    def get_sentiment(self, buzz):
        abr = 2,6
        alpha = buzz * (abr[1] - abr[0]) + abr[0]
        beta = buzz * (abr[0] - abr[1]) + abr[1]
        return random.betavariate(alpha, beta)

    def add(self):
        avg_fiat_holding = self.simulation.param('avg_fiat_holding')
        avg_cc_holding = self.simulation.param('avg_cc_holding')
        buzz = self.simulation.param('buzz')
        sentiment = self.get_sentiment(buzz)
        if sentiment > 0.5:
            initial_cc_holding = max(1, random.normalvariate(avg_cc_holding, math.sqrt(avg_cc_holding)))
        else:
            initial_cc_holding = 0

        initial_fiat_holding = max(1, random.normalvariate(avg_fiat_holding, math.sqrt(avg_fiat_holding)))

        my_state = {
            'initial_fiat_holding':  initial_fiat_holding, 
            'fiat_holding':  initial_fiat_holding,
            'initial_cc_holding':  initial_cc_holding,
            'cc_holding': initial_cc_holding,
            'sentiment': sentiment,
            'agent_id': len(self.simulation.agents)
        }

        self.simulation.agents.append(my_state)

    def schedule(self, agent_id):
        average_wait_for_review = self.simulation.param('average_wait_for_review')
        wait = max(1, random.normalvariate(average_wait_for_review * DAY, math.sqrt(average_wait_for_review) * DAY))
        event = Event(wait, self.evaluate, [agent_id])
        self.simulation.add_event(event)
        return wait

    def purchase_complete(self, quantity, price, agent_id):
        my_state = self.simulation.agents[agent_id]
        total = quantity * price
        my_state['cc_holding'] += quantity
        my_state['fiat_holding'] -= total
        if self.simulation.debug:
            self.simulation.log('Agent %s bought %s units of crypto at %s per unit for a total of %s, new ccholding = %s, new fiat_holding = %s' % (agent_id, quantity, price, total, my_state['cc_holding'], my_state['fiat_holding']))

    def sale_complete(self, quantity, price, agent_id):
        my_state = self.simulation.agents[agent_id]
        total = quantity * price
        my_state['cc_holding'] -= quantity
        my_state['fiat_holding'] += total
        if self.simulation.debug:
            self.simulation.log('Agent %s sold %s units of crypto at %s per unit for a total of %s, new ccholding = %s, new fiat_holding = %s' % (agent_id, quantity, price, total, my_state['cc_holding'], my_state['fiat_holding']))

    def evaluate(self, agent_id):
        my_state = self.simulation.agents[agent_id]
        buzz = self.simulation.param('buzz')
        timenow = BASETIME + timedelta(seconds=self.simulation.time)
        new_sentiment = self.get_sentiment(buzz)
        if self.simulation.debug:
            self.simulation.log('Agent %s updated their sentiment from %s to %s (buzz level was %s)' % (agent_id, my_state['sentiment'], new_sentiment, buzz))
        my_state['sentiment'] = new_sentiment
        best_buy_price = self.exchange.get_best_buy_price(agent_id)
        max_can_buy = my_state['fiat_holding'] / best_buy_price
        
        sent0to1 = my_state['sentiment'] * 2 - 1
        if my_state['sentiment'] > 0.5:
            quantity_to_buy = sent0to1 * max_can_buy

            sf = 0.4 * my_state['sentiment'] + 0.8
            best_price = self.exchange.get_best_buy_price(agent_id)
            buy_price_to_offer = best_price * sf

            self.exchange.cancel_all_agent_buy_orders(agent_id)
            buy_order = BuyOrder(quantity_to_buy, buy_price_to_offer, self.simulation.time, self.simulation.time + 30 * DAY, agent_id, self)
            if self.simulation.debug:
                self.simulation.log('\nagent %s placed buy order -> quantity: %s, price: %s, time: %s, best_price: %s, sentiment: %s\n' % (agent_id, quantity_to_buy, buy_price_to_offer, self.simulation.time, best_price, my_state['sentiment']))
            self.exchange.receive_buy_order(buy_order)

        if my_state['cc_holding'] > 0:
            if my_state['sentiment'] < 0.5:
                quantity_to_sell = my_state['cc_holding']
            else:
                quantity_to_sell = (1 - sent0to1) * my_state['cc_holding']
            best_price = self.exchange.get_best_sell_price(agent_id)

            if buzz > 0.5:
                sf = random.uniform(1, buzz * 0.1 + 1)
            else:
                sf = random.uniform(0.98, buzz * 0.05 + 0.98)

            # sf = my_state['sentiment'] + 0.5
            sell_price_to_offer = best_price * sf
            self.exchange.cancel_all_agent_sell_orders(agent_id)
            sell_order = SellOrder(quantity_to_sell, sell_price_to_offer, self.simulation.time, self.simulation.time + 30 * DAY, agent_id, self)
            if self.simulation.debug:
                self.simulation.log('\nagent %s placed sell order -> quantity: %s, price: %s, time: %s, best_price: %s, sentiment: %s\n' % (agent_id, quantity_to_sell , sell_price_to_offer, self.simulation.time, best_price, my_state['sentiment']))
            self.exchange.receive_sell_order(sell_order)

        wait = self.schedule(agent_id)


    def onCurrencyCollapse(self):
        """
        Event handler for the 'collapse' event.
        For now lets say everyone just pulls out!
        """
        pass
        # for agent in self.simulation.agents:
        #     agent['participating'] = False