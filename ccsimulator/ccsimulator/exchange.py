class Order(object):
    
    def __init__(self, quantity, price, time, expires, agent_id, agents):
        self.quantity = quantity
        self.price = price
        self.time = time
        self.expires = expires
        self.agent_id = agent_id
        self.type = None

        # track how much of the order has been fulfilled so we know fulfilled + quantity = original order quantity
        self.fulfilled = 0

        # instance of the agents class so the appropriate agent can be notified when a transaction completes.
        self.agents = agents

    def transact(self, quantity, price):
        # price may not be the price of this offer, it may be the matched offer
        quantity = min(quantity, self.quantity)
        self.fulfilled += quantity
        self.quantity -= quantity
        if self.type == 'Buy':
            self.agents.purchase_complete(quantity, price, self.agent_id)
        if self.type == 'Sell':
            self.agents.sale_complete(quantity, price, self.agent_id)

    def is_fulfilled(self):
        return self.quantity <= 0

class BuyOrder(Order):

    def __init__(self, *args):
        super().__init__(*args)
        self.type = 'Buy'

    def __lt__(self, other):
        return (self.price < other.price or (self.price == other.price and self.time > other.time))

    def __le__(self, other):
        return (self.price < other.price or (self.price == other.price and self.time >= other.time))

    def __eq__(self, other):
        return (self.price == other.price and self.time == other.time)

    def __ne__(self, other):
        return (self.price != other.price or self.time != other.time)

    def __gt__(self, other):
        return (self.price > other.price or (self.price == other.price and self.time < other.time))

    def __ge__(self, other):
        return (self.price > other.price or (self.price == other.price and self.time <= other.time))

class SellOrder(Order):

    def __init__(self, *args):
        super().__init__(*args)
        self.type = 'Sell'

    def __lt__(self, other):
        return (self.price > other.price or (self.price == other.price and self.time > other.time))

    def __le__(self, other):
        return (self.price > other.price or (self.price == other.price and self.time >= other.time))

    def __eq__(self, other):
        return (self.price == other.price and self.time == other.time)

    def __ne__(self, other):
        return (self.price != other.price or self.time != other.time)

    def __gt__(self, other):
        return (self.price < other.price or (self.price == other.price and self.time < other.time))

    def __ge__(self, other):
        return (self.price < other.price or (self.price == other.price and self.time <= other.time))

class Exchange(object):

    def __init__(self, simulation):
        self.simulation = simulation

    def record_transaction(self, buying_agent_id, selling_agent_id, quantity, price):
        if self.simulation.debug:
            self.simulation.log('Agent %s bought %s worth of crypto from Agent %s at %s' % (buying_agent_id, quantity, selling_agent_id, price))
        self.simulation.transactions.append([buying_agent_id, selling_agent_id, quantity, price, self.simulation.time])
        self.simulation.transactions_today += 1
        self.simulation.volume_transactions_today += quantity

    def get_current_price(self):
        if len(self.simulation.transactions) == 0:
            current_price = self.simulation.param('starting_crypto_price')
        else:
            current_price = self.simulation.transactions[-1][3]
        return current_price

    def get_best_buy_price(self, agent_id): 
        orders = [o for o in self.simulation.sell_orders if o.agent_id != agent_id]
        if len(orders) == 0:
            return self.get_current_price()
        else:
            return orders[-1].price

    def get_best_sell_price(self, agent_id):
        orders = [o for o in self.simulation.buy_orders if o.agent_id != agent_id]
        if len(orders) == 0:
            return self.get_current_price()
        else:
            return orders[-1].price

    def receive_buy_order(self, order):
        assert isinstance(order, BuyOrder)
        order = self.match_buy_order(order)
        
        # If we couldn't fulfil it straight away then add to the list.
        if not order.is_fulfilled():
            self.simulation.buy_orders.append(order)
            self.simulation.buy_orders.sort()

    def receive_sell_order(self, order):
        assert isinstance(order, SellOrder)
        order = self.match_sell_order(order)

        # If we couldn't fulfil it straight away then add to the list.
        if not order.is_fulfilled():
            self.simulation.sell_orders.append(order)
            self.simulation.sell_orders.sort()

    def match_buy_order(self, buy_order):
        assert isinstance(buy_order, BuyOrder)
        
        if len(self.simulation.sell_orders) > 0:
            i = len(self.simulation.sell_orders) - 1
            sell_order = self.simulation.sell_orders[i]
            while buy_order.price >= sell_order.price and not buy_order.is_fulfilled():
                if buy_order.agent_id != sell_order.agent_id:
                    quantity = min(buy_order.quantity, sell_order.quantity)
                    buy_order.transact(quantity, sell_order.price)
                    sell_order.transact(quantity, sell_order.price)
                    self.record_transaction(buy_order.agent_id, sell_order.agent_id, quantity, sell_order.price)

                if sell_order.is_fulfilled():
                    # Then remove the order from the list and look at the next one
                    self.simulation.sell_orders.pop(i)
                if i == 0: break
                i -=1
                sell_order = self.simulation.sell_orders[i]

        # Done matching - return the buy order in whatever state
        return buy_order

    def match_sell_order(self, sell_order):
        assert isinstance(sell_order, SellOrder)

        if len(self.simulation.buy_orders) > 0:
            i = len(self.simulation.buy_orders) - 1
            buy_order = self.simulation.buy_orders[i]
            while sell_order.price <= buy_order.price and not sell_order.is_fulfilled():
                if buy_order.agent_id != sell_order.agent_id:
                    quantity = min(sell_order.quantity, buy_order.quantity)
                    sell_order.transact(quantity, buy_order.price)
                    buy_order.transact(quantity, buy_order.price)
                    self.record_transaction(buy_order.agent_id, sell_order.agent_id, quantity, buy_order.price)

                if buy_order.is_fulfilled():
                    # Then remove the order from the list and look at the next one
                    self.simulation.buy_orders.pop(i)
                if i == 0: break
                i -=1
                buy_order = self.simulation.buy_orders[i]

        # Done matching - return the sell order in whatever state
        return sell_order

    def get_buy_orders_by_agent(self, agent_id):
        return [(i, order) for i, order in enumerate(self.simulation.buy_orders) if order.agent_id == agent_id]
    
    def get_sell_orders_by_agent(self, agent_id):
        return [(i, order) for i, order in enumerate(self.simulation.sell_orders) if order.agent_id == agent_id]

    def cancel_buy_order(self, order_id):
        # Order id is just the sequence number
        self.simulation.buy_orders.pop(order_id)

    def cancel_sell_order(self, order_id):
        # Order id is just the sequence number
        self.simulation.sell_orders.pop(order_id)

    def cancel_all_agent_buy_orders(self, agent_id):
        for order_id, order in self.get_buy_orders_by_agent(agent_id):
            self.cancel_buy_order(order_id)

    def cancel_all_agent_sell_orders(self, agent_id):
        for order_id, order in self.get_sell_orders_by_agent(agent_id):
            self.cancel_sell_order(order_id)