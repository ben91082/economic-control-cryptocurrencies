import random
from datetime import timedelta
import math
from ccsimulator.exchange import BuyOrder, SellOrder

from .constants import (
    PROB_LEAVE, PROB_JOIN, BASETIME, DAY, NUMBER_OF_AGENTS, TOTAL_NUMBER_DAYS, AVERAGE_WAIT_FOR_REVIEW, 
    PROBABILITY_COLLAPSE, AVG_FIAT_HOLDING, STARTING_CRYPTO_PRICE, AVG_CC_HOLDING, BUZZ
)

class Event(object):
    
    def __init__(self, time, callback, args):
        # When creating the event, the time should be a delay
        # The simulation will convert to an absolute time once the event has been added to the queue
        self.time = time

        # N.B. Python functions are just objects like anything else which can be passed around.
        # Callback should be an actual function which we can call directly rather than passing function names 
        # This avoids all the potentially messy if event.type == 'agent.evaluate': agent.evaluate() 
        self.callback = callback

        # The arguments to be passed to the callback function
        self.args = args 

    # Python magic methods below allow event objects to be compared to each other and hence ordered
    # We are only ordering on time at the moment, so this isn't doing anything too special but 
    # we but may want to include priority later
    def __lt__(self, other):
        return self.time < other.time

    def __le__(self, other):
        return self.time <= other.time

    def __eq__(self, other):
        return self.time == other.time

    def __ne__(self, other):
        return self.time != other.time

    def __gt__(self, other):
        return self.time > other.time

    def __ge__(self, other):
        return self.time >= other.time

    def process(self):
        # The * is special python syntax to expand a list into function arguments
        # e.g callback(*[a,b,c]) is equivalent to callback(a,b,c)
        self.callback(*self.args)

class TrialEnded(Exception):
    pass

class StepEnded(Exception):
    pass

class Simulation(object):

    def __init__(self):
        # Anything that needs to be committed to disk needs to go into this dictionary
        # We expect the defaults here to be overriden by load_state method.
        self.__data__ = {
            "eventq": [],
            "params": self.default_params(),
            "buy_orders" : [],
            "sell_orders" : [],
            "transactions" : [],
            "transactions_today": 0,
            "volume_transactions_today": 0,
            "stats": [],
            "_log": [],
            "steps": [],
            "agents": [],
            "trials": [],
            "time": 0,
            "day": 0,
            "next_step_end": 0,
            "trial_end": TOTAL_NUMBER_DAYS * DAY,
            "debug": False
        }

        # Allow actors in our simulation to broadcast and subscribe to specific events
        self.__event_handlers__ = {}

    def default_params(self):
        return {
            "agents_n": NUMBER_OF_AGENTS,
            "days_n": TOTAL_NUMBER_DAYS,
            "average_wait_for_review" : AVERAGE_WAIT_FOR_REVIEW,
            "prob_collapse": PROBABILITY_COLLAPSE,
            "avg_fiat_holding": AVG_FIAT_HOLDING,
            "starting_crypto_price":  STARTING_CRYPTO_PRICE,
            "avg_cc_holding": AVG_CC_HOLDING,
            "buzz": BUZZ
        }

    def param(self, name):
        """
        Get a simulation parameter, this may be stored in 'params' which means it can not change throughout the duration of the trial
        Or we may find it in one of the steps 
        """
        if len(self.steps) > 0:
            for step in reversed(self.steps):
                try:
                    return step['params'][name]
                except KeyError:
                    continue
            # Try in the params dict and allow the KeyError to be thrown if not present
            return self.params[name]
        else:
            return self.params[name]

    def __getattr__(self, name):
        """
        Use some python magic so that we can keep all trial data/parameters bundled together in a single dictionary __data__
        that is very easy to export or load from disk, but still access data as if they were normal class attributes
        e.g. simulation.day works instead of simulation.__data__['day']
        N.B. Python tries normal attribute access BEFORE calling this method
        """
        if name in self.__data__:
            return self.__data__[name]
        elif name == 'debug':
            return False
        else:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        """
        The reverse of above, so for example we can do simulation.day = 10 as opposed to simulation.__data__['day'] = 10
        N.B. This is called INSTEAD of python's normal mechanism for setting attributes
        """
        if name[:2] == '__':
            # Special attributes prefixed with '__' are set as normal
            object.__setattr__(self, name, value)
        else:
            # Everything else is put inside __data__
            self.__data__[name] = value

    def run_step(self):
        for event in self.run():
            pass

    def set_run_time(self):
        if len(self.steps) == 0:
            raise Exception('Need to add at least one step first!')
        self.next_step_end = self.steps[-1]['day_range'][1] * DAY

    def run(self):
        self.set_run_time()
        while len(self.eventq) > 0:
            try:
                yield self.process_next_event()
            except TrialEnded:
                print('End of trial (Day %s)' % self.day)
                return
            except StepEnded:
                print('End of current step: (Day %s)' % self.day)
                return
        
        # We should never get here if agents keep registering events!
        print('Ran out of events to process!')        

    def process_next_event(self):
        event = self.eventq[0]
        if event.time > self.trial_end:
            self.time = self.trial_end
            raise TrialEnded()
        if event.time > self.next_step_end:
            self.time = self.next_step_end
            raise StepEnded()
        self.time = event.time
        event.process()
        return self.eventq.pop(0)

    def add_event(self, event):
        # Event is initially created with a delay, convert to absolute time
        event.time = event.time + self.time
        
        # Loop through existing queue and make sure we place this event in the correct position
        for i in range(len(self.eventq)):
            if event < self.eventq[i]:
                self.eventq.insert(i, event)
                return
        self.eventq.append(event)

    def collect_stats(self):
        if len(self.transactions) == 0:
            current_price = self.param('starting_crypto_price')
        else:
            current_price = self.transactions[-1][3]
        participation = 0
        for agent in self.agents:
            if agent['cc_holding'] > 0:
                participation += 1

        self.stats.append({'current_price' : current_price, 'participation': participation, 'day': self.day, 'transactions_today': self.transactions_today, 'volume_transactions_today': self.volume_transactions_today})
        self.transactions_today = 0
        self.volume_transactions_today = 0
        if self.debug:
            self.log('## END OF DAY %s, current_price = %s, participation = %s, # buy orders=%s, # of sell orders = %s' % (self.day, current_price, participation, len(self.buy_orders), len(self.sell_orders)))

    def log(self, text):
        """
        Simple text logging for debugging purposes
        """
        self._log.append(text)

    def record_step(self, params, days_to_run):
        if len(self.steps) > 0 and self.get_last_step()['day_range'][1] >= self.param('days_n'):
            raise TrialEnded
        next_step = {
            'params': params,
            'day_range': [self.day, min(self.day + days_to_run, self.param('days_n'))]
        }
        if len(self.steps) > 0:
            last_step = self.get_last_step()
            if last_step['day_range'][0] == self.day:
                # Previous step hasn't started
                last_step.update(next_step)
            elif last_step['day_range'][1] > self.day:
                raise Exception("The previous step hasn't finished yet!")
            elif last_step['day_range'][1] == self.day:
                # Previous step is complete so we are adding a new one
                self.steps.append(next_step) 
        else:
            self.steps.append(next_step)

    def trial_ended(self):
        return self.time == self.trial_end and self.time > 0

    def trial_not_started(self):
        return len(self.eventq) == 0 and self.time == 0

    def has_steps_pending(self):
        return self.next_step_end > self.time

    def reset_current_trial(self):
        self.stats = []
        self.eventq = []
        self.buy_orders = []
        self.sell_orders = []
        self.transactions = []
        self._log = []
        self.steps = []
        self.agents = []
        self.time = 0
        self.day = 0
        self.next_step_end = 0
        self.trial_end = self.param('days_n') * DAY

    def save_trial(self):
        self.trials.append(self.stats)

    def export_state(self):
        return self.__data__

    def load_state(self, state):
        self.__data__.update(state)
        self.trial_end = self.param('days_n') * DAY

    def subscribe(self, event_name, handler):
        "Add a handler for a specific event"
        if event_name not in self.__event_handlers__:
            self.__event_handlers__[event_name] = []
        self.__event_handlers__[event_name].append(handler)

    def broadcast(self, event_name):
        "Allow an actor to broadcast that a specific event occured"
        if event_name in self.__event_handlers__:
            for handler in self.__event_handlers__[event_name]:
                handler()

    def check_step_progress(self):
        return self.day, self.next_step_end / DAY, self.param('days_n')

    def remove_last_step(self):
        if len(self.steps) > 0:
            del self.steps[-1]
            if len(self.steps) > 0:
                self.next_step_end = self.steps[-1]['day_range'][1] * DAY
            else:
                self.next_step_end = 0

    def get_last_step(self):
        if len(self.steps) > 0:
            return self.steps[-1]
        else:
            # There may not be a step yet, in which case just return main parameters for the simulation
            return {
                "params": self.params,
                "day_range": [0,0]
            }

    def export_plot_data(self):
        return {
            'plot': self.stats,
            'agents_n': self.param('agents_n'),
            'days_n': self.param('days_n')
        }

class Day(object):
    """
    Used to schedule events at the end of every day which trigger the collection of stats
    """
    def __init__(self, simulation):
        self.simulation = simulation
        
    def schedule(self):
        event = Event(DAY, self.process, [])
        self.simulation.add_event(event)

    def process(self):
        self.simulation.day += 1

        """
        # Example of an event broadcase
        if random.random() < self.simulation.param('prob_collapse'):
            self.simulation.broadcast('collapse')
            self.simulation.log('### COLLAPSE!! ###')
        """

        self.simulation.collect_stats()
        self.schedule()