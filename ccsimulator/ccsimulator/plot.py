import matplotlib.pyplot as plt

def plot(total_number_days, this_trial, all_trials, key):
    # Plot all trial data
    x = list(range(total_number_days))
    this_trial = [y[key] for y in this_trial]
    if len(this_trial) < len(x):
        # Pad out current stats with missing values
        y = this_trial + list([None for i in range(len(x) - len(this_trial))])
    else:
        y = this_trial
    subplots = plt.subplots()
    ax = subplots[1]
    ax.plot(x, y)
    for trial in all_trials:
        ax.plot(x, [y[key] for y in trial])
    plt.plot(this_trial)
    plt.show()
