import unittest
from ccsimulator.exchange import BuyOrder, SellOrder, Exchange
from ccsimulator.simulator import Day, Agents, Simulation

class TestExchange(unittest.TestCase):

    def setUp(self):
        self.simulation = Simulation()
        self.exchange = Exchange(self.simulation)
        self.agents = Agents(self.simulation, self.exchange)
        self.number_agents = 12
        for i in range(self.number_agents):
            self.agents.add()

    def test_buy_order_equal(self):
        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertEqual(o1, o2)

    def test_buy_order_not_equal(self):
        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertNotEqual(o1, o2)    

        o1 = BuyOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertNotEqual(o1, o2)    

    def test_buy_order_greater_than(self):
        o1 = BuyOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreater(o1, o2)    

        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreater(o1, o2)    

    def test_buy_order_less_than(self):
        o1 = BuyOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLess(o2, o1)    

        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLess(o2, o1) 

    def test_buy_order_greater_than_equal(self):
        o1 = BuyOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreaterEqual(o1, o2) 

        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreaterEqual(o1, o2) 

        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertGreaterEqual(o1, o2) 

    def test_buy_order_less_than_equal(self):
        o1 = BuyOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLessEqual(o2, o1) 

        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLessEqual(o2, o1) 

        o1 = BuyOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = BuyOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertLessEqual(o2, o1) 


    def test_sell_order_equal(self):
        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertEqual(o1, o2)

    def test_sell_order_not_equal(self):
        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertNotEqual(o1, o2)    

        o1 = SellOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertNotEqual(o1, o2)    

    def test_sell_order_greater_than(self):
        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 11, 1001, 3000, 0, self.agents)
        self.assertGreater(o1, o2)    

        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreater(o1, o2)    

    def test_sell_order_less_than(self):
        o1 = SellOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLess(o1, o2)    

        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLess(o2, o1) 

    def test_sell_order_greater_than_equal(self):
        o1 = SellOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreaterEqual(o2, o1) 

        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertGreaterEqual(o1, o2) 

        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertGreaterEqual(o1, o2) 

    def test_sell_order_less_than_equal(self):
        o1 = SellOrder(10, 11, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLessEqual(o1, o2) 

        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1001, 3000, 0, self.agents)
        self.assertLessEqual(o2, o1) 

        o1 = SellOrder(10, 10, 1000, 2000, 0, self.agents)
        o2 = SellOrder(20, 10, 1000, 3000, 0, self.agents)
        self.assertLessEqual(o2, o1) 

    def test_sort_sell_orders(self):
        orders = []
        for price, time in ((19.7, 5), (20.1, 6), (19.5, 6), (20.3, 9), (19.8, 5), (20.3, 8), (20.1, 7), (20.7, 5), (19.8, 5), (20.6, 8), (20.5, 7), (20.2, 6), (19.9, 9), (20.5, 6)):
            orders.append(SellOrder(1, price, time, 0, 0, self.agents))
        orders.sort()

        sorted_orders = ((20.7,5), (20.6,8), (20.5,7), (20.5,6), (20.3,9), (20.3,8), (20.2,6), (20.1,7), (20.1,6), (19.9,9), (19.8,5), (19.8,5), (19.7,5), (19.5,6))

        for i, order in enumerate(orders):
            self.assertEqual(order.price, float(sorted_orders[i][0]))
            self.assertEqual(order.time, sorted_orders[i][1])

    def test_sort_buy_orders(self):
        orders = []
        for price, time in ((20.2, 2), (20.6, 3), (19.9, 8), (20.5, 6), (20.8, 10), (20.0, 7), (19.8, 8), (19.7, 2), (20.2, 3), (19.4, 10), (20.9, 9), (20.5, 5), (19.0, 10), (20.1, 4)):
            orders.append(BuyOrder(1, price, time, 0, 0, self.agents))
        orders.sort()

        sorted_orders = ((19.0, 10), (19.4, 10), (19.7, 2), (19.8, 8), (19.9, 8), (20.0, 7), (20.1, 4), (20.2, 3), (20.2, 2), (20.5, 6), (20.5, 5), (20.6, 3), (20.8, 10), (20.9, 9))
        for i, order in enumerate(orders):
            self.assertEqual(order.price, sorted_orders[i][0])
            self.assertEqual(order.time, sorted_orders[i][1])

    def test_order_matching(self):    
        orders = (('Buy', 17, 20.2, 2, 0),
        ('Buy', 11, 19.7, 2, 1),
        ('Buy', 13, 20.6, 3, 2),
        ('Buy', 18, 20.2, 3, 3),
        ('Buy', 13, 20.1, 4, 4),
        ('Sell', 18, 19.7, 5, 5),
        ('Sell', 10, 19.8, 5, 6),
        ('Sell', 11, 20.7, 5, 7),
        ('Sell', 14, 19.8, 5, 8),
        ('Buy', 15, 20.5, 5, 9),
        ('Sell', 20, 20.1, 6, 10),
        ('Sell', 19, 19.5, 6, 11))

        for order_type, quantity, price, time, agent_id in orders:
            if order_type == 'Buy':
                order = BuyOrder(quantity, price, time, 0, agent_id, self.agents)
                self.exchange.receive_buy_order(order)
            if order_type == 'Sell':
                order = SellOrder(quantity, price, time, 0, agent_id, self.agents)
                self.exchange.receive_sell_order(order)

        self.assertEqual(len(self.exchange.buy_orders), 1)
        self.assertEqual(len(self.exchange.sell_orders), 1)

        self.assertEqual(self.exchange.buy_orders[0].price, 19.7)
        self.assertEqual(self.exchange.sell_orders[0].price, 20.7)

        self.assertEqual(self.exchange.buy_orders[0].quantity, 6)
        self.assertEqual(self.exchange.sell_orders[0].quantity, 11)

        # Check cancelling works
        self.exchange.cancel_all_agent_buy_orders(1)
        self.exchange.cancel_all_agent_sell_orders(7)

        self.assertEqual(len(self.exchange.buy_orders), 0)
        self.assertEqual(len(self.exchange.sell_orders), 0)

        print('\n'.join(self.simulation._log))


if __name__ == '__main__':
    unittest.main() 