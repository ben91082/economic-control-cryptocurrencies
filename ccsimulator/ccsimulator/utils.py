import json
from .simulator import Event
from .exchange import BuyOrder, SellOrder

class EventEncoder(json.JSONEncoder):
    """
    Our Event queue has custom objects in it which are not JSON encodable
    We need to write an encoder ourselves to handle these objects
    """
    def default(self, obj):
        if isinstance(obj, (BuyOrder, SellOrder)):
            return {
                '_type': obj.__class__.__name__,
                'quantity': obj.quantity,
                'price': obj.price,
                'time': obj.time,
                'expires': obj.expires,
                'agent_id': obj.agent_id,
            }
        if isinstance(obj, Event):
            return {
                '_type': 'Event',
                'callback_method': obj.callback.__name__,
                'callback_class_name': type(obj.callback.__self__).__name__,
                'time': obj.time,
                'args': obj.args
            }
        return json.JSONEncoder.default(self, obj)

def event_decoder(instances):
    """
    Hook to decode our event queue correctly into Event objects
    instances should be a dict containing the class instances required to recreate the callbacks
    The closure is necessary in order to make the instances dictionary available.
    """
    def inner(obj):
        if '_type' in obj:
            if obj['_type'] == 'Event':
                callback_class_instance = instances[obj['callback_class_name']]
                callback = getattr(callback_class_instance, obj['callback_method'])
                return Event(obj['time'], callback, obj['args'])
            if obj['_type'] == 'BuyOrder':
                return BuyOrder(obj['quantity'], obj['price'], obj['time'], obj['expires'], obj['agent_id'], instances['Agents'])
            if obj['_type'] == 'SellOrder':
                return SellOrder(obj['quantity'], obj['price'], obj['time'], obj['expires'], obj['agent_id'], instances['Agents'])

        return obj
    return inner

__all__ = ['EventEncoder', 'event_decoder']