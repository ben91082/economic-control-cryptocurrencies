import argparse
import json
from ccsimulator.agents import Agents
from ccsimulator.exchange import Exchange
from ccsimulator.simulator import Day, Simulation
from ccsimulator.utils import event_decoder, EventEncoder
from ccsimulator.constants import (
    PROB_LEAVE, PROB_JOIN, DAY, NUMBER_OF_AGENTS, DAYS_IN_NEXT_STEP,
    TOTAL_NUMBER_DAYS, AVERAGE_WAIT_FOR_REVIEW, PROBABILITY_COLLAPSE, BUZZ,
    STARTING_CRYPTO_PRICE, AVG_CC_HOLDING, AVG_FIAT_HOLDING
)
from ccsimulator.plot import plot

parser = argparse.ArgumentParser(prog='python run_trial.py -f state_file')
parser.add_argument("-f", "--file", type=str, help="File to write output data", required=True)
parser.add_argument("--agents_n", type=int, default=NUMBER_OF_AGENTS, help="Number of agents")
parser.add_argument("--days_n", type=int, default=TOTAL_NUMBER_DAYS, help="Number of days to simulate")
parser.add_argument("--run_n", type=int, default=DAYS_IN_NEXT_STEP, help="Number of days to run in this step")
parser.add_argument("--average_wait_for_review", type=int, default=AVERAGE_WAIT_FOR_REVIEW, help="Average time agents wait before reviewing their holdings")
parser.add_argument("--starting_crypto_price", type=int, default=STARTING_CRYPTO_PRICE, help="Starting exchange rate")
parser.add_argument("--avg_cc_holding", type=int, default=AVG_CC_HOLDING, help="Average start holding of crypto")
parser.add_argument("--avg_fiat_holding", type=int, default=AVG_FIAT_HOLDING, help="Average start holding of fiat")
parser.add_argument("--buzz", type=float, default=BUZZ, help="Buzz level")

options = parser.parse_args()

simulation = Simulation()
day = Day(simulation)
exchange = Exchange(simulation)
agents = Agents(simulation, exchange)

params = {
    "agents_n": options.agents_n,
    "days_n": options.days_n,
    "average_wait_for_review": options.average_wait_for_review,
    "avg_fiat_holding": options.avg_fiat_holding,
    "starting_crypto_price": options.starting_crypto_price,
    "avg_cc_holding": options.avg_cc_holding,
    "buzz": options.buzz
}

with open(options.file, 'r') as fh:
    # Read state from file for now, but this can easily be switched for another storage method e.g. database
    try:
        state = json.load(fh, object_hook=event_decoder({'Day': day, 'Agents': agents}))
    except:
        state = {
            "params": params,
        }

simulation.load_state(state)
if simulation.trial_ended():
    simulation.save_trial()
    simulation.reset_current_trial()

# We need to create all the initial events before the first step
if simulation.trial_not_started():
    day.schedule()
    for i in range(NUMBER_OF_AGENTS):
        agents.add()
        agents.schedule(i)

simulation.record_step(params, options.run_n)
simulation.run_step()

with open(options.file, 'w') as fh:
    json.dump(simulation.export_state(), fh, cls=EventEncoder, indent=4)

plot(options.days_n, simulation.stats, simulation.trials, 'participation')
plot(options.days_n, simulation.stats, simulation.trials, 'current_price')
