jQuery(document).ready(function($) {
    $(".expandable h5").click(onClickExpandable)
    function onClickExpandable () {
        var panel = $(this).parent()
        var open = panel.is('.open');
        var icon = panel.find(".feather");
        if (open) {
            panel.addClass('closed').removeClass('open')
        } else {
            panel.addClass('open').removeClass('closed')
        }
        var new_icon = $(feather.icons[open ? 'chevron-right' : 'chevron-down'].toSvg())
        icon.replaceWith(new_icon)
    }
})
