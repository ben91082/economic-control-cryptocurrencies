var CeleryProgressBar = (function () {
    function onSuccessDefault(progressBarCompleteEl, progressBarPendingEl, progressBarMessageEl) {
        $(progressBarCompleteEl).addClass('bg-success').removeClass('bg-info').removeClass('bg-danger');
        progressBarMessageEl.html('')
    }

    function onErrorDefault(progressBarCompleteEl, progressBarPendingEl, progressBarMessageEl) {
        $(progressBarCompleteEl).addClass('bg-danger').removeClass('bg-success').removeClass('bg-info');
        $(progressBarPendingEl).width(0);
        progressBarMessageEl.html('Uh oh, something went wrong!')
    }

    function onProgressDefault(progressBarCompleteEl, progressBarPendingEl, progress) {
        if (progress.current && progress.total && progress.pending) {
            $(progressBarCompleteEl).addClass('bg-info').removeClass('bg-success').removeClass('bg-danger');
            $(progressBarCompleteEl).width(((progress.current / progress.total) * 100) + "%");
            $(progressBarPendingEl).width((((progress.pending - progress.current) / progress.total) * 100) + "%");
        }
    }

    function updateProgress (progressUrl, options) {
        options = options || {};
        var progressBarCompleteEl = options.progressBarCompleteEl;
        if (!progressBarCompleteEl) throw "You must define progressBarCompleteEl in the options";

        var progressBarPendingEl = options.progressBarPendingEl;
        if (!progressBarPendingEl) throw "You must define progressBarPendingEl in the options";

        var progressBarMessageEl = options.progressBarMessageEl;
        if (!progressBarMessageEl) throw "You must define progressBarMessageEl in the options";

        var onProgressUser = options.onProgress || function() {};
        var onSuccessUser = options.onSuccess || function() {};
        var onErrorUser = options.onError || function() {};
        var pollInterval = options.pollInterval || 500;

        function progressCallback(response) {
            var html = '';
            if (response.state == 'PENDING') {
                $(progressBarCompleteEl).addClass('bg-info').removeClass('bg-success').removeClass('bg-danger');
                if (ajax_loader_src) {
                    html += '<img src="'+ ajax_loader_src +'" style="width: 16px; height: 16px" /> &nbsp;';
                }
                html += 'Our minions are busy with other simulations right now, check back soon!';
                progressBarMessageEl.html(html)
            } else {
                progressBarMessageEl.html('');
            }
            if (response.progress) {
                onProgressDefault(progressBarCompleteEl, progressBarPendingEl, response.progress);
                onProgressUser(response)
            }
            if (!response.complete) {
                setTimeout(updateProgress, pollInterval, progressUrl, options);
            } else {
                if (response.success) {
                    onSuccessDefault(progressBarCompleteEl, progressBarPendingEl, progressBarMessageEl);
                    onSuccessUser(response);
                } else {
                    onErrorDefault(progressBarCompleteEl, progressBarPendingEl, progressBarMessageEl);
                    onErrorUser(response);
                }
            }
        }

        $.ajax({
            url: progressUrl, 
            success: progressCallback,
            error: function(response) {
                onErrorDefault(progressBarCompleteEl, progressBarPendingEl, progressBarMessageEl);
                onErrorUser(response);
            },
            dataType : 'json'
        });
    }

    return {
        onSuccessDefault: onSuccessDefault,
        onErrorDefault: onErrorDefault,
        onProgressDefault: onProgressDefault,
        updateProgress: updateProgress,
        initProgressBar: updateProgress,  // just for api cleanliness
    };
})();

