jQuery(document).ready(function($) {

    // Always serialise the server side data present in the form on page load so we can easily detect changes
    var server_side_data = form_data_to_string();
    var store_unsaved = !ECS.trial_complete;

    restore_unsaved_parameters();
    window.addEventListener("beforeunload", store_unsaved_parameters);

    var reset_modal = $('#resetModal');
    var confirm_reset_btn = reset_modal.find('#confirm_reset');

    $("#do_reset").click(onClickReset);
    $("a.sidebar-heading").click(onClickTabHeading);
    $("[data-save-next]").click(saveThenRedirect);
    $("#loadSimModal .nav-link").click(onClickLoadLink);
    $(".warn-unsaved").click(unsaved_data_warning);

    $("#form_parameters").submit(no_store_unsaved);
    $("#form_delete").submit(no_store_unsaved);
    $(".no-store-unsaved").click(no_store_unsaved);

    function no_store_unsaved() {
        store_unsaved = false;
    }

    $(window).resize(function() {
        if (ECS.trialData) {
            allPlots(ECS.trialData)
        }
    });
    
    if (ECS.trialData) {
        allPlots(ECS.trialData)
    }

    field_error_helper();

    function field_error_helper() {
        var el_name, el, tab_container, tab_heading;
        if (ECS.field_errors && ECS.field_errors.length > 0) {
            for (var i = ECS.field_errors.length - 1; i >= 0; i--) {
                el_name = ECS.field_errors[i];
                el = $("input[name='"+ el_name + "']");
                tab_container = el.closest('.tab-pane');
                tab_heading = $("[href='#" + tab_container.attr('id') + "']")
                tab_heading.addClass('alert-danger')
            }
            tab_heading.tab('show')
        }
    }

    function allPlots(data) {
        $("main svg:visible").not('.feather').each(function() {
            var svg = $(this);
            var plot_type = svg.attr('data-plot-type');
            var width = svg.parent().width();
            svg.width(width).attr('width', width)
            if (plot_type == 'bar') {
                plotBar(data, this);
            } else {
                plotLine(data, this);
            }
        })
    }    

    function onClickReset(e, data) {
        if (!data || !data.confirmed) {
            e.preventDefault()
            reset_modal.modal('show');
            confirm_reset_btn.on('click', function () {
                $(e.target).trigger('click', {confirmed: true});
                reset_modal.modal('hide');
            })
        }
    }

    reset_modal.on('hidden.bs.modal', function (e) {
        confirm_reset_btn.off('click');
    })

    function onClickLoadLink () {
        var link = $(this);
        var href = link.attr('data-href');
        if (!href) {
            return false;
        }
        var all_links = link.closest('.nav').find('.nav-link');
        all_links.removeClass('active');
        link.addClass('active');
        var modal = link.closest('.modal-content');
        modal.find('[data-save-next]').attr('data-save-next', href);
        modal.find('.modal-footer a').attr('href', href);
    }

    function saveThenRedirect() {
        var url = $(this).attr('data-save-next');
        $("input[name='next']").val(url);
        $("#form_parameters").submit();
    }

    function onClickTabHeading() {
        var a = $(this);
        var tab_heading = $("#tab-heading")
        var page_title = a.attr('data-title');
        if (!page_title) {
            page_title = a.text();
        }
        tab_heading.text(page_title); 
    }

    function plotBar(data1, svg) {
        var y_value = svg.getAttribute('data-plot');

        var data = [], f, day;
        for (var i = 0; i < data1.days_n; i++) {
            if (i < data1.plot.length) {
                f = data1.plot[i][y_value]
            } else {
                f = 0;
            }
            data.push({
                "day":  i + 1,
                "frequency": f
            });
        }

        var svg = d3.select(svg),
        margin = {top: 20, right: 20, bottom: 30, left: 50},
        width = +svg.attr("width") - margin.left - margin.right,
        height = +svg.attr("height") - margin.top - margin.bottom;
    
        svg.select('g').remove();

        var x = d3.scaleBand().range([0, width]).padding(0.1),
            y = d3.scaleLinear().rangeRound([height, 0]);
        
        var g = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        x.domain(data.map(function(d) { return d.day; }));
        y.domain([0, d3.max(data, function(d) { return d.frequency; })]);
        
        g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x).tickValues(data.map(function (d) {return d.day}).filter(function(d) {return !(d % 50) })));
        
        g.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(y).ticks(10))
        
        g.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function(d) { return x(d.day); })
            .attr("y", function(d) { return y(d.frequency); })
            .attr("width", x.bandwidth())
            .attr("height", function(d) { return height - y(d.frequency); });
    }

    function plotLine (data, svg) {
        var y_value = svg.getAttribute('data-plot');
        var svg = d3.select(svg);
        svg.select('g').remove()

        var margin = {top: 20, right: 20, bottom: 30, left: 50},
            width = + svg.attr("width") - margin.left - margin.right,
            height = +svg.attr("height") - margin.top - margin.bottom,
            g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.scaleLinear()
            .rangeRound([0, width]);
    
        var y = d3.scaleLinear()
            .rangeRound([height, 0]);
    
        var line = d3.line()
            .x(function(d) { return x(d.day); })
            .y(function(d) { return y(d[y_value]); });
        
        x.domain([0, data.days_n]);

        var y_max_attr = svg.attr('data-y-max');
        var y_max;
        if (y_max_attr) {
            y_max = data[y_max_attr];
        } else {
            y_max = d3.max(data.plot, function(d) {return d[y_value]});
        }
        y.domain([0, y_max]);

        g.append("g")
            .attr("transform", "translate(0," + height + ")")
            .attr("class", "x")
            .call(d3.axisBottom(x))
    
        g.append("g")
            .attr("class", "y")
            .call(d3.axisLeft(y))
    
        g.append("path")
            .datum(data.plot)
            .attr("fill", "none")
            .attr("stroke", "steelblue")
            .attr("stroke-linejoin", "round")
            .attr("stroke-linecap", "round")
            .attr("stroke-width", 1.5)
            .attr("d", line);
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function() {
        if (ECS.trialData) {
            allPlots(ECS.trialData)
        }
    });

    $("[data-progress-url]").each(function() {
        var row = $(this);
        var progress_url = row.attr("data-progress-url")
        if (progress_url) {
            var container = row.find('.progress');
            var progressBarCompleteEl = container.find('.progress-bar.completed');
            var progressBarPendingEl = container.find('.progress-bar.pending');
            var progressBarMessageEl = container.siblings('.progress-bar-message')
            CeleryProgressBar.initProgressBar(progress_url, {
                progressBarCompleteEl: progressBarCompleteEl,
                progressBarPendingEl: progressBarPendingEl,
                progressBarMessageEl: progressBarMessageEl,
                pollInterval: 500,
                onProgress: function(response) {
                    ECS.trialData = response.progress.data;
                    allPlots(ECS.trialData)
                },
                onSuccess: function(response) {
                    store_unsaved = false;
                }
            })
        }
    })

    function unsaved_data_warning() {
        if (ECS.trial_complete) {
            return true;
        }
        var data = form_data_to_string();
        if (data != server_side_data) {
            var r = confirm(ECS.warn_unsaved_changes_msg);
            if (r !== true) {
                return false
            }
        }

        // They've seen the warning and ignored it so don't keep their changes
        store_unsaved = false;
        return true;
    }

    function form_data_to_string() {
        var no_serialize = ['csrfmiddlewaretoken', 'next']
        var data = $("#form_parameters").serializeArray();  
        data = data.filter(function(item) {
            if (no_serialize.indexOf(item.name) > -1) {
                return false
            }
            return true;
        })
        return JSON.stringify(data);
    }

    function store_unsaved_parameters(e) {
        if (!store_unsaved) {
            return;
        }
        var data = form_data_to_string();
        if (data == server_side_data) {
            return;
        }
        var key = 'unsaved_parameters__' + window.location.pathname;
        localStorage.setItem(key, data);
    }

    function restore_unsaved_parameters() {
        var key = 'unsaved_parameters__' + window.location.pathname;
        var data = window.localStorage.getItem(key);
        var input, old_val;
        if (data && data != server_side_data)  {
            var msg = ECS.restore_unsaved_changes_msg;
            var r = confirm(msg);
            if (r === true) {
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {
                    input = $("#form_parameters [name='"+ data[i].name +"']");
                    old_val = input.val();
                    if (old_val != data[i].value) {
                        $("#form_parameters [name='"+ data[i].name +"']").val(data[i].value);
                    }
                }
            } 
        }
        window.localStorage.removeItem(key);
    }
})
