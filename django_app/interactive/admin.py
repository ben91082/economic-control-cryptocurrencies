from django.contrib import admin

from .models import Trial, Preset, Simulation

admin.site.register(Trial)
admin.site.register(Preset)
admin.site.register(Simulation)
