from django import template

register = template.Library()

@register.inclusion_tag('interactive/progress_bar.html')
def progress_bar(p):
    complete, pending, total = p
    context = {}
    context['progress_1_width'] = '%.6f%%' % ((complete * 100) / total)
    context['progress_2_width'] = '%.6f%%' % (((pending - complete) * 100) / total)
    return context