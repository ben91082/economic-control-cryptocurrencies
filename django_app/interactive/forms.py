from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from interactive.models import Trial, TASK_NEW_TRIAL, TASK_SAVE_TRIAL, TASK_SAVE_SIMULATION, TASK_START_TRIAL, TASK_CONTINUE_TRIAL, TASK_RESET_TRIAL
from interactive.models import Preset, Simulation as SimulationModel
from interactive import tasks
from ccsimulator import Simulation as Simulation

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Please provide a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

class RangeInput(forms.NumberInput):
    input_type = 'range'


class BaseSimulationForm(forms.ModelForm):
    # form fields which don't need to be saved with other parameters
    _no_save = ['description', 'name', 'task']
    description = forms.CharField(label="Short Description", widget=forms.Textarea(attrs={'rows': 2}), required=False)
    notes = forms.CharField(widget=forms.Textarea(attrs={'rows': 16}), required=False)

    agents_n = forms.IntegerField(label="Agent Population Size", min_value=1, max_value=200)
    days_n = forms.IntegerField(label="Number of days", min_value=1, max_value=730)
    average_wait_for_review = forms.IntegerField(label="Average wait time before an agent reviews their position", min_value=1, max_value=30, widget=forms.NumberInput(attrs={'step': "1"}))
    avg_fiat_holding = forms.IntegerField(label="Average starting holding of fiat currency", min_value=10, max_value=1000000, widget=forms.NumberInput(attrs={'step': "10"}))
    avg_cc_holding = forms.IntegerField(label="Average starting holding of cryptocurrency", min_value=10, max_value=1000000, widget=forms.NumberInput(attrs={'step': "10"}))
    starting_crypto_price = forms.FloatField(label="Starting exchange rate", min_value=0)
    buzz = forms.FloatField(label="Promotional Effort", min_value=0, max_value=1, required=False, widget=RangeInput())

    run_n = forms.IntegerField(label="Number of days to run in each step of trial", min_value=1, max_value=730 )

    # Pointless placeholder fields
    env1 = forms.FloatField(label="Environment Param 1", min_value=0, max_value=1, required=False, widget=RangeInput())
    env2 = forms.FloatField(label="Environment Param 2", min_value=0, max_value=1, required=False)
    env3 = forms.ChoiceField(label="Environment Param 3", choices=[(i, 'Option %s' % i) for i in range(1,6)], required=False)
    exc1 = forms.FloatField(label="Exchange(s) Param 1", min_value=0, max_value=1, required=False, widget=RangeInput())
    exc2 = forms.FloatField(label="Exchange(s) Param 2", min_value=0, max_value=1, required=False)
    exc3 = forms.ChoiceField(label="Exchange(s) Param 3", choices=[(i, 'Option %s' % i) for i in range(1,6)], required=False)
    task = forms.CharField(widget=forms.HiddenInput, required=False)

class UpdateSimulationForm(BaseSimulationForm):

    def save(self, commit=True, user=None):
        simulation_obj = super().save(False)
        if not simulation_obj.data:
            simulation_obj.data = {"params": {}}
        if user:
            simulation_obj.user = user
        params = dict((k,v) for k,v in self.cleaned_data.items() if k not in self._no_save)
        simulation_obj.data['params'].update(params)
        if commit:
            simulation_obj.save()
            if self.cleaned_data['task'] == TASK_NEW_TRIAL:
                simulation_obj.start_trial() 
        return simulation_obj        

    class Meta:
        model = SimulationModel
        fields = ('name', 'id', 'description', 'notes')

class StartSimulationForm(forms.ModelForm):

    description = forms.CharField(label="Short Description", widget=forms.Textarea(attrs={'rows': 2}), required=False)
    preset = forms.ModelChoiceField(queryset=Preset.objects.all(), required=False, empty_label="Blank", help_text="Populate your simulations' parameters with default values.")

    def save(self, commit=True, user=None):
        simulation_obj = super().save(False)
        if 'preset' in self.cleaned_data and self.cleaned_data['preset']:
            simulation_obj.data = {"params": self.cleaned_data['preset'].data}
        else:
            simulation_obj.data = {"params": {}}
        if user:
            simulation_obj.user = user
        if commit:
            simulation_obj.save()
        return simulation_obj        

    class Meta:
        model = SimulationModel
        fields = ('name', 'id', 'description')


class StartTrialForm(BaseSimulationForm):

    def save(self, commit=True):
        trial = super().save(False)
        simulation = Simulation()
        simulation.load_state(trial.data)
        params = dict((k,v) for k,v in self.cleaned_data.items() if k not in self._no_save)
        task = self.cleaned_data.get('task')
        if task == TASK_RESET_TRIAL:
            simulation.reset_current_trial()
        else:
            simulation.record_step(params, self.cleaned_data['run_n'])
            if task in (TASK_START_TRIAL, TASK_CONTINUE_TRIAL):
                simulation.set_run_time()

        trial.data = simulation.export_state()
        if commit:
            trial.save()
            if task in (TASK_START_TRIAL, TASK_CONTINUE_TRIAL):
                result = tasks.trial.delay(trial.pk)
                trial.current_task_id = result.task_id
                trial.save()
        return trial

    def clean(self):
        cleaned_data = super().clean()
        run_n = cleaned_data.get("run_n")
        days_n = cleaned_data.get("days_n")

        if run_n and days_n and (run_n > days_n):
            raise forms.ValidationError( "Number of days to run in each step must be less than or equal to the total number of days in the simulation")
        
        if self.instance.current_task_id and self.cleaned_data.get('task') in (TASK_START_TRIAL, TASK_CONTINUE_TRIAL, TASK_RESET_TRIAL):
            raise forms.ValidationError( "Not possible while the trial is running!" )

    class Meta:
        model = Trial
        fields = ('name', 'id', 'description', 'notes')

