# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from django.conf import settings
import time
import json
import os
from .models import Trial

from ccsimulator.simulator import Day, Simulation
from ccsimulator.exchange import Exchange
from ccsimulator.agents import Agents
from ccsimulator.utils import event_decoder, EventEncoder
from ccsimulator.constants import (
    PROB_LEAVE, PROB_JOIN, DAY, NUMBER_OF_AGENTS, DAYS_IN_NEXT_STEP,
    TOTAL_NUMBER_DAYS, AVERAGE_WAIT_FOR_REVIEW, PROBABILITY_COLLAPSE
)

@shared_task(bind=True)
def trial(self, trial_id):

    trial = Trial.objects.get(pk=trial_id)
    simulation = Simulation()
    day = Day(simulation)
    exchange = Exchange(simulation)
    agents = Agents(simulation, exchange)

    decoder = event_decoder({'Day': day, 'Agents': agents})
    trial.decode_event_queue(decoder)
    simulation.load_state(trial.data)

    # We need to create all the initial events before the first step
    if simulation.trial_not_started():
        day.schedule()
        for i in range(NUMBER_OF_AGENTS):
            agents.add()
            agents.schedule(i)

    last_day_reached = 0
    for event in simulation.run():
        current, pending, total_days = simulation.check_step_progress()
        if current > last_day_reached:
            self.update_state(state='PROGRESS', meta={'current': current, 'pending': pending, 'total': total_days, 'data': simulation.export_plot_data()})
            last_day_reached = current

    return simulation.export_state()
