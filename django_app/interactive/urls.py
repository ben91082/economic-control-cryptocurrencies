from django.urls import path
from interactive import views as interactive_views

urlpatterns = [
    path('', interactive_views.StartSimulation.as_view(), name='logged_in_home'),
    path('simulation/<int:pk>', interactive_views.UpdateSimulation.as_view(), name='simulation-update'),
    path('simulation/<int:simulation>/trial/<int:pk>', interactive_views.TrialUpdate.as_view(), name='trial-update'),
    path('progress/<int:pk>/', interactive_views.progress, name='progress'),
    path('trial/<int:pk>/delete/', interactive_views.TrialDelete.as_view(), name='trial-delete'),
    path('simulation/<int:pk>/delete/', interactive_views.SimulationDelete.as_view(), name='simulation-delete')
]