import math

def int_dist(l,n,s):
    a = 10 * n/l
    na = [max(1, math.ceil(a/(math.pow(i,s)))) for i in range(1, l + 1)]
    t = sum(na)
    return [max(1, math.ceil(i * (n/t))) for i in na]