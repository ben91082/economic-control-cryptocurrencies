from datetime import datetime

from celery.result import AsyncResult
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from jsonfield import JSONField
from ccsimulator.utils import EventEncoder

from ccsimulator.simulator import Simulation as SimulationRunner

TASK_NEW_TRIAL = 'Save Defaults & Create New Trial'
TASK_SAVE_TRIAL = 'Save Trial Settings'
TASK_SAVE_SIMULATION = 'Save Simulation Defaults'
TASK_START_TRIAL = 'Start Trial'
TASK_CONTINUE_TRIAL = 'Continue Trial'
TASK_RESET_TRIAL = 'Restart Trial'

class Preset(models.Model):
    data = JSONField()
    name = models.CharField(max_length=200, default="")

    def __str__(self):
        return self.name

class Simulation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # TaskResult object is only available when a worker picks it up 
    # so we'll use UUIDField instead of ForeignKey
    data = JSONField(encoder_class=EventEncoder)
    name = models.CharField(max_length=100, default="", verbose_name="Name of your simulation")
    description = models.CharField(max_length=250, blank=True, default="", verbose_name="Short Description")
    notes = models.TextField(blank=True, default="")
    created = models.DateTimeField(auto_now=False, auto_now_add=True)

    def start_trial(self):
        runner = SimulationRunner()
        runner.load_state(self.data)
        existing_trials = self.trial_set.count()
        trial_name = 'Trial %s' %  (existing_trials + 1)
        trial = Trial(simulation=self, data=runner.export_state(), name=trial_name)
        trial.save()
        self.new_trial_id = trial.pk

    def get_absolute_url(self):
        return reverse('simulation-update', kwargs={'pk': self.id})

    def __str__(self):
        if self.name: 
            return self.name
        else:
            return 'Untitled Simulation'
    

class Trial(models.Model):
    simulation = models.ForeignKey(Simulation, on_delete=models.CASCADE)
    # TaskResult object is only available when a worker picks it up 
    # so we'll use UUIDField instead of ForeignKey
    current_task_id = models.UUIDField(null=True, blank=True)
    data = JSONField(encoder_class=EventEncoder)
    name = models.CharField(max_length=100, default="", verbose_name="Name of this trial")
    description = models.CharField(max_length=250, blank=True, default="", verbose_name="Short Description")
    notes = models.TextField(blank=True, default="")
    created = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        if self.name: 
            return self.name
        else:
            return 'Untitled Trial'

    def decode_event_queue(self, decoder):
        for i, event in enumerate(self.data['eventq']):
            self.data['eventq'][i] = decoder(event)
        for i, order in enumerate(self.data['buy_orders']):
            self.data['buy_orders'][i] = decoder(order)
        for i, order in enumerate(self.data['sell_orders']):
            self.data['sell_orders'][i] = decoder(order)
        

    def check_status(self):
        if self.current_task_id:
            result = AsyncResult(self.current_task_id)
            return result.state
        return None
    
    def get_absolute_url(self):
        return reverse('trial-update', kwargs={'pk': self.id, 'simulation': self.simulation.pk})

    def get_progress(self):
        simulation = SimulationRunner()
        simulation.load_state(self.data)
        return simulation.check_step_progress()

    def has_not_started(self):
        completed, pending, total = self.get_progress()
        return completed == 0 and pending == 0

    def is_complete(self):
        completed, pending, total = self.get_progress()
        if completed >= total or pending >= total:
            return True
        else:
            return False