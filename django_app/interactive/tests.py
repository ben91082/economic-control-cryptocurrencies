from django.test import TestCase
from interactive.helpers import int_dist
from interactive.models import Trial
from ccsimulator.utils import EventEncoder, event_decoder
from ccsimulator.simulator import Day, Simulation, Agents, Event
from django.contrib.auth.models import User
import json

class SimulatorTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.user.save()

    def test_int_dist(self):
        self.assertEqual(int_dist(10, 200, 1.1), [75,35,23,17,13,11,9,8,7,6])


    def test_encode_decode(self):
        simulation = Simulation()
        day = Day(simulation)
        agents = Agents(simulation)
        decoder = event_decoder({'Day': day, 'Agents': agents})

        with open('C:/tmp/state.json', 'r') as fh:
            state = json.load(fh, object_hook=decoder)
        t1 = Trial(user=self.user, data=state)
        t1.save()

        t2 = Trial.objects.first()
        t2.decode_event_queue(decoder)

        self.assertIsInstance(t2.data['eventq'][0], Event)



