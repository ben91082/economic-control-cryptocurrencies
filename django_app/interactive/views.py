import json
import os
from time import sleep

import requests
from celery.result import AsyncResult
from ccsimulator import Simulation
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views.generic.edit import CreateView, FormView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.csrf import requires_csrf_token
from django.shortcuts import get_object_or_404
from django.http import HttpResponseServerError

from django.http import HttpResponseNotFound
from django.template import Context, Engine, TemplateDoesNotExist, loader

from interactive import tasks
from interactive.forms import SignUpForm

from .forms import StartTrialForm, UpdateSimulationForm, StartSimulationForm
from .models import Trial, Preset, TASK_NEW_TRIAL, TASK_SAVE_TRIAL, TASK_SAVE_SIMULATION, TASK_START_TRIAL, TASK_CONTINUE_TRIAL, TASK_RESET_TRIAL
from .models import Simulation as SimulationModel

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('logged_in_home')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

@login_required
def progress(request, pk):
    response_data = {
        'complete': False,
        'success': None,
        'progress': None,
        'state' : 'SUCCESS'
    }
    simulation = Simulation()

    trial = get_object_or_404(Trial, pk=pk)
    if not trial.current_task_id:
        simulation.load_state(trial.data)
        response_data['progress'] = {'current': simulation.day, 'pending': 0, 'total': simulation.param('days_n'), 'data': simulation.export_plot_data()}
        response_data['complete'] = True
        response_data['success'] = True
        return HttpResponse(json.dumps(response_data), content_type='application/json')

    try:
        result = AsyncResult(trial.current_task_id)
    except:
        return HttpResponseServerError(json.dumps('Something went wrong'), content_type='application/json')

    response_data['state'] = result.state
    if result.ready():
        response_data['complete'] = True
        if result.successful():
            simulation.load_state(result.get())
            response_data['success'] = True
            response_data['progress'] = {'current': simulation.day, 'pending': simulation.day, 'total': simulation.param('days_n'), 'data': simulation.export_plot_data()}
        else:
            simulation.load_state(trial.data)
            simulation.remove_last_step()
            response_data['success'] = False
            response_data['progress'] = {'current': simulation.day, 'pending': 0, 'total': simulation.param('days_n'), 'data': simulation.export_plot_data()}
        result.forget()
        trial.current_task_id = None
        trial.data = simulation.export_state()
        trial.save()
    else:
        response_data['progress'] = result.info

    return HttpResponse(json.dumps(response_data), content_type='application/json')

class StartSimulation(LoginRequiredMixin, CreateView):
    template_name = 'interactive/logged_in_home.html'
    form_class = StartSimulationForm

    def get_context_data(self, **context):
        context['presets_list'] = Preset.objects.all()
        context['simulations_list'] = self.request.user.simulation_set.all()
        context['start_simulation_form'] = self.get_form()
        return context

    def form_valid(self, form):
        self.object = form.save(user=self.request.user)
        return super().form_valid(form)

class UpdateSimulation(LoginRequiredMixin, UpdateView):
    template_name = 'interactive/simulation_dashboard.html'
    form_class = UpdateSimulationForm
    model = SimulationModel

    def get_queryset(self):
        return self.request.user.simulation_set.all()

    def get_success_url(self):
        next = self.request.POST.get('next')
        task = self.request.POST.get('task')
        if next: 
            return next
        elif task == TASK_NEW_TRIAL:
            try:
                return reverse('trial-update', kwargs={'simulation': self.object.pk, 'pk' : self.object.new_trial_id})
            except AttributeError as e:
                pass

        return super().get_success_url()

    def form_valid(self, form):
        self.object = form.save(user=self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **context):
        trial = self.object
        context['trials'] = self.object.trial_set.all()
        context['trial'] = None
        context['simulations_list'] = self.request.user.simulation_set.all()
        context['presets_list'] = Preset.objects.all()
        context['trial_json'] = json.dumps(trial.data)
        context['start_simulation_form'] = StartSimulationForm()
        context['restore_unsaved_changes_msg'] = json.dumps('''Last time you were here you left some changes to your default simulation settings unsaved. Would you like to restore these?
        
To restore your changes click "OK" and to keep them permanently you should then click "%s"''' % TASK_SAVE_SIMULATION)
        context['warn_unsaved_changes_msg'] = json.dumps('''You have unsaved changes to your default simulation settings, are you sure you want to leave this page without saving them first?''')


        context['buttons'] = {
            'start_new_trial': TASK_NEW_TRIAL,
            'save': TASK_SAVE_SIMULATION
        }
        return super().get_context_data(**context)

    def get_initial(self):
        initial = self.initial.copy()
        if 'params' in self.object.data:
            initial.update(self.object.data['params'])
        return initial

class TrialDelete(LoginRequiredMixin, DeleteView):
    model = Trial

    def get_success_url(self):
        return reverse('simulation-update', kwargs={'pk' : self.object.simulation.id})

    def get_queryset(self):
        simulations = self.request.user.simulation_set.all()
        return Trial.objects.filter(simulation__in=simulations)

class SimulationDelete(LoginRequiredMixin, DeleteView):
    model = SimulationModel
    success_url = reverse_lazy('logged_in_home')

    def get_queryset(self):
        return self.request.user.simulation_set.all()

class TrialReset(LoginRequiredMixin, UpdateView):
    form_class = StartTrialForm
    template_name = 'interactive/trial_dashboard.html'
    model = Trial

class TrialUpdate(LoginRequiredMixin, UpdateView):
    form_class = StartTrialForm
    template_name = 'interactive/trial_dashboard.html'
    model = Trial

    def get_queryset(self):
        simulations = self.request.user.simulation_set.get(pk=self.kwargs['simulation'])
        return Trial.objects.filter(simulation=simulations)

    def get_success_url(self):
        next = self.request.POST.get('next')
        if next: 
            return next
        else:
            return super().get_success_url()

    def get_context_data(self, **context):
        trial = self.object
        simulation = Simulation()
        simulation.load_state(trial.data)
        context['simulations_list'] = self.request.user.simulation_set.all()
        context['presets_list'] = Preset.objects.all()
        context['trial_json'] = json.dumps(simulation.export_plot_data())
        context['start_simulation_form'] = StartSimulationForm()
        context['restore_unsaved_changes_msg'] = json.dumps('''Last time you were here you left some changes to your current trial settings unsaved. Would you like to restore these?
        
To restore your changes click "OK" and to keep them permanently you should then click "%s"''' % TASK_SAVE_TRIAL)
        context['warn_unsaved_changes_msg'] = json.dumps('''You have unsaved changes to your current trial settings, are you sure you want to leave this page without saving them first?''')

        context['buttons'] = {
            'save': TASK_SAVE_TRIAL,
            'reset': TASK_RESET_TRIAL
        }

        if simulation.trial_not_started() and not simulation.has_steps_pending():
            context['buttons']['run_trial'] = TASK_START_TRIAL
        else:
            context['buttons']['run_trial'] = TASK_CONTINUE_TRIAL

        return super().get_context_data(**context)

    def get_initial(self):
        initial = self.initial.copy()
        if 'steps' in self.object.data and len(self.object.data['steps']) > 0:
            initial.update(self.object.data['steps'][-1]['params'])
        else:
            initial.update(self.object.data['params'])
        return initial

@requires_csrf_token
def page_not_found(request, exception):
    exception_repr = exception.__class__.__name__
    try:
        message = exception.args[0]
    except (AttributeError, IndexError):
        pass
    else:
        if isinstance(message, str):
            exception_repr = message
    context = {
        'request_path': request.path,
        'exception': exception_repr,
    }

    if request.user.is_authenticated:
        context['presets_list'] = Preset.objects.all()
        context['simulations_list'] = request.user.simulation_set.all()
        context['start_simulation_form'] = StartSimulationForm()
        template = loader.get_template('logged_in_404.html')
    else:
        template = loader.get_template('logged_out_404.html')
    body = template.render(context, request)
    return HttpResponseNotFound(body, content_type=None)