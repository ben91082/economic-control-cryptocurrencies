from __future__ import absolute_import, unicode_literals

from .base import *

DEBUG = False
ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '54.213.179.162', 'ec2-54-213-179-162.us-west-2.compute.amazonaws.com', 'ecc.longfinance.net']

try:
    from .local import *
except ImportError:
    pass