# Economic Simulation of Cryptocurrencies

## Project Layout

R - Matthew's original R simulations for reference
django_app - Django web app for the interactive simulator.

## Python/Django Setup instuctions (Windows)

### Requirements 

* **Python >= 3.6** https://www.python.org/ftp/python/3.6.5/python-3.6.5.exe

Create and activate a new virtual environment
```
py -m venv C:\path\to\virtual\environment
C:\path\to\virtual\environment\Scripts\activate
```

Now install required python modules into the virtual environment which are listed in the requirements.txt file
```
pip install -r requirements.txt
```

Now cd into simulator directory and run intial database migrations to create the test database.
```
cd django_app
python manage.py migrate
```

## Docker 

Build an image and run container based off of it:
```
docker build -t ben91082/economiccryptocurrencysimulation_web:latest .
docker run -p 8888:80 ben91082/economiccryptocurrencysimulation_web:latest

# To push/pull to docker hub
docker push ben91082/economiccryptocurrencysimulation_web:latest
docker pull ben91082/economiccryptocurrencysimulation_web:latest

# Or run an interactive terminal
docker exec -i -t 750f16c007ad  /bin/bash
docker exec  -t 750f16c007ad  cat /home/docker/code/django_app/ecs/settings/local.py
```

Run a redis container for celery broker
```
docker run -p 6379:6379 redis
```

## Celery

To run workers 

```
celery -A ecs worker --pool=solo -l info
```

--pool=solo is needed for windows which has limited support. Will omit in production

## R Setup

```
install.packages('ineq')
install.packages('extrafont')
install.packages('plumber')
```

We can run a server written in R which exposes the "run_trial" method as a simple web service returning trial data in JSON format. For now the Django app depends on this.

```
Rscript server.R
```


## ECS Deployment Stuff

```
ecs-cli configure profile --profile-name economiccryptocurrencysimulation --access-key xxx --secret-key xxxxxx
ecs-cli configure --cluster economiccryptocurrencysimulation --default-launch-type EC2 --region us-west-2 --config-name economiccryptocurrencysimulation
ecs-cli up --keypair ben91082 --capability-iam --size 2 --instance-type t2.micro
ecs-cli compose --file ecs-compose.yml up
```