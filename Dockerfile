FROM centos:7

RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm && \
	yum update -y && \
	yum install -y \
	gcc \
	git \
	python36u \
	python36u-pip \
	python36u-devel \
	nginx \
	supervisor \
	sqlite3

RUN pip3.6 install -U pip setuptools && pip3.6 install uwsgi

# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx
COPY supervisor-app.ini /etc/supervisord.d

# COPY requirements.txt and RUN pip install BEFORE adding the rest of your code, this will cause Docker's caching mechanism
# to prevent re-installing (all your) dependencies when you made a change a line or two in your app.

COPY django_app/requirements.txt /home/docker/code/django_app/
RUN pip3.6 install -r /home/docker/code/django_app/requirements.txt

# add (the rest of) our code
COPY . /home/docker/code/
RUN pip3.6 install -e /home/docker/code/ccsimulator
RUN echo "CELERY_BROKER_URL = 'redis://redis:6379/0'" > /home/docker/code/django_app/ecs/settings/local.py

WORKDIR /home/docker/code/django_app
RUN python3.6 manage.py collectstatic --no-input
RUN python3.6 manage.py migrate --no-input

EXPOSE 80
CMD ["supervisord", "-n"]